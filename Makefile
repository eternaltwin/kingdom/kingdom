all: install docker-start

bash-apache:
	docker exec -it kingdom_apache bash

bash-app:
	docker exec -it -u dev -it kingdom_php bash

bash-app-root:
	docker exec -it kingdom_php bash

bash-db:
	docker exec -it kingdom_database bash

db-kingdom:
	docker exec -it kingdom_database psql -U etwinAdmin kingdom

bash-eternaltwin:
	docker exec -it eternaltwin bash

build:
	docker compose -f Docker/docker-compose.yml -f Docker/docker-compose.dev.yml build
	docker compose -f Docker/docker-compose.yml -f Docker/docker-compose.dev.yml run -u root eternaltwin chown -R node:node /www
	docker compose -f Docker/docker-compose.yml -f Docker/docker-compose.dev.yml run -u root kingdom_php chown -R dev:dev /www
	docker compose -f Docker/docker-compose.yml -f Docker/docker-compose.dev.yml up --no-start

docker-start: docker-stop 
	docker compose -f Docker/docker-compose.yml -f Docker/docker-compose.dev.yml up -d --no-recreate --remove-orphans
	@echo "Project started successfully! Access : "
	@echo "EternalKingdom: http://localhost:8000"
	@echo "Eternaltwin: http://localhost:50320"

docker-stop:
	docker compose -f Docker/docker-compose.yml -f Docker/docker-compose.dev.yml stop

docker-watch:
	docker compose -f Docker/docker-compose.yml -f Docker/docker-compose.dev.yml up --no-recreate --remove-orphans

install: setup-env-variables build install-app install-eternaltwin 
	@echo "Installation completed successfully!"

install-eternaltwin: start-kingdom-database
	docker start eternaltwin
	docker compose -f Docker/docker-compose.yml -f Docker/docker-compose.dev.yml run -u node eternaltwin yarn install
	docker compose -f Docker/docker-compose.yml -f Docker/docker-compose.dev.yml run -u node eternaltwin yarn etwin db reset
	docker compose -f Docker/docker-compose.yml -f Docker/docker-compose.dev.yml run -u node eternaltwin yarn etwin db sync

install-app:
	docker start kingdom_php kingdom_database eternaltwin &&\
	docker compose -f Docker/docker-compose.yml -f Docker/docker-compose.dev.yml run -u dev kingdom_php composer install
	docker compose -f Docker/docker-compose.yml -f Docker/docker-compose.dev.yml run -u dev kingdom_php composer db:reset-and-load-fixtures

remove-all: #Warning, it will remove EVERY container, images, volumes and network not only ekingdoms ones
	docker system prune --volumes -a

reset-dependencies: install-app install-eternaltwin

reset-eternaltwin-database: 
	docker compose -f Docker/docker-compose.yml -f Docker/docker-compose.dev.yml run -u node eternaltwin yarn etwin db reset
	docker compose -f Docker/docker-compose.yml -f Docker/docker-compose.dev.yml run -u node eternaltwin yarn etwin db sync

setup-env-variables:
	cp EternalKingdom/.env.docker EternalKingdom/.env.local
	cp Eternaltwin/eternaltwin.docker.toml Eternaltwin/eternaltwin.local.toml

start-kingdom-database:
	docker start kingdom_database
