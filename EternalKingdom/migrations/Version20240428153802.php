<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240428153802 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE construction_requirements_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE battle_unit_stat_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE battle_unit_stat (id INT NOT NULL, related_army_id INT NOT NULL, unit_id INT NOT NULL, healthy_count INT NOT NULL, injured_count INT NOT NULL, dead_count INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2EA29B1527CBBE88 ON battle_unit_stat (related_army_id)');
        $this->addSql('CREATE INDEX IDX_2EA29B15F8BD700D ON battle_unit_stat (unit_id)');
        $this->addSql('ALTER TABLE battle_unit_stat ADD CONSTRAINT FK_2EA29B1527CBBE88 FOREIGN KEY (related_army_id) REFERENCES engaged_army (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE battle_unit_stat ADD CONSTRAINT FK_2EA29B15F8BD700D FOREIGN KEY (unit_id) REFERENCES resources (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE construction_requirements DROP CONSTRAINT fk_27eac9c9cf48117a');
        $this->addSql('ALTER TABLE construction_requirements DROP CONSTRAINT fk_27eac9c99e4d9fcc');
        $this->addSql('ALTER TABLE construction_requirements DROP CONSTRAINT fk_27eac9c991daa42a');
        $this->addSql('DROP TABLE construction_requirements');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE battle_unit_stat_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE construction_requirements_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE construction_requirements (id INT NOT NULL, construction_id INT NOT NULL, required_construction_id INT DEFAULT NULL, required_title_id INT DEFAULT NULL, level INT NOT NULL, required_level INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_27eac9c991daa42a ON construction_requirements (required_title_id)');
        $this->addSql('CREATE INDEX idx_27eac9c99e4d9fcc ON construction_requirements (required_construction_id)');
        $this->addSql('CREATE INDEX idx_27eac9c9cf48117a ON construction_requirements (construction_id)');
        $this->addSql('ALTER TABLE construction_requirements ADD CONSTRAINT fk_27eac9c9cf48117a FOREIGN KEY (construction_id) REFERENCES constructions (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE construction_requirements ADD CONSTRAINT fk_27eac9c99e4d9fcc FOREIGN KEY (required_construction_id) REFERENCES constructions (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE construction_requirements ADD CONSTRAINT fk_27eac9c991daa42a FOREIGN KEY (required_title_id) REFERENCES titles (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE battle_unit_stat DROP CONSTRAINT FK_2EA29B1527CBBE88');
        $this->addSql('ALTER TABLE battle_unit_stat DROP CONSTRAINT FK_2EA29B15F8BD700D');
        $this->addSql('DROP TABLE battle_unit_stat');
    }
}
