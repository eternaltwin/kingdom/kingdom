<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211219183014 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Alpha2';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE built_buildings_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE city_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE construction_requirements_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE construction_schemas_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE constructions_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE lord_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE max_storage_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE resource_conversion_schemas_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE resources_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE storage_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE titles_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE built_buildings (id INT NOT NULL, city_id INT NOT NULL, building_id INT NOT NULL, level SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8D25853F8BAC62AF ON built_buildings (city_id)');
        $this->addSql('CREATE INDEX IDX_8D25853F4D2A7E12 ON built_buildings (building_id)');
        $this->addSql('CREATE TABLE city (id INT NOT NULL, lord_id INT NOT NULL, construction_project_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, grid VARCHAR(255) NOT NULL, remaining_turns SMALLINT NOT NULL, wasted_turns SMALLINT NOT NULL, recruiting BOOLEAN NOT NULL, project_level INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D5B0234868E8BB9 ON city (lord_id)');
        $this->addSql('CREATE INDEX IDX_2D5B0234C8CFBAB8 ON city (construction_project_id)');
        $this->addSql('CREATE TABLE construction_requirements (id INT NOT NULL, construction_id INT NOT NULL, required_construction_id INT DEFAULT NULL, required_title_id INT DEFAULT NULL, level INT NOT NULL, required_level INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_27EAC9C9CF48117A ON construction_requirements (construction_id)');
        $this->addSql('CREATE INDEX IDX_27EAC9C99E4D9FCC ON construction_requirements (required_construction_id)');
        $this->addSql('CREATE INDEX IDX_27EAC9C991DAA42A ON construction_requirements (required_title_id)');
        $this->addSql('CREATE TABLE construction_schemas (id INT NOT NULL, construction_id INT NOT NULL, resource_id INT NOT NULL, level INT NOT NULL, amount INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_21FAC2E6CF48117A ON construction_schemas (construction_id)');
        $this->addSql('CREATE INDEX IDX_21FAC2E689329D25 ON construction_schemas (resource_id)');
        $this->addSql('CREATE TABLE constructions (id INT NOT NULL, name VARCHAR(50) NOT NULL, construction_type VARCHAR(20) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE lord (id INT NOT NULL, name VARCHAR(50) NOT NULL, age_year INT NOT NULL, age_month DOUBLE PRECISION NOT NULL, health VARCHAR(20) NOT NULL, title VARCHAR(20) NOT NULL, glory INT NOT NULL, max_glory INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE max_storage (id INT NOT NULL, building_id INT NOT NULL, resource_id INT NOT NULL, level SMALLINT NOT NULL, max_amount INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C08941704D2A7E12 ON max_storage (building_id)');
        $this->addSql('CREATE INDEX IDX_C089417089329D25 ON max_storage (resource_id)');
        $this->addSql('CREATE TABLE resource_conversion_schemas (id INT NOT NULL, required_building_id INT NOT NULL, required_resource_id INT NOT NULL, produced_resource_id INT NOT NULL, required_level INT NOT NULL, required_amount INT NOT NULL, produced_amount INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_117462B8D691282 ON resource_conversion_schemas (required_building_id)');
        $this->addSql('CREATE INDEX IDX_117462B8C971F1B5 ON resource_conversion_schemas (required_resource_id)');
        $this->addSql('CREATE INDEX IDX_117462B841268226 ON resource_conversion_schemas (produced_resource_id)');
        $this->addSql('CREATE TABLE resources (id INT NOT NULL, name VARCHAR(50) NOT NULL, resource_type VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE storage (id INT NOT NULL, city_id INT NOT NULL, resource_id INT NOT NULL, amount INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_547A1B348BAC62AF ON storage (city_id)');
        $this->addSql('CREATE INDEX IDX_547A1B3489329D25 ON storage (resource_id)');
        $this->addSql('CREATE TABLE titles (id INT NOT NULL, name VARCHAR(50) NOT NULL, glory INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, user_id VARCHAR(180) NOT NULL, display_name VARCHAR(180) NOT NULL, roles JSON NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649A76ED395 ON "user" (user_id)');
        $this->addSql('ALTER TABLE built_buildings ADD CONSTRAINT FK_8D25853F8BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE built_buildings ADD CONSTRAINT FK_8D25853F4D2A7E12 FOREIGN KEY (building_id) REFERENCES constructions (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE city ADD CONSTRAINT FK_2D5B0234868E8BB9 FOREIGN KEY (lord_id) REFERENCES lord (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE city ADD CONSTRAINT FK_2D5B0234C8CFBAB8 FOREIGN KEY (construction_project_id) REFERENCES constructions (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE construction_requirements ADD CONSTRAINT FK_27EAC9C9CF48117A FOREIGN KEY (construction_id) REFERENCES constructions (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE construction_requirements ADD CONSTRAINT FK_27EAC9C99E4D9FCC FOREIGN KEY (required_construction_id) REFERENCES constructions (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE construction_requirements ADD CONSTRAINT FK_27EAC9C991DAA42A FOREIGN KEY (required_title_id) REFERENCES titles (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE construction_schemas ADD CONSTRAINT FK_21FAC2E6CF48117A FOREIGN KEY (construction_id) REFERENCES constructions (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE construction_schemas ADD CONSTRAINT FK_21FAC2E689329D25 FOREIGN KEY (resource_id) REFERENCES resources (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE max_storage ADD CONSTRAINT FK_C08941704D2A7E12 FOREIGN KEY (building_id) REFERENCES constructions (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE max_storage ADD CONSTRAINT FK_C089417089329D25 FOREIGN KEY (resource_id) REFERENCES resources (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE resource_conversion_schemas ADD CONSTRAINT FK_117462B8D691282 FOREIGN KEY (required_building_id) REFERENCES constructions (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE resource_conversion_schemas ADD CONSTRAINT FK_117462B8C971F1B5 FOREIGN KEY (required_resource_id) REFERENCES resources (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE resource_conversion_schemas ADD CONSTRAINT FK_117462B841268226 FOREIGN KEY (produced_resource_id) REFERENCES resources (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE storage ADD CONSTRAINT FK_547A1B348BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE storage ADD CONSTRAINT FK_547A1B3489329D25 FOREIGN KEY (resource_id) REFERENCES resources (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE built_buildings DROP CONSTRAINT FK_8D25853F8BAC62AF');
        $this->addSql('ALTER TABLE storage DROP CONSTRAINT FK_547A1B348BAC62AF');
        $this->addSql('ALTER TABLE built_buildings DROP CONSTRAINT FK_8D25853F4D2A7E12');
        $this->addSql('ALTER TABLE city DROP CONSTRAINT FK_2D5B0234C8CFBAB8');
        $this->addSql('ALTER TABLE construction_requirements DROP CONSTRAINT FK_27EAC9C9CF48117A');
        $this->addSql('ALTER TABLE construction_requirements DROP CONSTRAINT FK_27EAC9C99E4D9FCC');
        $this->addSql('ALTER TABLE construction_schemas DROP CONSTRAINT FK_21FAC2E6CF48117A');
        $this->addSql('ALTER TABLE max_storage DROP CONSTRAINT FK_C08941704D2A7E12');
        $this->addSql('ALTER TABLE resource_conversion_schemas DROP CONSTRAINT FK_117462B8D691282');
        $this->addSql('ALTER TABLE city DROP CONSTRAINT FK_2D5B0234868E8BB9');
        $this->addSql('ALTER TABLE construction_schemas DROP CONSTRAINT FK_21FAC2E689329D25');
        $this->addSql('ALTER TABLE max_storage DROP CONSTRAINT FK_C089417089329D25');
        $this->addSql('ALTER TABLE resource_conversion_schemas DROP CONSTRAINT FK_117462B8C971F1B5');
        $this->addSql('ALTER TABLE resource_conversion_schemas DROP CONSTRAINT FK_117462B841268226');
        $this->addSql('ALTER TABLE storage DROP CONSTRAINT FK_547A1B3489329D25');
        $this->addSql('ALTER TABLE construction_requirements DROP CONSTRAINT FK_27EAC9C991DAA42A');
        $this->addSql('DROP SEQUENCE built_buildings_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE city_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE construction_requirements_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE construction_schemas_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE constructions_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE lord_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE max_storage_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE resource_conversion_schemas_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE resources_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE storage_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE titles_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('DROP TABLE built_buildings');
        $this->addSql('DROP TABLE city');
        $this->addSql('DROP TABLE construction_requirements');
        $this->addSql('DROP TABLE construction_schemas');
        $this->addSql('DROP TABLE constructions');
        $this->addSql('DROP TABLE lord');
        $this->addSql('DROP TABLE max_storage');
        $this->addSql('DROP TABLE resource_conversion_schemas');
        $this->addSql('DROP TABLE resources');
        $this->addSql('DROP TABLE storage');
        $this->addSql('DROP TABLE titles');
        $this->addSql('DROP TABLE "user"');
    }
}
