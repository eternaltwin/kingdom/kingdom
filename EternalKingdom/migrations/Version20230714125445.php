<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230714125445 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Movement System V2 (MovementOrder/Section & removed General info) + added Defense to City + added sovereign to WorldMapNode';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE movement_order_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE movement_section_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE movement_order (id INT NOT NULL, instigator_id INT DEFAULT NULL, start_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, current_movement_section INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_893448DBDB3A40 ON movement_order (instigator_id)');
        $this->addSql('CREATE TABLE movement_section (id INT NOT NULL, owning_order_id INT DEFAULT NULL, start_id INT NOT NULL, end_id INT NOT NULL, travel_duration INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_27126367BE682AA ON movement_section (owning_order_id)');
        $this->addSql('CREATE INDEX IDX_2712636623DF99B ON movement_section (start_id)');
        $this->addSql('CREATE INDEX IDX_2712636E2BD8A10 ON movement_section (end_id)');
        $this->addSql('ALTER TABLE movement_order ADD CONSTRAINT FK_893448DBDB3A40 FOREIGN KEY (instigator_id) REFERENCES general (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE movement_section ADD CONSTRAINT FK_27126367BE682AA FOREIGN KEY (owning_order_id) REFERENCES movement_order (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE movement_section ADD CONSTRAINT FK_2712636623DF99B FOREIGN KEY (start_id) REFERENCES world_map_node (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE movement_section ADD CONSTRAINT FK_2712636E2BD8A10 FOREIGN KEY (end_id) REFERENCES world_map_node (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE city ADD defense_id INT NOT NULL');
        $this->addSql('ALTER TABLE city ADD CONSTRAINT FK_2D5B0234FB0C2DCF FOREIGN KEY (defense_id) REFERENCES army_container (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D5B0234FB0C2DCF ON city (defense_id)');
        $this->addSql('ALTER TABLE general DROP CONSTRAINT fk_ce29364a816c6140');
        $this->addSql('DROP INDEX idx_ce29364a816c6140');
        $this->addSql('ALTER TABLE general DROP travel_progression');
        $this->addSql('ALTER TABLE general ALTER location_id SET NOT NULL');
        $this->addSql('ALTER TABLE general RENAME COLUMN destination_id TO movement_order_id');
        $this->addSql('ALTER TABLE general ADD CONSTRAINT FK_CE29364A72BF0514 FOREIGN KEY (movement_order_id) REFERENCES movement_order (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CE29364A72BF0514 ON general (movement_order_id)');
        $this->addSql('DROP INDEX idx_e3b7cea97e3c61f9');
        $this->addSql('ALTER TABLE world_map_node ADD sovereign_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE world_map_node ADD CONSTRAINT FK_E3B7CEA977655AD3 FOREIGN KEY (sovereign_id) REFERENCES lord (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E3B7CEA97E3C61F9 ON world_map_node (owner_id)');
        $this->addSql('CREATE INDEX IDX_E3B7CEA977655AD3 ON world_map_node (sovereign_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE general DROP CONSTRAINT FK_CE29364A72BF0514');
        $this->addSql('DROP SEQUENCE movement_order_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE movement_section_id_seq CASCADE');
        $this->addSql('ALTER TABLE movement_order DROP CONSTRAINT FK_893448DBDB3A40');
        $this->addSql('ALTER TABLE movement_section DROP CONSTRAINT FK_27126367BE682AA');
        $this->addSql('ALTER TABLE movement_section DROP CONSTRAINT FK_2712636623DF99B');
        $this->addSql('ALTER TABLE movement_section DROP CONSTRAINT FK_2712636E2BD8A10');
        $this->addSql('DROP TABLE movement_order');
        $this->addSql('DROP TABLE movement_section');
        $this->addSql('ALTER TABLE world_map_node DROP CONSTRAINT FK_E3B7CEA977655AD3');
        $this->addSql('DROP INDEX UNIQ_E3B7CEA97E3C61F9');
        $this->addSql('DROP INDEX IDX_E3B7CEA977655AD3');
        $this->addSql('ALTER TABLE world_map_node DROP sovereign_id');
        $this->addSql('CREATE INDEX idx_e3b7cea97e3c61f9 ON world_map_node (owner_id)');
        $this->addSql('ALTER TABLE city DROP CONSTRAINT FK_2D5B0234FB0C2DCF');
        $this->addSql('DROP INDEX UNIQ_2D5B0234FB0C2DCF');
        $this->addSql('ALTER TABLE city DROP defense_id');
        $this->addSql('DROP INDEX UNIQ_CE29364A72BF0514');
        $this->addSql('ALTER TABLE general ADD travel_progression DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE general ALTER location_id DROP NOT NULL');
        $this->addSql('ALTER TABLE general RENAME COLUMN movement_order_id TO destination_id');
        $this->addSql('ALTER TABLE general ADD CONSTRAINT fk_ce29364a816c6140 FOREIGN KEY (destination_id) REFERENCES world_map_node (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_ce29364a816c6140 ON general (destination_id)');
    }
}
