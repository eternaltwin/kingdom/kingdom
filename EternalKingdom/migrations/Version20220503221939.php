<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220503221939 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Removed City node (inheritance)';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE city_node');
        $this->addSql('ALTER TABLE city ADD node_id INT NOT NULL');
        $this->addSql('ALTER TABLE city ADD CONSTRAINT FK_2D5B0234460D9FD7 FOREIGN KEY (node_id) REFERENCES world_map_node (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D5B0234460D9FD7 ON city (node_id)');
        $this->addSql('ALTER TABLE world_map_node DROP discr');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE TABLE city_node (id INT NOT NULL, city_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_6631d52d8bac62af ON city_node (city_id)');
        $this->addSql('ALTER TABLE city_node ADD CONSTRAINT fk_6631d52d8bac62af FOREIGN KEY (city_id) REFERENCES city (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE city_node ADD CONSTRAINT fk_6631d52dbf396750 FOREIGN KEY (id) REFERENCES world_map_node (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE city DROP CONSTRAINT FK_2D5B0234460D9FD7');
        $this->addSql('DROP INDEX UNIQ_2D5B0234460D9FD7');
        $this->addSql('ALTER TABLE city DROP node_id');
        $this->addSql('ALTER TABLE world_map_node ADD discr VARCHAR(255) NOT NULL');
    }
}
