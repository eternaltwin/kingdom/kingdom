<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220508020036 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create tables to handle City, World and Kingdom Logs';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE city_log_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE log_city_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE log_kingdom_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE log_world_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE log_city (id INT NOT NULL, event_id INT NOT NULL, lord_id INT NOT NULL, timestamp TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, dynamic_data JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3C35EC4171F7E88B ON log_city (event_id)');
        $this->addSql('CREATE INDEX IDX_3C35EC41868E8BB9 ON log_city (lord_id)');
        $this->addSql('CREATE TABLE log_kingdom (id INT NOT NULL, event_id INT NOT NULL, lord_id INT NOT NULL, timestamp TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, dynamic_data JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1DA0AE8371F7E88B ON log_kingdom (event_id)');
        $this->addSql('CREATE INDEX IDX_1DA0AE83868E8BB9 ON log_kingdom (lord_id)');
        $this->addSql('CREATE TABLE log_world (id INT NOT NULL, event_id INT NOT NULL, world_id INT NOT NULL, timestamp TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, dynamic_data JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1A09FA1E71F7E88B ON log_world (event_id)');
        $this->addSql('CREATE INDEX IDX_1A09FA1E8925311C ON log_world (world_id)');
        $this->addSql('ALTER TABLE log_city ADD CONSTRAINT FK_3C35EC4171F7E88B FOREIGN KEY (event_id) REFERENCES log_event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE log_city ADD CONSTRAINT FK_3C35EC41868E8BB9 FOREIGN KEY (lord_id) REFERENCES lord (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE log_kingdom ADD CONSTRAINT FK_1DA0AE8371F7E88B FOREIGN KEY (event_id) REFERENCES log_event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE log_kingdom ADD CONSTRAINT FK_1DA0AE83868E8BB9 FOREIGN KEY (lord_id) REFERENCES lord (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE log_world ADD CONSTRAINT FK_1A09FA1E71F7E88B FOREIGN KEY (event_id) REFERENCES log_event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE log_world ADD CONSTRAINT FK_1A09FA1E8925311C FOREIGN KEY (world_id) REFERENCES world_map (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE city_log');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE log_city_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE log_kingdom_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE log_world_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE city_log_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE city_log (id INT NOT NULL, event_id INT NOT NULL, lord_id INT NOT NULL, "timestamp" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, dynamic_data JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_a209124d71f7e88b ON city_log (event_id)');
        $this->addSql('CREATE INDEX idx_a209124d868e8bb9 ON city_log (lord_id)');
        $this->addSql('ALTER TABLE city_log ADD CONSTRAINT fk_a209124d71f7e88b FOREIGN KEY (event_id) REFERENCES log_event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE city_log ADD CONSTRAINT fk_a209124d868e8bb9 FOREIGN KEY (lord_id) REFERENCES lord (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE log_city');
        $this->addSql('DROP TABLE log_kingdom');
        $this->addSql('DROP TABLE log_world');
    }
}
