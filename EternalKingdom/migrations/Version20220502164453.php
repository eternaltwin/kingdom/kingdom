<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220502164453 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Static World Data + link to dynamic world data';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE world_data_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE world_node_data_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE world_data (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE world_node_data (id INT NOT NULL, world_id INT NOT NULL, x INT NOT NULL, y INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_437B57EC8925311C ON world_node_data (world_id)');
        $this->addSql('CREATE TABLE world_map_node_adjacencies (world_node_data_source INT NOT NULL, world_node_data_target INT NOT NULL, PRIMARY KEY(world_node_data_source, world_node_data_target))');
        $this->addSql('CREATE INDEX IDX_AB07F7F574914AD1 ON world_map_node_adjacencies (world_node_data_source)');
        $this->addSql('CREATE INDEX IDX_AB07F7F56D741A5E ON world_map_node_adjacencies (world_node_data_target)');
        $this->addSql('ALTER TABLE world_node_data ADD CONSTRAINT FK_437B57EC8925311C FOREIGN KEY (world_id) REFERENCES world_data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE world_map_node_adjacencies ADD CONSTRAINT FK_AB07F7F574914AD1 FOREIGN KEY (world_node_data_source) REFERENCES world_node_data (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE world_map_node_adjacencies ADD CONSTRAINT FK_AB07F7F56D741A5E FOREIGN KEY (world_node_data_target) REFERENCES world_node_data (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE world_map_node ADD owning_world_id INT NOT NULL');
        $this->addSql('ALTER TABLE world_map_node ADD data_id INT NOT NULL');
        $this->addSql('ALTER TABLE world_map_node ALTER name DROP NOT NULL');
        $this->addSql('ALTER TABLE world_map_node ADD CONSTRAINT FK_E3B7CEA97F5CDE8E FOREIGN KEY (owning_world_id) REFERENCES world_map (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE world_map_node ADD CONSTRAINT FK_E3B7CEA937F5A13C FOREIGN KEY (data_id) REFERENCES world_node_data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E3B7CEA97F5CDE8E ON world_map_node (owning_world_id)');
        $this->addSql('CREATE INDEX IDX_E3B7CEA937F5A13C ON world_map_node (data_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE world_node_data DROP CONSTRAINT FK_437B57EC8925311C');
        $this->addSql('ALTER TABLE world_map_node DROP CONSTRAINT FK_E3B7CEA937F5A13C');
        $this->addSql('ALTER TABLE world_map_node_adjacencies DROP CONSTRAINT FK_AB07F7F574914AD1');
        $this->addSql('ALTER TABLE world_map_node_adjacencies DROP CONSTRAINT FK_AB07F7F56D741A5E');
        $this->addSql('DROP SEQUENCE world_data_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE world_node_data_id_seq CASCADE');
        $this->addSql('DROP TABLE world_data');
        $this->addSql('DROP TABLE world_node_data');
        $this->addSql('DROP TABLE world_map_node_adjacencies');
        $this->addSql('ALTER TABLE world_map_node DROP CONSTRAINT FK_E3B7CEA97F5CDE8E');
        $this->addSql('DROP INDEX IDX_E3B7CEA97F5CDE8E');
        $this->addSql('DROP INDEX IDX_E3B7CEA937F5A13C');
        $this->addSql('ALTER TABLE world_map_node DROP owning_world_id');
        $this->addSql('ALTER TABLE world_map_node DROP data_id');
        $this->addSql('ALTER TABLE world_map_node ALTER name SET NOT NULL');
    }
}
