<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221104225341 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Moved isSpawnPoint flag from WorldMapNode to WorldNodeData';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE world_map_node DROP is_spawn_point');
        $this->addSql('ALTER TABLE world_node_data ADD is_spawn_point INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE world_node_data DROP is_spawn_point');
        $this->addSql('ALTER TABLE world_map_node ADD is_spawn_point INT NOT NULL');
    }
}
