<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230411211041 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Alpha 5 : City population + delete deprecated name';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE city ADD dungeon_level SMALLINT NOT NULL');
        $this->addSql('ALTER TABLE city ADD houses_count SMALLINT NOT NULL');
        $this->addSql('ALTER TABLE city DROP name');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE city ADD name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE city DROP dungeon_level');
        $this->addSql('ALTER TABLE city DROP houses_count');
    }
}
