<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230711095107 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Relation Entity';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE relation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE relation (id INT NOT NULL, lord_id INT NOT NULL, target_id INT NOT NULL, diplomacy_type VARCHAR(255) NOT NULL, can_cross BOOLEAN NOT NULL, can_harvest BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_62894749868E8BB9 ON relation (lord_id)');
        $this->addSql('CREATE INDEX IDX_62894749158E0B66 ON relation (target_id)');
        $this->addSql('ALTER TABLE relation ADD CONSTRAINT FK_62894749868E8BB9 FOREIGN KEY (lord_id) REFERENCES lord (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE relation ADD CONSTRAINT FK_62894749158E0B66 FOREIGN KEY (target_id) REFERENCES lord (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE relation_id_seq CASCADE');
        $this->addSql('ALTER TABLE relation DROP CONSTRAINT FK_62894749868E8BB9');
        $this->addSql('ALTER TABLE relation DROP CONSTRAINT FK_62894749158E0B66');
        $this->addSql('DROP TABLE relation');
    }
}
