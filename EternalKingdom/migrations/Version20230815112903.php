<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230815112903 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Map refactor';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE world_data RENAME COLUMN dir_name TO map_file_name');
        $this->addSql('ALTER TABLE world_map ALTER speed DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE world_data RENAME COLUMN map_file_name TO dir_name');
        $this->addSql('ALTER TABLE world_map ALTER speed SET DEFAULT 1');
    }
}
