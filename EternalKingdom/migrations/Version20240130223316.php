<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240130223316 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Alpha 6 - Battle Entities';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE battle_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE engaged_army_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE fight_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE fighting_unit_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE log_battle_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE battle (id INT NOT NULL, location_id INT NOT NULL, start_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, is_finished BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1399173464D218E ON battle (location_id)');
        $this->addSql('CREATE TABLE engaged_army (id INT NOT NULL, owning_battle_id INT NOT NULL, army_container_id INT DEFAULT NULL, linked_army_id INT DEFAULT NULL, is_attacking BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B646C53BD784AC9E ON engaged_army (owning_battle_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B646C53B766D5309 ON engaged_army (army_container_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B646C53B739A4CBA ON engaged_army (linked_army_id)');
        $this->addSql('CREATE TABLE fight (id INT NOT NULL, battle_id INT NOT NULL, attacking_unit_id INT NOT NULL, defending_unit_id INT NOT NULL, start_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_21AA4456C9732719 ON fight (battle_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_21AA445674920362 ON fight (attacking_unit_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_21AA44568831E2B ON fight (defending_unit_id)');
        $this->addSql('CREATE TABLE fighting_unit (id INT NOT NULL, unit_type_id INT NOT NULL, unit_owner_id INT NOT NULL, remaining_health INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_446978E191058251 ON fighting_unit (unit_type_id)');
        $this->addSql('CREATE INDEX IDX_446978E1B028B6B ON fighting_unit (unit_owner_id)');
        $this->addSql('CREATE TABLE log_battle (id INT NOT NULL, event_id INT NOT NULL, battle_id INT NOT NULL, timestamp TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, dynamic_data JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_663449671F7E88B ON log_battle (event_id)');
        $this->addSql('CREATE INDEX IDX_6634496C9732719 ON log_battle (battle_id)');
        $this->addSql('ALTER TABLE battle ADD CONSTRAINT FK_1399173464D218E FOREIGN KEY (location_id) REFERENCES world_map_node (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE engaged_army ADD CONSTRAINT FK_B646C53BD784AC9E FOREIGN KEY (owning_battle_id) REFERENCES battle (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE engaged_army ADD CONSTRAINT FK_B646C53B766D5309 FOREIGN KEY (army_container_id) REFERENCES army_container (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE engaged_army ADD CONSTRAINT FK_B646C53B739A4CBA FOREIGN KEY (linked_army_id) REFERENCES army_container (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE fight ADD CONSTRAINT FK_21AA4456C9732719 FOREIGN KEY (battle_id) REFERENCES battle (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE fight ADD CONSTRAINT FK_21AA445674920362 FOREIGN KEY (attacking_unit_id) REFERENCES fighting_unit (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE fight ADD CONSTRAINT FK_21AA44568831E2B FOREIGN KEY (defending_unit_id) REFERENCES fighting_unit (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE fighting_unit ADD CONSTRAINT FK_446978E191058251 FOREIGN KEY (unit_type_id) REFERENCES resources (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE fighting_unit ADD CONSTRAINT FK_446978E1B028B6B FOREIGN KEY (unit_owner_id) REFERENCES army_container (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE log_battle ADD CONSTRAINT FK_663449671F7E88B FOREIGN KEY (event_id) REFERENCES log_event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE log_battle ADD CONSTRAINT FK_6634496C9732719 FOREIGN KEY (battle_id) REFERENCES battle (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE general ADD battle_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE general ADD CONSTRAINT FK_CE29364AC9732719 FOREIGN KEY (battle_id) REFERENCES battle (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_CE29364AC9732719 ON general (battle_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE general DROP CONSTRAINT FK_CE29364AC9732719');
        $this->addSql('DROP SEQUENCE battle_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE engaged_army_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE fight_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE fighting_unit_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE log_battle_id_seq CASCADE');
        $this->addSql('ALTER TABLE battle DROP CONSTRAINT FK_1399173464D218E');
        $this->addSql('ALTER TABLE engaged_army DROP CONSTRAINT FK_B646C53BD784AC9E');
        $this->addSql('ALTER TABLE engaged_army DROP CONSTRAINT FK_B646C53B766D5309');
        $this->addSql('ALTER TABLE engaged_army DROP CONSTRAINT FK_B646C53B739A4CBA');
        $this->addSql('ALTER TABLE fight DROP CONSTRAINT FK_21AA4456C9732719');
        $this->addSql('ALTER TABLE fight DROP CONSTRAINT FK_21AA445674920362');
        $this->addSql('ALTER TABLE fight DROP CONSTRAINT FK_21AA44568831E2B');
        $this->addSql('ALTER TABLE fighting_unit DROP CONSTRAINT FK_446978E191058251');
        $this->addSql('ALTER TABLE fighting_unit DROP CONSTRAINT FK_446978E1B028B6B');
        $this->addSql('ALTER TABLE log_battle DROP CONSTRAINT FK_663449671F7E88B');
        $this->addSql('ALTER TABLE log_battle DROP CONSTRAINT FK_6634496C9732719');
        $this->addSql('DROP TABLE battle');
        $this->addSql('DROP TABLE engaged_army');
        $this->addSql('DROP TABLE fight');
        $this->addSql('DROP TABLE fighting_unit');
        $this->addSql('DROP TABLE log_battle');
        $this->addSql('DROP INDEX IDX_CE29364AC9732719');
        $this->addSql('ALTER TABLE general DROP battle_id');
    }
}
