<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240601235828 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'WorldMap last update date & Battle worldmap FK';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE battle ADD world_map_id INT NOT NULL');
        $this->addSql('ALTER TABLE battle ADD CONSTRAINT FK_13991734C3FB2227 FOREIGN KEY (world_map_id) REFERENCES world_map (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_13991734C3FB2227 ON battle (world_map_id)');
        $this->addSql('ALTER TABLE world_map ADD last_update_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE world_map DROP last_update_date');
        $this->addSql('ALTER TABLE battle DROP CONSTRAINT FK_13991734C3FB2227');
        $this->addSql('DROP INDEX IDX_13991734C3FB2227');
        $this->addSql('ALTER TABLE battle DROP world_map_id');
    }
}
