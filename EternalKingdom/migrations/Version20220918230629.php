<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220918230629 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add ManyToOne relation between City & Node';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX uniq_2d5b0234460d9fd7');
        $this->addSql('CREATE INDEX IDX_2D5B0234460D9FD7 ON city (node_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX IDX_2D5B0234460D9FD7');
        $this->addSql('CREATE UNIQUE INDEX uniq_2d5b0234460d9fd7 ON city (node_id)');
    }
}
