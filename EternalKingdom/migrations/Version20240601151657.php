<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240601151657 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Replaced the ArmyContainer FK by an EngagedArmy FK in the FightingUnit Entity';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE fighting_unit DROP CONSTRAINT fk_446978e1b028b6b');
        $this->addSql('DROP INDEX idx_446978e1b028b6b');
        $this->addSql('ALTER TABLE fighting_unit RENAME COLUMN unit_owner_id TO owning_engaged_army_id');
        $this->addSql('ALTER TABLE fighting_unit ADD CONSTRAINT FK_446978E1AFC70DDE FOREIGN KEY (owning_engaged_army_id) REFERENCES engaged_army (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_446978E1AFC70DDE ON fighting_unit (owning_engaged_army_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE fighting_unit DROP CONSTRAINT FK_446978E1AFC70DDE');
        $this->addSql('DROP INDEX IDX_446978E1AFC70DDE');
        $this->addSql('ALTER TABLE fighting_unit RENAME COLUMN owning_engaged_army_id TO unit_owner_id');
        $this->addSql('ALTER TABLE fighting_unit ADD CONSTRAINT fk_446978e1b028b6b FOREIGN KEY (unit_owner_id) REFERENCES army_container (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_446978e1b028b6b ON fighting_unit (unit_owner_id)');
    }
}
