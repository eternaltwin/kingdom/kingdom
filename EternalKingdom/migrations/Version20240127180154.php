<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240127180154 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Inactivity + Eternaltwin User Page logic';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE lord ALTER birth_date SET NOT NULL');
        $this->addSql('ALTER TABLE "user" ADD is_inactive_in_game BOOLEAN NOT NULL DEFAULT false');
        $this->addSql('CREATE INDEX eternaltwin_idx ON "user" (user_id)');
        $this->addSql('ALTER TABLE world_map ALTER speed SET DEFAULT 1');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE lord ALTER birth_date DROP NOT NULL');
        $this->addSql('DROP INDEX eternaltwin_idx');
        $this->addSql('ALTER TABLE "user" DROP is_inactive_in_game');
        $this->addSql('ALTER TABLE world_map ALTER speed DROP DEFAULT');
    }
}
