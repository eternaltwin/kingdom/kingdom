<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250102210731 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Replaced "Inactive Units" logic by a new FightSurvivor class';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE fight_survivor_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE fight_survivor (id INT NOT NULL, unit_type_id INT NOT NULL, owning_engaged_army_id INT NOT NULL, remaining_health INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2E9D2F1891058251 ON fight_survivor (unit_type_id)');
        $this->addSql('CREATE INDEX IDX_2E9D2F18AFC70DDE ON fight_survivor (owning_engaged_army_id)');
        $this->addSql('ALTER TABLE fight_survivor ADD CONSTRAINT FK_2E9D2F1891058251 FOREIGN KEY (unit_type_id) REFERENCES resources (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE fight_survivor ADD CONSTRAINT FK_2E9D2F18AFC70DDE FOREIGN KEY (owning_engaged_army_id) REFERENCES engaged_army (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE fight_survivor_id_seq CASCADE');
        $this->addSql('ALTER TABLE fight_survivor DROP CONSTRAINT FK_2E9D2F1891058251');
        $this->addSql('ALTER TABLE fight_survivor DROP CONSTRAINT FK_2E9D2F18AFC70DDE');
        $this->addSql('DROP TABLE fight_survivor');
    }
}
