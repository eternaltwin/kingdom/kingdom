<?php

use App\Entity\Lord;
use App\Entity\World\WorldMapNode;
use App\Kingdom\Lord\Enum\TitleEnum;
use App\Kingdom\Lord\Service\UpdateGloryAndTitle;
use App\Kingdom\Ruleset\Ruleset;
use App\Kingdom\Ruleset\Service\GetRuleset;
use App\Service\GameLogManager;
use Doctrine\Common\Collections\ArrayCollection;

beforeEach(function () {
    /** @var ReflectionClass<Lord> */
    $lordReflection = new ReflectionClass(Lord::class);
    $this->lord = $lordReflection->newInstanceWithoutConstructor();

    $lordReflection->getProperty('apogee')->setValue($this->lord, 5);
    $lordReflection->getProperty('glory')->setValue($this->lord, 5);

    $this->ruleset = Mockery::mock(Ruleset::class);
    $this->ruleset->shouldReceive('getTitleFromApogee')->andReturn(TitleEnum::KING);
});

// describe('Lord', function () {
test('Lord should update apogee to reflect a glory gain and update title accordingly', function () {
    $this->lord->setGlory(10, $this->ruleset);

    expect($this->lord->getApogee())->toBe(10);
    expect($this->lord->getTitle())->toBe(TitleEnum::KING);
});

test('Lord should not lower apogee on glory loss', function () {
    $this->lord->setGlory(3, $this->ruleset);

    expect($this->lord->getApogee())->toBe(5);
});

// TODO Ask BSanchez why it doesn't work...
/*
test('Lord glory should be updated based on kingdom size', function () {
    $getRuleset = Mockery::mock(GetRuleset::class);
    $getRuleset->shouldReceive('exec')->andReturn($this->ruleset);

    $kingdomNode = Mockery::mock(WorldMapNode::class);
    $lord = Mockery::mock(Lord::class);
    $lord->shouldReceive('getKingdom')->andReturn(new ArrayCollection([$kingdomNode, $kingdomNode, $kingdomNode, $kingdomNode]));
    $lord->shouldReceive('setGlory')->withArgs(fn (int $glory, Ruleset $ruleset) => 4 === $glory);

    $updateGloryAndTitle = new UpdateGloryAndTitle($getRuleset);

    $updateGloryAndTitle->exec($lord);
});
*/
