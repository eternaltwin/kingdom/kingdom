date:
  logTimestamp: "Le %date% à %time%"
  dateFormat: "d M."
  timeFormat: "H:i:s"
city:
  resource:
    production: "Votre ville a produit %production% %icon%"
    waste: "%waste% %icon% ont été gâchés car vous n'avez pas de stock suffisant"
    conversion: "Vous avez converti %inAmount% %inIcon% en %outAmount% %outIcon%"
  consumption:
    food: "Vos habitants ont mangé <b>%amount%</b> %icon%"
    foodAndGold: "Vos habitants ont mangé <b>%foodAmount%</b> %foodIcon% (et vous avez dépensé <b>%goldAmount%</b> %goldIcon%)"
  citizen:
    new: "Un nouveau citoyen %icon% a rejoint votre ville"
    conversion: "Un citoyen a appris le métier <b>%jobName%</b>"
    starvation: "Un <b>%jobName%</b> est mort de faim"
  hammer:
    noConstruction: "Les ouvriers n'ont pas pu travailler car aucun bâtiment n'est en construction en ce moment %icon%"
    production: "Votre ville a produit %amount% %iconGold% et %amount% %iconFood% grâce au chantier."
    progress: "L'avancement de votre construction a progressé de <b>%amount%</b> %icon%"
  replay: "{1}Vous pouvez rejouer <b>1</b> tour %icon%|]1,Inf[ Vous pouvez rejouer <b>%count%</b> tours %icon%"
  army:
    recruitment: "Vous avez recruté <b>%amount%</b> %icon%"
    noBarracks: "Vous n'avez pas encore construit de caserne"
    noRecruitment: "Aucun soldat n'a été produit car vous avez désactivé le recrutement %icon%"
    conversion: "Vous avez amélioré %soldierAmount% de vos soldats en %outAmount% %outIcon% !"
  general:
    recruitment: "Félitations! Un nouveau général a rejoint vos rangs et peut désormais diriger une armée."
    harvest: "Votre général <b>%generalName%</b> a ramassé %amount% %icon% du coté de %nodeLink%."
  construction:
    building:
      started: "Vous avez lancé la construction du bâtiment <b>%name%</b>"
      finished: "Vous avez terminé la construction du bâtiment <b>%name%</b>"
      cancelled: "La construction du bâtiment <b>%name%</b> a été annulée %icon%"
    unit:
      started: "Vous avez lancé la construction de l'unité <b>%name%</b>"
      finished: "Vous avez terminé la construction de l'unité <b>%name%</b>"
      cancelled: "La construction du l'unité <b>%name%</b> a été annulée %icon%"
  gridReseted: "Aucune action n'étant disponible, la grille a été réinitialisée"

management:
  spawn: "%icon% La ville de %nodeLink% vous a été confiée et vous y avez établi votre capitale !"
  death:
    inactivity: "%icon% Suite à votre longue inactivité, votre Royaume s'est effondré après une longue décadence..."
  promotion: "%icon% Félicitations ! Grâce à vos nombreuses conquêtes, vous avez obtenu le titre <b>%title%</b> !"
  conquest:
    noResistance: "%icon% Vous avez pris le contrôle de %nodeLink%."
    withBattle: "%icon% Vous avez pris le contrôle de %nodeLink% qui était le propriété de %lordLink%."
    fromVassal: "%l_conquest% Votre vassal le %lordLink% a conquis %nodeLink% ce qui augmente votre Gloire."
  territoryGotStolen: "%icon% %lordLink% a capturé %nodeLink% qui faisait partie de votre Royaume !"
  healthDegradation:
    excellentToGood: "%icon% Après des années de labeur, vous commencez à sentir le poids de vos efforts peser sur vos épaules."
    goodToBad: "%icon% Votre santé s'est fortement dégradée ces derniers temps. Il faudrait commencer à réfléchir à l'éventualité de votre mort et à la façon dont vos voisins vont se partager votre Royaume..."
  diplomacy:
    friend: "%icon% Le %lordLink% a adopté une attitude amicale à votre égard."
    enemy: "%icon% Le %lordLink% a déclaré que vous faisiez désormais partie de ses ennemis !"
    neutral: "%icon% Le %lordLink% a adopté une attitude neutre à votre égard."
  crossRight:
    added: "%icon% Le %lordLink% vous a accordé le droit de passage dans son Royaume."
    removed: "%icon% Le %lordLink% vous a enlevé le droit de passage dans son Royaume !"
  harvestRight:
    added: "%icon% Le %lordLink% vous a accordé la récolte des ressources de son Royaume."
    removed: "%icon% Le %lordLink% vous a enlevé la possibilité de récolter les ressources de son Royaume !"
  battle:
    instigated:
      againstBarbarian: "%icon% Votre général <b>%generalName%</b> a attaqué un chef barbare à %nodeLink%."
      againstPlayer: "%icon% Votre général <b>%generalName%</b> a attaqué le %lordLink% à %nodeLink%."
    defense:
      onOurTerritory: "%icon% Le général <b>%generalName%</b> aux ordres de %lordLink% a attaqué %nodeLink%."
      onOurCapitalCity: "%icon% Le général <b>%generalName%</b> aux ordres de %lordLink% a attaqué votre capitale %nodeLink%."
    won: "%icon% Vous avez remporté la %battleLink% !"
    lost: "%icon% Vous avez perdu la %battleLink% !"

world:
  spawn:
    free: "%icon% Un jeune chevalier du nom de %lordLink% a pris le contrôle de %nodeLink%."
  resource:
    spawn: "%icon% On a découvert une <b>%resourceSpot%</b> du côté de %nodeLink% !"
  death:
    inactivity: "%icon% Le Royaume du %lordLink% s'est effondré : %nodeLink% est désormais une ville Barbare."
  conquest:
    noResistance: "%icon% Le général <strong>%generalName%</strong> sous les ordres du %lordLink% a capturé %nodeLink% sans aucune résistance."
    withBattle: "%icon% Le général <strong>%generalName%</strong> sous les ordres du %lordLink% a conquis %nodeLink%."
  promotion: "%icon% Suite à ses nombreuses conquêtes, %lordLink% a été proclamé <strong>%newTitle%</strong>"
  renameNode: "%icon% A sa mort, l'Empereur %lordLink% a donné à sa capitale le bon nom de %newName% !"

battle:
  defense:
    barbarianJoined: "%icon% Un chef barbare a rejoint la défense avec <b>%amount%</b> unités."
    garrisonJoined: "%icon% La garnison du %lordLink% a rejoint la défense avec <b>%amount%</b> unités."
    generalJoined: "%icon% Le général <b>%generalName%</b> aux ordres du %lordLink% a rejoint la défense avec <b>%amount%</b> unités."
    won: "%icon% La bataille est terminée et la victoire revient aux défenseurs."
  attack:
    generalJoined: "%icon% Le général <b>%generalName%</b> aux ordres du %lordLink% a rejoint l'attaque avec <b>%amount%</b> unités."
    won: "%icon% La bataille est terminée et la victoire revient aux attaquants."
  nodeOwner:
    unit:
      died: "%icon% Un <b>%unit%</b> du %lordLink% est mort !"
      destroyed: "%icon% Une <b>%unit%</b> du %lordLink% a été détruite !"
  general:
    unit:
      died: "%icon% Un <b>%unit%</b> du général <b>%generalName%</b> est mort !"
      destroyed: "%icon% Une <b>%unit%</b> du général <b>%generalName%</b> a été détruite !"
    defeated: "%icon% Le général <b>%generalName%</b> aux ordres du %lordLink% a été vaincu !"
  barbarian:
    unit:
      died: "%icon% Un <b>%unit%</b> barbare est mort !"
      destroyed: "%icon% Une <b>%unit%</b> barbare a été détruite !"