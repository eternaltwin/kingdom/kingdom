const debugServerRequest = true;

//#region Server request

// TODO Create Unit tests for these requests.
// TODO Rename every function in low caps!

/*
 * Triggers a GET request and call the callback when request succeeded!
 * It handles Text and JSON response!
 */
function ServerGet(path, params, callback)
{
    if (debugServerRequest)
    {
        console.log("[ServerGET] URL:" + path);
    }

    if (params !== null)
    {
        path = path + "?" + new URLSearchParams(params);
        if (debugServerRequest)
        {
            console.log("[ServerGET] Params\n");
            console.log(params);
        }
    }

    fetch(path, {
        method: "GET",
    })
    .then(function (response)
    {
        if(handleServerResponse(response, "GET"))
        {
            const contentType = response.headers.get("content-type");
            if (contentType !== null && contentType.indexOf("application/json") !== -1) {
                return response.json().then(function (data)
                {
                    if(data != null)
                    {
                        if (debugServerRequest)
                        {
                            console.log("[ServerGET] JSON:");
                            console.log(data);
                        }

                        callback(data);
                    }
                });
            }
            else
            {
                return response.text().then(function (text)
                {
                    // It can be very verbose!
                    /*
                    if (debugServerRequest)
                    {
                        console.log("[ServerGET] Text:");
                        console.log(text);
                    }
                     */

                    callback(text);
                });
            }
        }
    })
    .catch(function (error)
    {
        console.error(error);
    });
}

/*
 * Triggers a POST request with the specified payload and call the callback when request succeeded!
 * It only handle JSON response!
 */
function ServerPost(path, payload, callback)
{
    if (debugServerRequest)
    {
        console.log("[ServerPOST] URL:" + path);
        // I separated those 2 lines to be able to manipulate the json object in the debug console!
        console.log("[ServerPOST] Payload:\n");
        console.log(payload);
    }

    // TODO If we need other type of body, refactor the function!
    let body = GenerateFormDataBody(payload);

    fetch(path, {
        method: "POST",
        body: body
    })
    .then(function (response)
    {
        if (handleServerResponse(response, "POST"))
        {
            return response.json();
        }
    })
    .then(function (data)
    {
        if (debugServerRequest)
        {
            console.log("[ServerPOST] JSON:");
            console.log(data);
        }

        callback(data);
    })
    .catch(function (error)
    {
        console.error(error);
    });
}

function handleServerResponse(response, prefix)
{
    let debugResponse = "[Server"+prefix+"] " + response.status + " -> " + response.statusText;
    if (response.status !== 200)
    {
        throw debugResponse;
    }

    return true;
}

/*
 * Generates a specific type of POST/PUT request's body using a Form.
 */
function GenerateFormDataBody(payload)
{
    let data = new FormData();
    data.append("json", JSON.stringify(payload));

    return data;
}

//#endregion

//#region Maths

function interpolate(start, end, t)
{
    return start * (1.0 - t) + end * t;
}

function clamp(number, min, max)
{
    return Math.max(min, Math.min(number, max));
}

// Get random integer between 0 and max exclusive
function GetRandomInRange(a_max)
{
    return Math.floor(Math.random() * a_max);
}

//#endregion

//#region Easings

function easeInQuad(t) {
    return t*t;
}

function easeOutQuad(t) {
    return 1 - (1 - t) * (1 - t);
}

function easeInExpo(t) {
    return t === 0 ? 0 : Math.pow(2, 10 * t - 10);
}

function easeOutExpo(t) {
    return t === 1 ? 1 : 1 - Math.pow(2, -10 * t);
}

function easeOutSine(t) {
    return Math.sin((t * Math.PI) / 2);
}

function easeOutBack(t) {
    const c1 = 2.25158;
    const c3 = c1 + 1;
    return 1 + c3 * Math.pow(t - 1, 3) + c1 * Math.pow(t - 1, 2);
}

//#endregion

//#region Seeded Random
/**
 * Returns a hash code from a string 
 * (simple, *insecure* hash that's short, fast, and has no dependencies.)
 * @param  {String} str The string to hash.
 * @return {Number}    A 32bit integer
 * @see https://stackoverflow.com/questions/521295/seeding-the-random-number-generator-in-javascript/47593316#47593316
 */
 function hashCode(str)
 {
    let hash = 0;
    for (let i = 0, len = str.length; i < len; i++) 
    {
        let chr = str.charCodeAt(i);
        hash = (hash << 5) - hash + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
}

function mulberry32(a) {
    return function() {
        let t = a += 0x6D2B79F5;
        t = Math.imul(t ^ t >>> 15, t | 1);
        t ^= t + Math.imul(t ^ t >>> 7, t | 61);
        return ((t ^ t >>> 14) >>> 0) / 4294967296;
    }
}

const seededRandom = ({rng = null, seed = "cucurbipaillettes"} = {}) => {
    rng = rng || mulberry32(hashCode(seed));
  
    return (maxRange) => Math.floor(rng() * maxRange);
};

//#endregion

//#region MT function wrappers

/* Classic hint with a title and a message */
function showHint(refObj, title, message)
{
    mt.js.Tip.show(refObj,'<div class=\'tiptop\'><div class=\'tipbottom\'><div class=\'tipbg\'><h1>'+title+'</h1>'+message+'</div></div></div>');
}

/* Simple hint with a message only */
function showHintMsg(refObj, message)
{
    mt.js.Tip.show(refObj,'<div class=\'tiptop\'><div class=\'tipbottom\'><div class=\'tipbg\'>'+message+'</div></div></div>');
}

function showWipMsg(refObj, message)
{
    showHintMsg(refObj,'<strong class=\'wip\'>'+message+'</strong>');
}

function hideHint()
{
    mt.js.Tip.hide();
}

//#endregion

function showDOM(element, isVisible)
{
    console.assert(element != null);
    element.style.display = isVisible ? "block" : "none";
}

/**
 * 
 * @param date (Date object)
 * @returns string (formatted date)
 */
function formatDate(date) {
    const datePart = new Intl.DateTimeFormat('fr-CA').format(date);
    const timePart = date.toTimeString().split(' ')[0];
    return `${datePart} ${timePart}`;
}

/**
 * 
 * @param date (Date object)
 * @returns string (formatted date)
 */
function formatDateToISOString(date) {
    return date.toISOString().substring(0, 19).replace("T", " ");
}

function updateTimer(endDate, timerDivId = null)
{
    const currentDate = formatDateToISOString(new Date());
    if(currentDate < endDate) 
    {
        mt.js.Timer.TIMES="jhms";
        mt.js.Timer.alloc(formatDateToISOString(new Date()), endDate, 3, timerDivId);
    }
}

//#region Url

function getUserShortcutURL(owner)
{
    return '/user/' + owner.userId;
}

function getNodeShortcutURL(worldDbID, nodeDbID)
{
    return '/map/' + worldDbID + '/node/' + nodeDbID;
}

function getGeneralShortcutURL(worldDbID, generalDbID)
{
    return '/map/' + worldDbID + '/general/' + generalDbID;
}

function getGeneralActionURL(generalId, action)
{
    return '/general/' + generalId + '/' + action;
}

function getBattleActionURL(battleDbId)
{
    return '/battle/' + battleDbId;
}

//#endregion

//#region Common button

function recruitGeneral()
{
    document.location = '/city/chooseGeneralName';
}

//#endregion