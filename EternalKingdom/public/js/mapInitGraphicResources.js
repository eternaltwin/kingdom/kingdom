// ETERNAL KINGDOM - Generation and display of the map background
// We call "map background" everything that never change in the map:
//  - ground
//  - sprites of mountains, trees, lakes, etc.
//  - places (not the capitals)
//  - paths between cities


const chunkWidth = 1024;
const chunkHeight = 512;

let mapRenderer;

let shaders = {};
let uniformBufferObjects = {}
let textures = {};

let distancesDict = {};
let vectorsLinksDict = {};
let cityNameToId = {};
let linksMesh;
let citiesRenderableData;
let resourcePanelsMesh;
let resourcesMesh;

let kingdomsColorTex;

let citiesSprites = {};
let groundSprites = {};
let landscapeSprites = {};
let lakesSprites = {};
let groundMinMax = [0., 0., 0., 0.];
let landscapeMinMax = [0., 0., 0., 0.];
let wavesSprites = [];

let easterEggsMesh;
let easterEggMeshSelection;


async function initMapRendering(mapGenWidth, mapGenHeight)
{
    await compileShaders();
    await createTextures();

    prepareLinks();
    prepareSprites();
    
    mapRenderer = new MapRenderer(mapGenWidth, mapGenHeight);

    let nbChunksY = mapGenHeight * MAP_SCALE_FACTOR / chunkHeight;
    let ceilPowerOfTwo = 1;
    let v = nbChunksY;
    while (v >>= 1) ceilPowerOfTwo <<= 1;
    if (ceilPowerOfTwo !== nbChunksY) ceilPowerOfTwo <<= 1;
    let zRatio = 2. / (chunkHeight * ceilPowerOfTwo);

    uniformBufferObjects['mapGenSize'].sendData([
        mapGenWidth, mapGenHeight
    ]);
    uniformBufferObjects['viewParameters'].sendData([
        2. / gl.canvas.width, 2. / gl.canvas.height, zRatio,
        mapRenderer.viewPosition.x, mapRenderer.viewPosition.y,
        mapRenderer.viewPosition.zoom
    ]);
    
    uniformBufferObjects['chunksRatio'].sendData([
        2. / chunkWidth, 2. / chunkHeight, zRatio
    ]);
    uniformBufferObjects['time'].sendData([0.]);

    loadMapGameState();

    let zonesFramebuffer = renderZonesFramebuffer(mapGenWidth, mapGenHeight);

    mapRenderer.renderChunks(zonesFramebuffer);
    mapRenderer.renderChunksSelection();
}

async function compileShaders()
{
    for (const [name, shaderParam] of Object.entries(shadersParam))
    {
        shaders[name] = new ShaderProgram(
            name, shaderParam['vs'], shaderParam['fs'], 
            shaderParam['attributes'], shaderParam['uniforms'],
            shaderParam['perInstanceAttributes']
        );
    }

    let uniformBlockBinding = 0;
    let bufferBaseIndex = 0;

    for (const [name, param] of Object.entries(uboParam))
    {
        programList = param['programs'].map(name => shaders[name]);

        uniformBufferObjects[name] = new UniformBufferObject(
            programList, name, param['variables'], bufferBaseIndex, uniformBlockBinding
        );
        bufferBaseIndex++;
        uniformBlockBinding++;
    }
}

async function createTextures()
{
    // Flip image pixels into the bottom-to-top order that WebGL expects.
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

    for (let key in imagesSrc)
    {
        const img = await loadImage(key);
        textures[key] = Texture.loadFromImage(img, key === 'background');
    }
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, false);
}

function loadImage(key)
{
    return new Promise((resolve, reject) => {
      const img = new Image();
      img.onload = () => resolve(img);
      img.onerror = reject;
      img.src = imagesSrc[key];
    });
}

function prepareLinks()
{
    for (let i = 0; i < mapData["cities"].length; i++)
    {
        if (!(i in distancesDict))
        {
            distancesDict[i] = {};
            vectorsLinksDict[i] = {};
        }
        let city = mapData["cities"][i];
        cityNameToId[city["name"]] = i;
        let x = city["x"];
        let y = city["y"];
        let adjCities = city["adj"];
        for (let adj of adjCities)
        {
            if( adj > i)
            {
                let adjCity = mapData["cities"][adj];
                let deltax = adjCity["x"] - x;
                let deltay = adjCity["y"] - y;
                let norm = Math.sqrt(deltax * deltax + deltay* deltay)
                distancesDict[i][adj] = norm;
                vectorsLinksDict[i][adj] = [deltax / norm, deltay / norm];
                if (!(adj in distancesDict))
                {
                    distancesDict[adj] = {};
                    vectorsLinksDict[adj] = {};
                }
                distancesDict[adj][i] = norm;
                vectorsLinksDict[adj][i] = [- deltax / norm, - deltay / norm]
            }
        }
    }

    let linksVertices = [];
    let linksIndices = [];

    const thickness = 1.5;
    const deltaCity = 16.;
    let i = 0;
    for (const [id_city_1, value] of Object.entries(distancesDict))
    {
        for (const [id_city_2, distance] of Object.entries(value))
        {
            if(id_city_1 < id_city_2)
            {
                let city_1 = mapData["cities"][id_city_1];
                let city_2 = mapData["cities"][id_city_2];
                let vectorLink = vectorsLinksDict[id_city_1][id_city_2];

                let x1 = MAP_SCALE_FACTOR * city_1["x"] + deltaCity * vectorLink[0];
                let y1 = MAP_SCALE_FACTOR * city_1["y"] + deltaCity * vectorLink[1];
                let x2 = MAP_SCALE_FACTOR * city_2["x"] - deltaCity * vectorLink[0];
                let y2 = MAP_SCALE_FACTOR * city_2["y"] - deltaCity * vectorLink[1];
                let dx = thickness * vectorLink[0];
                let dy = thickness * vectorLink[1];

                linksVertices.push(x1 - dy, y1 + dx, 1., 0.);
                linksVertices.push(x1 + dy, y1 - dx, -1., 0.);
                linksVertices.push(x2 + dy, y2 - dx, -1., distance);
                linksVertices.push(x2 - dy, y2 + dx, 1., distance);
                linksIndices.push(4 * i, 4 * i + 1, 4 * i + 2);
                linksIndices.push(4 * i, 4 * i + 2, 4 * i + 3);
                i++;
            }
        }
    }

    linksMesh = new IndicedMesh2d(
        new VertexBuffer(linksVertices), new IndexBuffer(linksIndices), shaders['links']);
}


function prepareSprites()
{
    let spriteSheetWidth = textures[spritesData["cities"]["source"]].width;
    let spriteSheetHeight = textures[spritesData["cities"]["source"]].height;
    citiesSprites["city"] = prepareSpriteArray(
        spritesData["cities"]["sprites"]["city"], 
        spriteSheetWidth, spriteSheetHeight,
        false
    );
    citiesSprites["towers"] = [];
    for(const towerData of spritesData["cities"]["sprites"]["towers"]) 
    {
        citiesSprites["towers"].push(
            prepareSpriteArray(towerData, spriteSheetWidth, spriteSheetHeight, true)
        );
    }
    citiesSprites["houses"] = [];
    for(const housesList of spritesData["cities"]["sprites"]["houses"]) 
    {
        spritesArrays = [];
        for (const houseData of housesList)
        {
            spritesArrays.push(
                prepareSpriteArray(houseData, spriteSheetWidth, spriteSheetHeight, true)
            )
        }
        citiesSprites["houses"].push(spritesArrays);
    }
        
    for (const [groundName, spriteData] of Object.entries(spritesData["ground"]["sprites"]))
    {
        groundSprites[groundName] = prepareSpriteArray(
            spriteData, 
            textures[spritesData["ground"]["source"]].width,
            textures[spritesData["ground"]["source"]].height,
            false
        );
        if(groundSprites[groundName][0] < groundMinMax[0]) groundMinMax[0] = groundSprites[groundName][0];
        if(groundSprites[groundName][2] > groundMinMax[1]) groundMinMax[1] = groundSprites[groundName][2];
        if(groundSprites[groundName][4] < groundMinMax[2]) groundMinMax[2] = groundSprites[groundName][4];
        if(groundSprites[groundName].length > 8) 
        {
            if(groundSprites[groundName][8] > groundMinMax[3]) groundMinMax[3] = groundSprites[groundName][8];
        }
        else
        {
            if(groundSprites[groundName][6] > groundMinMax[3]) groundMinMax[3] = groundSprites[groundName][6];
        }
    }

    for (const [name, spriteData] of Object.entries(spritesData["landscape"]["sprites"]))
    {
        landscapeSprites[name] = prepareSpriteArray(
            spriteData, 
            textures[spritesData["landscape"]["source"]].width,
            textures[spritesData["landscape"]["source"]].height,
            true
        );
        if(landscapeSprites[name][0] < landscapeMinMax[0]) landscapeMinMax[0] = landscapeSprites[name][0];
        if(landscapeSprites[name][2] > landscapeMinMax[1]) landscapeMinMax[1] = landscapeSprites[name][2];
        if(landscapeSprites[name][4] < landscapeMinMax[2]) landscapeMinMax[2] = landscapeSprites[name][4];
        if(landscapeSprites[name].length > 8) 
        {
            if(landscapeSprites[name][8] > landscapeMinMax[3]) landscapeMinMax[3] = landscapeSprites[name][8];
        }
        else
        {
            if(landscapeSprites[name][6] > landscapeMinMax[3]) landscapeMinMax[3] = landscapeSprites[name][6];
        }
    }

    for (const spriteData of spritesData["waves"]["sprites"])
    {
        wavesSprites.push(prepareSpriteArray(
            spriteData, 
            textures[spritesData["waves"]["source"]].width,
            textures[spritesData["waves"]["source"]].height,
            false
        ));
    }

    for(const [cellName, cellDataList] of Object.entries(spritesData["lake"]["sprites"])) 
    {
        lakesSprites[cellName] = [];
        for (const cellData of cellDataList)
        {
            lakesSprites[cellName].push(
                prepareMarchingSquareSpriteArray(
                    cellData[0], cellData[1], spritesData["lake"]["tile_size"],
                    textures[spritesData["lake"]["source"]].width,
                    textures[spritesData["lake"]["source"]].height
                )
            );
        }
    }
}

function prepareSpriteArray(spriteData, spriteSheetWidth, spriteSheetHeight, needDepth)
{
    let deltaXLeft = (spriteData.length === 5) ? Math.round(spriteData[2] / 3.) : Math.round(spriteData[2] / 2);
    let deltaXRight = spriteData[2] - deltaXLeft;
    let deltaYDown = (spriteData.length === 5) ? spriteData[4] : Math.round(spriteData[3] / 2);
    let deltaYUp = spriteData[3] - deltaYDown;

    let spriteArray = [];

    spriteArray.push(-deltaXLeft, (spriteData[0] + 0.5) / spriteSheetWidth);
    spriteArray.push(deltaXRight, (spriteData[0] + spriteData[2] + 0.5) / spriteSheetWidth);

    spriteArray.push(-deltaYDown, (spriteSheetHeight - spriteData[1] - spriteData[3] + 0.5) / spriteSheetHeight);
    if((needDepth)&&(spriteData.length === 5))
    {
        spriteArray.push(0., (spriteSheetHeight - spriteData[1] - spriteData[3] + spriteData[4] + 0.5) / spriteSheetHeight);
    }
    spriteArray.push(deltaYUp, (spriteSheetHeight - spriteData[1] + 0.5) / spriteSheetHeight);

    return spriteArray;
}

function prepareMarchingSquareSpriteArray(xSprite, ySprite, spriteSize, spriteSheetWidth, spriteSheetHeight)
{
    let spriteArray = [];

    spriteArray.push(0., (xSprite * spriteSize + 0.5) / spriteSheetWidth);
    spriteArray.push(spriteSize, (xSprite * spriteSize + spriteSize + 0.5) / spriteSheetWidth);
    spriteArray.push(0., (spriteSheetHeight - ySprite * spriteSize - spriteSize + 0.5) / spriteSheetHeight);
    spriteArray.push(spriteSize, (spriteSheetHeight - ySprite * spriteSize + 0.5) / spriteSheetHeight);

    return spriteArray;
}


function addSprite(vertices, indices, firstIndex, x, y, spriteArray)
{
    let nbVertices = 4;

    vertices.push(x + spriteArray[0], y + spriteArray[4], y + spriteArray[4]);
    vertices.push(spriteArray[1], spriteArray[5]);
    vertices.push(x + spriteArray[2], y + spriteArray[4], y + spriteArray[4]);
    vertices.push(spriteArray[3], spriteArray[5]);

    vertices.push(x + spriteArray[0], y + spriteArray[6], y + spriteArray[6]);
    vertices.push(spriteArray[1], spriteArray[7]);
    vertices.push(x + spriteArray[2], y + spriteArray[6], y + spriteArray[6]);
    vertices.push(spriteArray[3], spriteArray[7]);

    indices.push(firstIndex, firstIndex + 1, firstIndex + 2);
    indices.push(firstIndex + 1, firstIndex + 3, firstIndex + 2);

    if(spriteArray.length == 10)
    {
        nbVertices += 2;
        
        vertices.push(x + spriteArray[0], y + spriteArray[8], y + spriteArray[6]);
        vertices.push(spriteArray[1], spriteArray[9]);
        vertices.push(x + spriteArray[2], y + spriteArray[8], y + spriteArray[6]);
        vertices.push(spriteArray[3], spriteArray[9]);
        
        indices.push(firstIndex + 2, firstIndex + 3, firstIndex + 4);
        indices.push(firstIndex + 3, firstIndex + 5, firstIndex + 4);
    } 

    return nbVertices; 
}


function prepareEasterEggs()
{
    const owner = new Lord(InvalidDBId, InvalidDBId, -1, "King", 'Mysterious');
    let x, y;
    mapSide = GetRandomInRange(4);
    if(mapSide % 2)
    {
        x = GetRandomInRange(MAP_SCALE_FACTOR * (mapData['map_width'] - 2)) + MAP_SCALE_FACTOR;
    }
    else if (x === 0)
    {
        x = MAP_SCALE_FACTOR + GetRandomInRange(MAP_SCALE_FACTOR * 8);
    }
    else
    {
        x = MAP_SCALE_FACTOR  * (mapData['map_width'] - 1) - GetRandomInRange(MAP_SCALE_FACTOR * 8);
    }
    
    if(mapSide % 2 === 0)
    {
        y = GetRandomInRange(MAP_SCALE_FACTOR * (mapData['map_height'] - 2) + MAP_SCALE_FACTOR);
    }
    else if (mapSide === 1)
    {
        y = MAP_SCALE_FACTOR + GetRandomInRange(MAP_SCALE_FACTOR * 8);
    }
    else
    {
        y = MAP_SCALE_FACTOR * (mapData['map_height'] - 2) - GetRandomInRange(MAP_SCALE_FACTOR * 8);
    }
    let kingAltitude = GetRandomInRange(10);
    generalEasterEgg = new General(1, "WIP", owner, mapData["cities"][0]["name"], 1000, false);
    generalEasterEgg.setLocation(x, y);

    let spriteData = spritesData["easter_eggs"]["sprites"]["mountain_king"];
    let spriteArray = prepareSpriteArray(
        spriteData, 
        textures[spritesData["easter_eggs"]["source"]].width,
        textures[spritesData["easter_eggs"]["source"]].height,
        true
    );
    let vertexBuffer = new VertexBuffer([
        x + spriteArray[0], y + spriteArray[4], y + spriteArray[4] - kingAltitude,
        spriteArray[1], spriteArray[5],
        x + spriteArray[2], y + spriteArray[4], y + spriteArray[4] - kingAltitude,
        spriteArray[3], spriteArray[5],

        x + spriteArray[0], y + spriteArray[6], y + spriteArray[6] - kingAltitude,
        spriteArray[1], spriteArray[7],
        x + spriteArray[2], y + spriteArray[6], y + spriteArray[6] - kingAltitude,
        spriteArray[3], spriteArray[7],

        x + spriteArray[0], y + spriteArray[8], y + spriteArray[6] - kingAltitude,
        spriteArray[1], spriteArray[9],
        x + spriteArray[2], y + spriteArray[8], y + spriteArray[6] - kingAltitude,
        spriteArray[3], spriteArray[9]
    ]);


    let indexBuffer = new IndexBuffer([0,1,3,0,3,2,2,3,5,2,5,4]);
    let selectionVertexBuffer = new VertexBuffer([
        x + spriteArray[0], y + spriteArray[4],
        x + spriteArray[2], y + spriteArray[4],
        x + spriteArray[0], y + spriteArray[8],
        x + spriteArray[2], y + spriteArray[8],
    ]);
    let selectionIndexBuffer = new IndexBuffer([0,1,3,0,3,2]);

    easterEggsMesh = new IndicedMesh2d(vertexBuffer, indexBuffer, shaders['images']);
    easterEggMeshSelection = new IndicedMesh2d(selectionVertexBuffer, selectionIndexBuffer, shaders['easterEggSelection']);
}

