// ETERNAL KINGDOM - Offscreen rendering of map zones

/**
 * Draw the minimum distance in pixel to links or cities for every point in the map
 *
 */
function drawLinksDistance()
{
    let linksVertices = [];
    let linksIndices = [];

    const maxDist = 256 / MAP_SCALE_FACTOR;
    let i = 0;
    for (const [id_city_1, value] of Object.entries(vectorsLinksDict))
    {
        for (const [id_city_2, vectorLink] of Object.entries(value))
        {
            if(id_city_1 < id_city_2)
            {
                let city_1 = mapData["cities"][id_city_1];
                let city_2 = mapData["cities"][id_city_2];
                let x1 = city_1["x"];
                let y1 = city_1["y"];
                let x2 = city_2["x"];
                let y2 = city_2["y"];
                let dx = maxDist * vectorLink[0];
                let dy = maxDist * vectorLink[1];
                linksVertices.push(x1 - dy, y1 + dx, 1.);
                linksVertices.push(x1 + dy, y1 - dx, -1.);
                linksVertices.push(x2 + dy, y2 - dx, -1.);
                linksVertices.push(x2 - dy, y2 + dx, 1.);
                linksIndices.push(4 * i, 4 * i + 1, 4 * i + 2);
                linksIndices.push(4 * i, 4 * i + 2, 4 * i + 3);
                i++;
            }
        }
    }

    let vertexBuffer = new VertexBuffer(linksVertices);
    let indexBuffer = new IndexBuffer(linksIndices);
    let linksMesh = new IndicedMesh2d(vertexBuffer, indexBuffer, shaders['distToLinks']);
    linksMesh.draw();
    linksMesh.deleteGlObject();
    vertexBuffer.deleteGlObject();
    indexBuffer.deleteGlObject();
}

/**
 * Draw the minimum distance in pixel to cities for every point in the map
 *
 */
function drawCitiesDistance()
{
    let citiesDistVertices = [];
    let citiesAreaVertices = [];
    let citiesDistIndices = [];
    const maxDist = 256 / MAP_SCALE_FACTOR;

    for(let i = 0; i < mapData["cities"].length; i++)
    {
        let city = mapData["cities"][i];
        let x = city["x"];
        let y = city["y"];
        citiesDistVertices.push(x, y, 0.);
        citiesDistVertices.push(x - 2 * maxDist, y, 1.);
        citiesDistVertices.push(x - maxDist, y - maxDist, 1.);
        citiesDistVertices.push(x + maxDist, y - maxDist, 1.);
        citiesDistVertices.push(x + 2. * maxDist, y, 1.);
        citiesDistVertices.push(x + maxDist, y + maxDist, 1.);
        citiesDistVertices.push(x - maxDist, y + maxDist, 1.);

        let forbiddenDist = city["cap"] ? 40 / MAP_SCALE_FACTOR : 24 / MAP_SCALE_FACTOR;
        citiesAreaVertices.push(x, y, 0.);
        citiesAreaVertices.push(x - 2 * forbiddenDist, y, 0.);
        citiesAreaVertices.push(x - forbiddenDist, y - forbiddenDist, 0.);
        citiesAreaVertices.push(x + forbiddenDist, y - forbiddenDist, 0.);
        citiesAreaVertices.push(x + 2. * forbiddenDist, y, 0.);
        citiesAreaVertices.push(x + forbiddenDist, y + forbiddenDist, 0.);
        citiesAreaVertices.push(x - forbiddenDist, y + forbiddenDist, 0.);

        for(let j = 1; j < 6; j++)
        {
            citiesDistIndices.push(7 * i, 7 * i + j, 7 * i + j + 1);
        }
        citiesDistIndices.push(7 * i, 7 * i + 6, 7 * i + 1);
    }

    let distVertexBuffer = new VertexBuffer(citiesDistVertices);
    let areaVertexBuffer = new VertexBuffer(citiesAreaVertices);
    let indexBuffer = new IndexBuffer(citiesDistIndices);
    let citiesDistMesh = new IndicedMesh2d(distVertexBuffer, indexBuffer, shaders['distToLinks']);
    let citiesAreaMesh = new IndicedMesh2d(areaVertexBuffer, indexBuffer, shaders['distToLinks']);

    citiesDistMesh.draw();
    citiesAreaMesh.draw();
    citiesDistMesh.deleteGlObject();
    citiesAreaMesh.deleteGlObject();
    distVertexBuffer.deleteGlObject();
    areaVertexBuffer.deleteGlObject();
    indexBuffer.deleteGlObject();
}

/**
 * Render an offscreen texture, with for each point of the map:
 *  - red: the minimum distance in pixel to cities or links
 *  - green, blue: a pseudo-random value
 *
 * @param {number} mapGenWidth - the width of the generated map
 * @param {number} mapGenHeight - the height of the generated map
 */
function renderDistAndRandomFramebuffer(mapGenWidth, mapGenHeight)
{
    let distAndRandomFramebuffer = new Framebuffer(mapGenWidth, mapGenHeight, false);
    distAndRandomFramebuffer.bindAndSetViewport();
    gl.clearColor(1.0, 0.0, 0.0, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT);

    gl.colorMask(true, false, false, false);
    gl.enable(gl.BLEND);
    gl.blendEquation(gl.MIN);
    shaders['distToLinks'].use();
    drawLinksDistance();
    drawCitiesDistance();
    gl.disable(gl.BLEND);

    gl.colorMask(false, true, true, false);
    shaders['randomGreenBlue'].use();
    let fullscreenTriangle = 
        new Mesh2d(new VertexBuffer([-1.0, -1.0, 3.0, -1.0, -1.0, 3.0]), shaders['randomGreenBlue']);
        fullscreenTriangle.draw();

    gl.colorMask(true, true, true, true);

    return distAndRandomFramebuffer;
}

/**
 * Create and return a framebuffer containing for each point an average distance that will be used to evaluate
 * if the point in a lake or not.
 * 
 * @param {VertexBuffer} citiesVertexBuffer - VertexBuffer of the cities positions
 * @param {IndexBuffer} lakesIndexBuffer - IndexBuffer of the areas containing lakes
 * @param {Texture} distAndRandomTex - texture containing for each point the minimum distance to 
 * @param {number} mapGenWidth - the width of the generated map
 * @param {number} mapGenHeight - the height of the generated map
 * @returns {Framebuffer} - a frame buffer containing average distances
 */
function renderDistLakes(citiesVertexBuffer, lakesIndexBuffer, distAndRandomTex, mapGenWidth, mapGenHeight)
{
    let lakesSpriteSize = spritesData['lake']['tile_size'];
    let inLakesFramebufferWidth = mapGenWidth * MAP_SCALE_FACTOR / lakesSpriteSize;
    let inLakesFramebufferHeight = mapGenHeight * MAP_SCALE_FACTOR / lakesSpriteSize;
    let inLakesFramebuffer = new Framebuffer(inLakesFramebufferWidth, inLakesFramebufferHeight,  false);
    inLakesFramebuffer.bindAndSetViewport();
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.bindTexture(gl.TEXTURE_2D, distAndRandomTex);

    let lakes = new IndicedMesh2d(citiesVertexBuffer, lakesIndexBuffer, shaders['inLakes']);

    shaders['inLakes'].use();
    gl.uniform1i(shaders['inLakes'].uniformLoc['u_distAndRandomTex'], 0);
    gl.uniform1f(shaders['inLakes'].uniformLoc['u_minDist'], 20. / 255.);

    lakes.draw();
    lakes.deleteGlObject();

    return inLakesFramebuffer;
}

/**
 * Render sprites id for lakes in the ZoneFramebuffer
 * 
 * @param {VertexBuffer} citiesVertexBuffer - VertexBuffer of the cities positions
 * @param {IndexBuffer} lakesIndexBuffer - IndexBuffer of the areas containing lakes
 */
function drawLakes(citiesVertexBuffer, lakesIndexBuffer)
{
    shaders['lakesZones'].use();
    gl.uniform1i(shaders['lakesZones'].uniformLoc['u_inLakeTex'], 0);
    gl.uniform1i(shaders['lakesZones'].uniformLoc['u_landform'], 7);
    gl.uniform1f(shaders['lakesZones'].uniformLoc['u_threshold'], 9. / 255.);

    let lakes = new IndicedMesh2d(citiesVertexBuffer, lakesIndexBuffer, shaders['lakesZones']);
    lakes.draw();
    lakes.deleteGlObject();
}

/**
 * Render sprites id for mountains and deserts in the ZoneFramebuffer
 * 
 * @param {VertexBuffer} citiesVertexBuffer - VertexBuffer of the cities positions
 * @param {IndexBuffer} lakesIndexBuffer - IndexBuffer of the areas containing lakes
 * @param {number} mapGenWidth - the width of the generated map
 * @param {number} mapGenHeight - the height of the generated map 
 */
function drawMountainsDesert(citiesVertexBuffer, mapGenWidth, mapGenHeight)
{
    shaders['mountainsDesertZones'].use();
    gl.uniform1i(shaders['mountainsDesertZones'].uniformLoc['u_distAndRandomTex'], 0);
    let mountainsVertexBuffer = new VertexBuffer([0., 0., 0., mapGenHeight, mapGenWidth, mapGenHeight, mapGenWidth, 0.]);
    let mountainsIndexBuffer = new IndexBuffer([0, 1, 2, 0, 2, 3]);
    let mountainsMesh = new IndicedMesh2d(mountainsVertexBuffer, mountainsIndexBuffer, shaders['mountainsDesertZones']);

    gl.uniform1f(shaders['mountainsDesertZones'].uniformLoc['u_minDist'], 26. / 256.);
    gl.uniform2f(shaders['mountainsDesertZones'].uniformLoc['u_maxRandValue'], 1./15., 1./3.);
    gl.uniform1i(shaders['mountainsDesertZones'].uniformLoc['u_landform'], 0);

    mountainsMesh.draw();
    mountainsVertexBuffer.deleteGlObject();
    mountainsIndexBuffer.deleteGlObject();
    mountainsMesh.deleteGlObject();
    
    let indices = mapData['landforms']['snowy_mountains'];
    if(indices != null)
    {
        indexBuffer = new IndexBuffer(indices);
        zones = new IndicedMesh2d(citiesVertexBuffer, indexBuffer, shaders['mountainsDesertZones']);
        gl.uniform1f(shaders['mountainsDesertZones'].uniformLoc['u_minDist'], 26. / 255.);
        gl.uniform2f(shaders['mountainsDesertZones'].uniformLoc['u_maxRandValue'], 1./20., 1./3.);
        gl.uniform1i(shaders['mountainsDesertZones'].uniformLoc['u_landform'], 1);
        zones.draw();
        zones.deleteGlObject();
        indexBuffer.deleteGlObject();
    }
    
    indices = mapData['landforms']['volcanos'];
    if(indices != null)
    {
        indexBuffer = new IndexBuffer(indices);
        zones = new IndicedMesh2d(citiesVertexBuffer, indexBuffer, shaders['mountainsDesertZones']);
        gl.uniform1f(shaders['mountainsDesertZones'].uniformLoc['u_minDist'], 26. / 255.);
        gl.uniform2f(shaders['mountainsDesertZones'].uniformLoc['u_maxRandValue'], 1./10., 1./5.);
        gl.uniform1i(shaders['mountainsDesertZones'].uniformLoc['u_landform'], 2);
        zones.draw();
        zones.deleteGlObject();
        indexBuffer.deleteGlObject();
    }

    indices = mapData['landforms']['sand'];
    if(indices != null)
    {
        indexBuffer = new IndexBuffer(indices);
        zones = new IndicedMesh2d(citiesVertexBuffer, indexBuffer, shaders['mountainsDesertZones']);
        gl.uniform1f(shaders['mountainsDesertZones'].uniformLoc['u_minDist'], 26. / 255.);
        gl.uniform2f(shaders['mountainsDesertZones'].uniformLoc['u_maxRandValue'], 1./10., 1./15.);
        gl.uniform1i(shaders['mountainsDesertZones'].uniformLoc['u_landform'], 3);
        zones.draw();
        zones.deleteGlObject();
        indexBuffer.deleteGlObject();
    }
}

/**
 * Render sprites id for dirt in the ZoneFramebuffer
 * 
 * @param {VertexBuffer} citiesVertexBuffer - VertexBuffer of the cities positions
 * @param {IndexBuffer} lakesIndexBuffer - IndexBuffer of the areas containing lakes
 */
function drawDirt(citiesVertexBuffer)
{
    let indices = mapData['landforms']['dirt'];
    if(indices != null)
    {
        shaders['dirtZones'].use();
        gl.uniform1i(shaders['dirtZones'].uniformLoc['u_distAndRandomTex'], 0);
        indexBuffer = new IndexBuffer(indices);
        zones = new IndicedMesh2d(citiesVertexBuffer, indexBuffer, shaders['dirtZones']);
        gl.uniform1f(shaders['dirtZones'].uniformLoc['u_minDist'], 15. / 255.);
        gl.uniform1f(shaders['dirtZones'].uniformLoc['u_maxRandValue'], 1./50.);
        gl.uniform1i(shaders['dirtZones'].uniformLoc['u_landform'], 4);
        zones.draw();
        zones.deleteGlObject();
        indexBuffer.deleteGlObject();
    }
}

/**
 * Render sprites id for forest and hills in the ZoneFramebuffer
 * 
 * @param {VertexBuffer} citiesVertexBuffer - VertexBuffer of the cities positions
 * @param {IndexBuffer} lakesIndexBuffer - IndexBuffer of the areas containing lakes
 */
function drawForestHills(citiesVertexBuffer)
{
    shaders['forestHillsZones'].use();
    gl.uniform1i(shaders['forestHillsZones'].uniformLoc['u_distAndRandomTex'], 0);
    let indices = mapData['landforms']['forest'];
    if(indices != null)
    {
        indexBuffer = new IndexBuffer(indices);
        zones = new IndicedMesh2d(citiesVertexBuffer, indexBuffer, shaders['forestHillsZones']);
        gl.uniform3f(shaders['forestHillsZones'].uniformLoc['u_randRangeValues'], 1./20., 5./255., 20./255.);
        gl.uniform1i(shaders['forestHillsZones'].uniformLoc['u_landform'], 5);
        zones.draw();
        zones.deleteGlObject();
        indexBuffer.deleteGlObject();
    }

    indices = mapData['landforms']['hills'];
    if(indices != null)
    {
        indexBuffer = new IndexBuffer(indices);
        zones = new IndicedMesh2d(citiesVertexBuffer, indexBuffer, shaders['forestHillsZones']);
        gl.uniform3f(shaders['forestHillsZones'].uniformLoc['u_randRangeValues'], 1./20., 10./255., 11.5/255.);
        gl.uniform1i(shaders['forestHillsZones'].uniformLoc['u_landform'], 6);
        zones.draw();
        zones.deleteGlObject();
        indexBuffer.deleteGlObject();
    }
}

/**
 * Create and render a framebuffer with sprites ids.
 * 
 * @param {number} mapGenWidth - the width of the generated map
 * @param {number} mapGenHeight - the height of the generated map
 * @returns {Framebuffer} - a framebuffer with the type of zone (red chanel), 
 * sprite id for the ground (green chanel), sprite id for the landscape (blue chanel)
 */
function renderZonesFramebuffer(mapGenWidth, mapGenHeight)
{
    let distAndRandomFramebuffer = renderDistAndRandomFramebuffer(mapGenWidth, mapGenHeight);
    let citiesVertices = [];
    for (const city of mapData["cities"])
    {
        citiesVertices.push(city["x"]);
        citiesVertices.push(city["y"]);
    }
    let citiesVertexBuffer = new VertexBuffer(citiesVertices);

    let lakesIndices = mapData['landforms']['lake'];
    let lakesIndexBuffer;
    let inLakesFramebuffer;
    if (lakesIndices != null)
    {
        lakesIndexBuffer = new IndexBuffer(lakesIndices);
        inLakesFramebuffer = 
            renderDistLakes(citiesVertexBuffer, lakesIndexBuffer, distAndRandomFramebuffer.texture, mapGenWidth, mapGenHeight);
    } 
    let zonesFramebuffer = new Framebuffer(mapGenWidth, mapGenHeight, false);
    zonesFramebuffer.bindAndSetViewport();
    gl.clear(gl.COLOR_BUFFER_BIT);

    gl.bindTexture(gl.TEXTURE_2D, distAndRandomFramebuffer.texture);
    drawMountainsDesert(citiesVertexBuffer, mapGenWidth, mapGenHeight);
    drawDirt(citiesVertexBuffer);
    drawForestHills(citiesVertexBuffer);

    if (lakesIndices != null)
    {
        gl.bindTexture(gl.TEXTURE_2D, inLakesFramebuffer.texture);
        drawLakes(citiesVertexBuffer, lakesIndexBuffer);
        lakesIndexBuffer.deleteGlObject();
    }

    citiesVertexBuffer.deleteGlObject();

    return zonesFramebuffer;
}
