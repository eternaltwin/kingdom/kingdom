/** Map<string A, [String B]>
 * string A : Language
 * String B : Array of Map Names
 */
let worldMapNames;

// Buffer for the current world json selected
let selectedWorldJson;

let languageSelector = null;
let worldNameSelector = null;
let worldDataSelector = null;
let slotCountInput = null;

let currentLanguage;
let currentSlotCount = 0;
let slotCountMax = 0;

if (document.readyState === "complete" ||
    (document.readyState !== "loading" && !document.documentElement.doScroll)) {
    init();
} else {
    document.addEventListener("DOMContentLoaded", init);
}

function init()
{
    languageSelector = document.getElementById("lang");
    worldNameSelector = document.getElementById("worldName");
    worldDataSelector = document.getElementById("worldDataName");
    slotCountInput = document.getElementById("slotCount");

    loadJson().then(() =>
    {
        onChangedLanguage();
        onChangedWorldData();


        console.log(worldDataSelector.value);
        console.log(worldNameSelector.value);
        console.log(slotCountInput.value);
    });
}

function onChangedLanguage()
{
    let previousLanguage = currentLanguage;
    currentLanguage = languageSelector.value.toLowerCase();

    if (currentLanguage !== previousLanguage) {
        updateWorldMapNameSelector();
    }
}

function onChangedWorldData()
{
    loadWorldJson(worldDataSelector.value).then(() =>
    {
        let nodesBuffer = selectedWorldJson["cities"];
        let worldMaxSlotCount = 0;
        for(let i = 0; i < nodesBuffer.length; i++)
        {
            if (nodesBuffer[i]['cap'])
            {
                worldMaxSlotCount++;
            }
        }

        slotCountMax = worldMaxSlotCount;
        slotCountInput.value = slotCountMax;

        if (currentLanguage !== selectedWorldJson['language'])
        {
            let language = selectedWorldJson['language'];
            let newLanguage = language.charAt(0).toUpperCase() + language.slice(1);
            if (confirm(`World Data is in ${newLanguage}, do you want to change the current lang?`))
            {
                languageSelector.value = newLanguage;
                onChangedLanguage();
            }
        }

        let worldInfo = document.getElementById("worldInfo");
        let mapType = selectedWorldJson['map_type'].toString();
        let mapWidth = selectedWorldJson['map_width'];
        let mapHeight = selectedWorldJson['map_height'];
        worldInfo.innerText = 'Type: '+mapType+', Width: '+mapWidth+', Height: '+mapHeight + ', Nodes: ' + nodesBuffer.length;
    });
}

function onChangedSlotCount()
{
    let currentSlotCount = parseInt(slotCountInput.value);
    slotCountInput.value = clamp(currentSlotCount, 1, slotCountMax);
}

function updateWorldMapNameSelector()
{
    let namesBuffer = worldMapNames[currentLanguage];
    worldNameSelector.replaceChildren();
    for (let i = 0; i < namesBuffer.length; i++)
    {
        let option = document.createElement("option");
        option.text = namesBuffer[i];
        option.value = namesBuffer[i];
        worldNameSelector.appendChild(option);
    }
}

async function loadJson()
{
    let worldMapNamesData = '/maps/map_names.json';
    const [worldMapNamesResponse] = await Promise.all([fetch(worldMapNamesData)]);

    worldMapNames = await worldMapNamesResponse.json();
}

async function loadWorldJson(name)
{
    let worldData = '/maps/' + name + '.json';
    const [worldDataResponse] = await Promise.all([fetch(worldData)]);

    selectedWorldJson = await worldDataResponse.json();
}