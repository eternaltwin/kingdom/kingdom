function renameGeneral(generalId)
{
    let newName = prompt("Comment voulez-vous renommer ce général?");

    if (newName !== "")
    {
        let payload = {
            generalId : generalId,
            newName: newName
        }
        ServerPost('/admin/generals/rename', payload, handleGeneralRenameResponse);
    }
}

function resetGeneral(generalId)
{
    let payload = {
        generalId : generalId
    }
    ServerPost('/admin/generals/unlock', payload, handleGeneralRenameResponse);
}

function handleGeneralRenameResponse(data)
{
    if (data['success'])
    {
        location.reload();
    }
}