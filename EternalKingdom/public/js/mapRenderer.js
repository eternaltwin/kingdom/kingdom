// ETERNAL KINGDOM - Map rendering

/**
 * Class to represent the position of the view in the map
 */
class ViewPosition
{
    //x and y are the coordinates of the center of the view
    x;
    y;
    xMin;
    xMax;
    yMin;
    yMax;

    // 1/zoom is the magnification. The larger is the zoom, the larger part of the map you can see.
    zoom;
    zoomMax;

    constructor()
    {
        this.zoom = 1;
        this.zoomMax = 1;
        this.x = MAP_SCALE_FACTOR * mapData['map_width'] / 2.;
        this.y = MAP_SCALE_FACTOR * mapData['map_height'] / 2.;
        while ((MAP_SCALE_FACTOR * mapData['map_width'] / this.zoomMax > gl.canvas.width) 
            && (MAP_SCALE_FACTOR * mapData['map_height'] / this.zoomMax > gl.canvas.height)) {
            this.zoomMax *= 2;
        }
        this.xMin = 0.;
        this.yMin = 0.;
        this.xMax = mapData['map_width'] * MAP_SCALE_FACTOR - this.xMin;
        this.yMax = mapData['map_height'] * MAP_SCALE_FACTOR - this.yMin;
        this.#clampViewPosition();
    }

    setPosition(x, y)
    {
        this.x = x;
        this.y = y;
        this.#clampViewPosition();
        uniformBufferObjects['viewParameters'].sendSingleVariableData(
            'u_viewPosition', [this.x, this.y]
        );

    }

    drag(deltaX, deltaY)
    {
        this.setPosition(this.x + this.zoom * deltaX, this.y + this.zoom * deltaY);
    }

    zoomIn()
    {
        if (this.zoom > 1)
        {
            this.zoom /= 2;
            uniformBufferObjects['viewParameters'].sendSingleVariableData('u_zoom', [this.zoom]);
        }
    }

    zoomOut()
    {
        if (this.zoom < this.zoomMax)
        {
            this.zoom *= 2;
            uniformBufferObjects['viewParameters'].sendSingleVariableData('u_zoom', [this.zoom]);

        }
    }

    setPositionAndZoom(x, y, zoom)
    {
        this.x = x;
        this.y = y;
        if ((zoom >= 1) && (zoom <= this.zoomMax))
        {
            this.zoom = zoom;
            uniformBufferObjects['viewParameters'].sendSingleVariableData('u_zoom', [this.zoom]);
        }
        this.#clampViewPosition();
        uniformBufferObjects['viewParameters'].sendSingleVariableData(
            'u_viewPosition', [this.x, this.y]
        );
    }

    #clampViewPosition()
    {
        if (this.x < this.xMin)
        {
            this.x = this.xMin;
        }
        if (this.x > this.xMax)
        {
            this.x = this.xMax;
        }
        if (this.y < this.yMin)
        {
            this.y = this.yMin;
        }
        if (this.y > this.yMax)
        {
            this.y = this.yMax;
        }
    }
}


class MapRenderer
{
    mapGenWidth;
    mapGenHeight;
    nbChunksX;
    nbChunksY;
    nbChunkViewsX;
    nbChunkViewsY;
    
    playersChunkArray;
    chunkViewArray;
    chunkMesh;
    selectionFramebuffer;

    viewPosition;
    previousStep;
    animateRequestId;

    constructor(mapGenWidth, mapGenHeight)
    {
        ChunkView.width = chunkWidth;
        ChunkView.height = chunkHeight;

        this.mapGenWidth = mapGenWidth;
        this.mapGenHeight = mapGenHeight;

        this.nbChunksX = Math.ceil((mapGenWidth * MAP_SCALE_FACTOR) / chunkWidth);
        this.nbChunksY = Math.ceil((mapGenHeight * MAP_SCALE_FACTOR) / chunkHeight);
    
        //this.nbChunkViewsX  = Math.ceil(viewWidth / chunkWidth) + 1;
        //this.nbChunkViewsY = Math.ceil(viewHeight / chunkHeight) + 1;
        this.nbChunkViewsX  = this.nbChunksX;
        this.nbChunkViewsY = this.nbChunksY; //TODO: if the map is huge, do not prerender everything!

        this.viewPosition = new ViewPosition();
        this.previousStep = 0;

        let bgImgWidth = textures['background'].width;
        let bgImgHeight = textures['background'].height;
        let maxTexCoordX = ChunkView.width / bgImgWidth;
        let maxTexCoordY = ChunkView.height / bgImgHeight;
        const bgVertices = [
            -1.0, -1.0, 0.0, 0.0,
            3.0, -1.0, 2.0 * maxTexCoordX, 0.0,
            -1.0, 3.0, 0.0, 2.0 * maxTexCoordY];
        ChunkView.backgroundMesh = new Mesh2d(new VertexBuffer(bgVertices), shaders['background']);

        this.playersChunkArray = new Array(this.nbChunksX);
        for (let i = 0; i < this.nbChunksX; i++) {
            this.playersChunkArray[i] = new Array(this.nbChunksY);
            for (let j = 0; j < this.nbChunksY; j++) {
                this.playersChunkArray[i][j] = new PlayersDataChunk();
            }
        }
        this.chunkViewArray = new Array(this.nbChunkViewsX);
        for (let i = 0; i < this.nbChunkViewsX; i++) {
            this.chunkViewArray[i] = new Array(this.nbChunkViewsY);
            for (let j = 0; j < this.nbChunkViewsY; j++) {
                this.chunkViewArray[i][j] = new ChunkView(i, j);
                this.chunkViewArray[i][j].playersDataChunk = this.playersChunkArray[i][j];
            }
        }

        let chunkVertices = [
            0.0, 0.0, 0.5 / chunkWidth, 0.5 / chunkHeight,
            chunkWidth, 0.0, (chunkWidth - 0.5) / chunkWidth, 0.5 / chunkHeight,
            chunkWidth, chunkHeight, (chunkWidth - 0.5) / chunkWidth, (chunkHeight - 0.5) / chunkHeight,
            0.0, 0.0, 0.5 / chunkWidth, 0.5 / chunkHeight,
            chunkWidth, chunkHeight, (chunkWidth - 0.5) / chunkWidth, (chunkHeight - 0.5) / chunkHeight,
            0.0, chunkHeight, 0.5 / chunkWidth, (chunkHeight - 0.5) / chunkHeight
        ];
        this.chunkMesh = new Mesh2d(new VertexBuffer(chunkVertices), shaders['chunksToScreen']);
    
        this.selectionFramebuffer = new Framebuffer(gl.canvas.width, gl.canvas.height, false);
    }

    loadCitiesData()
    {
        let vertices = new Array(this.nbChunksX);
        let indices = new Array(this.nbChunksX);
        let nbCitiesVertices = new Array(this.nbChunksX);
        for (let i = 0; i < this.nbChunksX; i++)
        {
            vertices[i] = new Array(this.nbChunksY);
            indices[i] = new Array(this.nbChunksY);
            nbCitiesVertices[i] = new Array(this.nbChunksY);
            for (let j = 0; j < this.nbChunksY; j++) 
            {
                vertices[i][j] = [];
                indices[i][j] = [];
                nbCitiesVertices[i][j] = 0;
            }
        }

        for(const citySprite of citiesRenderableData.spriteList)
        {
            let x = citySprite.x;
            let y = citySprite.y;
            let spriteArray = citySprite.spriteData;
            let minX = Math.max(Math.floor((x + spriteArray[0]) / ChunkView.width), 0);
            let maxX = Math.min(Math.floor((x + spriteArray[2]) / ChunkView.height), this.nbChunksX - 1);
            let minY = Math.max(Math.floor((y + spriteArray[4]) / ChunkView.width), 0);
            let maxY = Math.min(Math.floor(((spriteArray.length > 8) ? y + spriteArray[8] : y + spriteArray[6]) / ChunkView.height), this.nbChunksY - 1);

            for (let i = minX; i <= maxX; i++) 
            {
                for (let j = minY; j <= maxY; j++) 
                {
                    nbCitiesVertices[i][j] += addSprite(vertices[i][j], indices[i][j], nbCitiesVertices[i][j], x, y, spriteArray);
                }
            }
        }

        for (let i = 0; i < this.nbChunksX; i++)
        {
            for (let j = 0; j < this.nbChunksY; j++) 
            {
                this.playersChunkArray[i][j].setCityMesh(
                    new IndicedMesh2d(new VertexBuffer(vertices[i][j]), new IndexBuffer(indices[i][j]), shaders['images'])
                );
            }
        }
    }


    prepareKingdomsMeshes()
    {
        let kingdomsAndFrontiers = new KingdomsAndFrontiers(this.nbChunksX, this.nbChunksY);

        let kingdomsVertexBuffer = new VertexBuffer(kingdomsAndFrontiers.voronoiVertices);

        let kingdomsColorMeshes = new Array(this.nbChunksX);
        for (let i = 0; i < this.nbChunksX; i++)
        {
            kingdomsColorMeshes[i] = new Array(this.nbChunksY);
            for (let j = 0; j < this.nbChunksY; j++) 
            {
                kingdomsColorMeshes[i][j] = {};
                for (const [playerId, indicesList] of Object.entries(kingdomsAndFrontiers.kingdomsColorIndices[i][j]))
                {
                    kingdomsColorMeshes[i][j][playerId] = new IndicedMesh2d(kingdomsVertexBuffer, new IndexBuffer(indicesList), shaders['kingdoms']);
                }
            }
        }

        let frontiersVertexBuffer = new VertexBuffer(kingdomsAndFrontiers.frontierVertices);

        let kingdomsFrontiersMeshes = new Array(this.nbChunksX);
        for (let i = 0; i < this.nbChunksX; i++)
        {
            kingdomsFrontiersMeshes[i] = new Array(this.nbChunksY);
            for (let j = 0; j < this.nbChunksY; j++) 
            {
                kingdomsFrontiersMeshes[i][j] = new IndicedMesh2d(frontiersVertexBuffer, new IndexBuffer(kingdomsAndFrontiers.frontierIndices[i][j]), shaders['frontiers']);
            }
        }


        for (let i = 0; i < this.nbChunksX; i++) {
            for (let j = 0; j < this.nbChunksY; j++) {
                this.playersChunkArray[i][j].setKingdomsMeshes(kingdomsColorMeshes[i][j], kingdomsFrontiersMeshes[i][j]);
            }
        }
    }


    //TODO: only render needed chunks, depending on scrolling
    renderChunks(zonesFramebuffer)
    {
        const zonePixels = zonesFramebuffer.bindAndReadPixels();
        for (let i = 0; i < this.nbChunkViewsX; i++)
        {
            for (let j = 0; j < this.nbChunkViewsY; j++)
            {
                this.chunkViewArray[i][j].render(zonePixels, zonesFramebuffer.width, zonesFramebuffer.height);
            }
        }
    }
    
    renderChunksSelection()
    {
        for (let i = 0; i < this.nbChunkViewsX; i++)
        {
            for (let j = 0; j < this.nbChunkViewsY; j++)
            {
                this.chunkViewArray[i][j].renderSelection();
            }
        }
    }

    #renderSelectionFramebuffer()
    {
        this.selectionFramebuffer.bindAndSetViewport();
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        shaders['chunksToSelection'].use();
        gl.uniform1i(shaders['chunksToSelection'].uniformLoc['u_texture'], 0);

        for (let i = 0; i < this.nbChunkViewsX; i++) {
            for (let j = 0; j < this.nbChunkViewsY; j++) {
                gl.bindTexture(gl.TEXTURE_2D, this.chunkViewArray[i][j].selectionFramebuffer.texture);
                gl.uniform2fv(
                    shaders['chunksToSelection'].uniformLoc['u_chunk_translation'],
                    [i * ChunkView.width, j * ChunkView.height]
                );
                this.chunkMesh.draw();
            }
        }
        shaders['generalsSelection'].use();
        generalsMeshes.generalsSelectionMesh.draw();
    }

    #renderMapChunks()
    {
        shaders['chunksToScreen'].use();
        gl.uniform1i(shaders['chunksToScreen'].uniformLoc['u_texture'], 0);
        gl.uniform1i(shaders['chunksToScreen'].uniformLoc['u_depth'], 1);
        
        for (let i = 0; i < this.nbChunkViewsX; i++)
        {
            for (let j = 0; j < this.nbChunkViewsY; j++)
            {
                gl.activeTexture(gl.TEXTURE1);
                gl.bindTexture(gl.TEXTURE_2D, this.chunkViewArray[i][j].renderFramebuffer.depthTexture);
                gl.activeTexture(gl.TEXTURE0);
                gl.bindTexture(gl.TEXTURE_2D, this.chunkViewArray[i][j].renderFramebuffer.texture);
                //gl.bindTexture(gl.TEXTURE_2D, this.chunkViewArray[i][j].selectionFramebuffer.texture);
                gl.uniform2fv(
                    shaders['chunksToScreen'].uniformLoc['u_chunk_translation'],
                    [i * ChunkView.width, j * ChunkView.height]
                );
                this.chunkMesh.draw();
            }
        }
    }

    #renderWaves()
    {
        shaders['waves'].use();
        textures['landscape'].bind();
        gl.uniform1i(shaders['waves'].uniformLoc['u_texture'], 0);
        for (let i = 0; i < this.nbChunkViewsX; i++)
        {
            for (let j = 0; j < this.nbChunkViewsY; j++)
            {
                if (this.chunkViewArray[i][j].wavesMesh != null)
                {
                    gl.uniform2fv(
                        shaders['waves'].uniformLoc['u_translation'], 
                        [i * ChunkView.width, j * ChunkView.height]
                    );
                    this.chunkViewArray[i][j].wavesMesh.draw();
                }
            }
        }
    }

    #renderGenerals()
    {
        shaders['generals'].use();
        gl.uniform1i(shaders['generals'].uniformLoc['u_texture'], 0);
        textures['generals'].bind();
        generalsMeshes.generalsMesh.draw();
    }

    #renderStrategicView()
    {
        shaders['strategic'].use();
        gl.activeTexture(gl.TEXTURE1);
        kingdomsColorTex.bind();
        gl.activeTexture(gl.TEXTURE0);
        gl.uniform1i(shaders['strategic'].uniformLoc['u_kingdomColorTex'], 1);
        gl.uniform1i(shaders['strategic'].uniformLoc['u_selectionTex'], 0);

        for (let i = 0; i < this.nbChunkViewsX; i++) {
            for (let j = 0; j < this.nbChunkViewsY; j++) {
                gl.bindTexture(gl.TEXTURE_2D, this.chunkViewArray[i][j].selectionFramebuffer.texture);
                gl.uniform2fv(
                    shaders['strategic'].uniformLoc['u_chunk_translation'],
                    [i * ChunkView.width, j * ChunkView.height]
                );
                this.chunkMesh.draw();
            }
        }
    }

    #renderSelectedKingdom(selectedKingdomId)
    {
        shaders['kingdomView'].use();
        gl.uniform1i(shaders['kingdomView'].uniformLoc['u_selectionTex'], 0);
        //TODO: vassal kingdoms
        gl.uniform1i(shaders['kingdomView'].uniformLoc['u_playerId'], selectedKingdomId);
        gl.uniform4f(shaders['kingdomView'].uniformLoc['u_kingdomColor'], 0., 0., 0., 0.4);

        for (let i = 0; i < this.nbChunkViewsX; i++) {
            for (let j = 0; j < this.nbChunkViewsY; j++) {
                gl.bindTexture(gl.TEXTURE_2D, this.chunkViewArray[i][j].selectionFramebuffer.texture);
                gl.uniform2fv(
                    shaders['kingdomView'].uniformLoc['u_chunk_translation'],
                    [i * ChunkView.width, j * ChunkView.height]
                );
                this.chunkMesh.draw();
            }
        }
    }

    #renderGeneralPath()
    {
        shaders['generalPath'].use();           
        generalPathMesh.draw();
    }

    renderMap()
    {
        gl.enable(gl.DEPTH_TEST);
        gl.depthFunc(gl.LEQUAL);

        this.#renderSelectionFramebuffer();

        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        this.#renderMapChunks();

        gl.enable(gl.BLEND);
        gl.blendEquation(gl.FUNC_ADD);
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
        gl.disable(gl.DEPTH_TEST);

        if (graphicsSettings.animate)
        {
            this.#renderWaves();
        }

        if (isStrategicView)
            this.#renderStrategicView();
        else
        {
            if (focusedKingdomLordDbID !== -1) {
                this.#renderSelectedKingdom(lordsDict[focusedKingdomLordDbID].localId);
            }
            else if (graphicsSettings.alwaysViewKingdom && myLord != null && myLord.isValid())
            {
                this.#renderSelectedKingdom(myLord.localId);
            }
        }

        if((isSelectingDestination)&&(generalPathMesh !== undefined))
            this.#renderGeneralPath();

        gl.enable(gl.DEPTH_TEST);
        this.#renderGenerals();
        gl.disable(gl.DEPTH_TEST);

        gl.disable(gl.BLEND);
    }

    getSelectedColor(xScreen, yScreen)
    {
        return this.selectionFramebuffer.bindAndReadSinglePixel(xScreen, gl.canvas.height - yScreen);
    }

    update(currentStep)
    {
        this.animateRequestId = requestAnimationFrame((step) => this.update(step));

        let delta = currentStep - this.previousStep;
        if (graphicsSettings.fpsLimit && delta < 1000 / graphicsSettings.fpsLimit)
        {
            return;
        }
        this.renderMap();
        this.previousStep = currentStep;
        uniformBufferObjects['time'].sendData([(currentStep / 1000.) % 100.]);

    }

    stopAnimation()
    {
        uniformBufferObjects['time'].sendData([0.]);
        cancelAnimationFrame(this.animateRequestId);
    }
}
