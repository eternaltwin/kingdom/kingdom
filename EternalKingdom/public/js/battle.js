// EternalKingdom - Battle

//#region Class and constants

class RenderView
{
    // Prevent to draw anything outside of the canvas / on a wall.
    limitY;

    constructor(canvas)
    {
        this.canvas = canvas;
        this.context = canvas.getContext("2d");

        this.canvas.height = 400;
        this.canvas.width = 500;

        this.limitY = [
            0.2 * this.canvas.height, 0.975 * this.canvas.height,
        ]
    }
}

const unitNameToIndex =
{
    'soldier' : 0,
    'archer' : 1,
    'pikeman' : 2,
    'horseman' : 3,
    'knight' : 4,
    'mountedArcher' : 5,
    'paladin' : 6,
    'catapult' : 7,
    'ballista' : 8,
}

class FightSurvivorStats
{
    unitCount;
    healthSum;

    constructor()
    {
        this.unitCount = 0;
        this.healthSum = 0;
    }

    addUnit(unit)
    {
        this.unitCount++;
        this.healthSum += unit.health;
    }

    getHealthAverage()
    {
        return this.healthSum / this.unitCount;
    }
}

class Army
{
    /**
     * Count of each Unit type
     */
    units;
    /**
     * Is defending side?
     */
    isDefending;
    /**
     * Aggregate injured fighter statistics.
     */
    fightSurvivorStats;

    constructor(units, isDefending)
    {
        this.units = units;
        this.isDefending = isDefending;
        this.fightSurvivorStats = [];
    }

    AddFightSurvivor(survivor)
    {
        if (this.fightSurvivorStats[survivor.name] === undefined) {
            this.fightSurvivorStats[survivor.name] = new FightSurvivorStats();
        }
        this.fightSurvivorStats[survivor.name].addUnit(survivor);

        const unitIndex = unitNameToIndex[survivor.name];
        this.units[unitIndex]++;
    }
}

const flagName = "flag";
const spriteUnitNames = ['soldier', 'pikeman', 'archer', 'horseman',
    'knight', 'mountedArcher', 'paladin', 'catapult', 'ballista'];
const spriteUnitNamesFlipped = [];
const waldoName = 'waldo';

const imagesSrc = {
    'soldier' : 'img/icons/u_soldier.png',
    'soldierLeft' : 'img/battle/u_soldierLeft.png',
    'archer'  : 'img/icons/u_archer.png',
    'archerLeft' : 'img/battle/u_archerLeft.png',
    'pikeman' : 'img/icons/u_pikeman.png',
    'pikemanLeft' : 'img/battle/u_pikemanLeft.png',
    'horseman' : 'img/icons/u_horseman.png',
    'horsemanLeft' : 'img/battle/u_horsemanLeft.png',
    'knight' : 'img/icons/u_knight.png',
    'knightLeft' : 'img/battle/u_knightLeft.png',
    'mountedArcher' : 'img/icons/u_mountedArcher.png',
    'mountedArcherLeft' : 'img/battle/u_mountedArcherLeft.png',
    'paladin' : 'img/icons/u_paladin.png',
    'paladinLeft' : 'img/battle/u_paladinLeft.png',
    'catapult' : 'img/icons/u_catapult.png',
    'catapultLeft' : 'img/battle/u_catapultLeft.png',
    'ballista' : 'img/icons/u_ballista.png',
    'ballistaLeft' : 'img/battle/u_ballistaLeft.png',
    'waldo' : 'img/battle/u_waldo.png',
    "flag" : 'img/icons/l_conquest.png',
    "shadow" : 'img/battle/shadow_3.png',
}

let battleView = null;
// List of images that can be used dynamically
let loadedImages = {};

let armies = {};

// How many army are present on the attacking side & on the defending side.
let armyCountOnEachSide = [0, 0];
// Heights used to display armies, flags, fights depending how many entities are present on the same column

let heightPerEntityShownOnColumn = [
    [0.5],
    [1/3, 1/2],
    [1/4, 2/4, 3/4],
    [1/5, 2/5, 3/5, 4/5],
    [1/6, 2/6, 3/6, 4/6, 5/6],
    [1/7, 2/7, 3/7, 4/7, 5/7, 6/7],
    [1/8, 2/8, 3/8, 4/8, 5/8, 6/8, 7/8]
];

//#region Rendering



function renderBattle() {
    renderArmies();
    renderFights();
}

function renderArmies()
{
    const canvasWidth = battleView.canvas.width;
    let armyLimitX = [
        [0.01 * canvasWidth, 0.325 * canvasWidth],
        [0.675 * canvasWidth, 0.95 * canvasWidth]
    ];

    let iNextAttackingArmyIndex = 0;
    let iNextDefendingArmyIndex = 0;

    for (const [key, value] of Object.entries(armies))
    {
        const army = value;
        // Center of the army displayed
        const currentCenterX = army.isDefending ? 0.825 : 0.175;
        const currentLimitX = armyLimitX[army.isDefending ? 1 : 0];

        const flagX = army.isDefending ? 0.625 : 0.375;

        let currentSideArmyCount = army.isDefending ? iNextDefendingArmyIndex : iNextAttackingArmyIndex;
        let sideArmyCount = armyCountOnEachSide[army.isDefending ? 1 : 0];
        let heightOffset = heightPerEntityShownOnColumn[sideArmyCount - 1][currentSideArmyCount];

        drawSprite(
            "flag",
            battleView.canvas.width * flagX,
            battleView.canvas.height * heightOffset,
            15,
            19);

        renderArmy(
            battleView.canvas.width * currentCenterX,
            battleView.canvas.height * heightOffset,
            currentLimitX, battleView.limitY,
            key);

        if (army.isDefending) {
            iNextDefendingArmyIndex++;
        } else {
            iNextAttackingArmyIndex++;
        }
    }
}

function renderArmy(centerX, centerY, limitX, limitY, armyId)
{
    const currentArmy = armies[armyId];
    let offsetBetweenUnits = 12;

    let remainingUnit = [];
    for (let unitIndex = 0; unitIndex < currentArmy.units.length; unitIndex++)
    {
        for (let i = 0; i < currentArmy.units[unitIndex]; i++)
        {
            remainingUnit.push(unitIndex);
        }
    }
    // TODO pick random from array https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array

    let x = 0;
    let y = 0;
    let useX = false;
    let direction = -1;
    let count = 1;
    while (remainingUnit.length > 0)
    {
        let currentUnit = remainingUnit[0];
        let displayedUnitName = currentArmy.isDefending ? spriteUnitNamesFlipped[currentUnit] : spriteUnitNames[currentUnit];

        let randomX = GetRandomInRange(offsetBetweenUnits * 0.5) - offsetBetweenUnits;
        let randomY = GetRandomInRange(offsetBetweenUnits * 1.5);
        //let randomX = 0;
        //let randomY = 0;
        // TODO Clamp function in utility.json!
        drawSprite(
            displayedUnitName,
            clamp(centerX + offsetBetweenUnits * x + randomX, limitX[0], limitX[1]),
            clamp(centerY + offsetBetweenUnits * y + randomY, limitY[0], limitY[1]),
            32,
            32
        );

        remainingUnit.shift();
        if (useX) {
            x += direction;
            if (Math.abs(x) >= count) {
                useX = false;
                direction *= -1;
                count++;
            }
        }
        else {
            y += direction;
            if (Math.abs(y) >= count) {
                useX = true;
            }
        }
    }
}

function renderFights()
{
    let simultaneousFights = serverData.fights.length;
    let offset = 24;
    for(let i = 0; i < simultaneousFights; i++)
    {
        let fight = serverData.fights[i];
        let fightDisplayHeight = battleView.canvas.height * (0.15 + 0.85 * heightPerEntityShownOnColumn[simultaneousFights - 1][i]);

        drawSprite("shadow", battleView.canvas.width * 0.5 - offset - 10, fightDisplayHeight + 16, 40, 17);
        drawSprite("shadow", battleView.canvas.width * 0.5 + offset - 8, fightDisplayHeight + 16, 40, 17);

        drawSprite(fight.attackingUnit.name, battleView.canvas.width * 0.5 - offset, fightDisplayHeight, 32, 32);
        drawSprite(fight.defendingUnit.name + "Left", battleView.canvas.width * 0.5 + offset, fightDisplayHeight, 32, 32);
    }
}

function drawSprite(image, x, y, sizeX, sizeY)
{
    battleView.context.drawImage(
        loadedImages[image],
        x,
        y,
        sizeX,
        sizeY
    );
}

//#endregion

//#region initialization

function initialize()
{
    let battleFrame = document.querySelector(".home_frame");
    battleView = new RenderView(battleFrame.querySelector("#battle_canvas"));
    generateFlippedSpriteNames();

    parseServerInfo();

    // Load all images once
    let loadImages = async function ()
    {
        for (let key in imagesSrc)
        {
            let img = new Image();
            img.src = "/" + imagesSrc[key];
            await img.decode();
            loadedImages[key] = img;
        }
    }

    loadImages().then(() =>
    {
        renderBattle();
    })
}

function generateFlippedSpriteNames()
{
    for (let i = 0; i < spriteUnitNames.length; i++)
    {
        spriteUnitNamesFlipped.push(spriteUnitNames[i] + "Left");
    }
}

window.addEventListener("load", function ()
{
    initialize();
});

//#endregion

//#region parse Server info

function parseServerInfo()
{
    // TODO Replace that by JsonSerializable?
    let attackingArmiesContainer = document.querySelector("#attackingArmies");
    let defendingArmiesContainer = document.querySelector("#defendingArmies");
    parseArmiesFromSide(attackingArmiesContainer);
    parseArmiesFromSide(defendingArmiesContainer);

    for (const [key, survivor] of Object.entries(serverData.fighters)) {
        armies[survivor.owningArmy].AddFightSurvivor(survivor);
    }
}
function parseArmiesFromSide(armyContainer)
{
    let allArmyDivs = armyContainer.children;
    for(let i = 0; i < allArmyDivs.length; i++){
        parseArmyInfo(allArmyDivs[i]);
    }
}

function parseArmyInfo(armyDiv)
{
    let armyUnitsString = armyDiv.dataset.army;
    let isDefendingSide = armyDiv.dataset.is_defending === "true";

    let armyUnitsArray = armyUnitsString.split(',').map(function(item) {
        return parseInt(item, 10);
    });

    armies[armyDiv.id] = new Army(armyUnitsArray, isDefendingSide);

    armyCountOnEachSide[isDefendingSide ? 1 : 0]++;
}

//#endregion
