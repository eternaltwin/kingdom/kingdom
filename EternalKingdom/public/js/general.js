// ETERNAL KINGDOM - General interaction


let generalsMeshes;
let generalPathMesh

const MoveActionState = {
    NONE                : 0,
    CHOOSE_DESTINATION  : 1,
}

let isSelectingDestination = false;
let movingGeneral = null;
let generalPath = {
    distance: 0.,
    path: []
};
let destination; //TODO: send full path to the server, and remove this variable

class General extends ServerEntity
{
    name;
    owner;
    isFortifying = false;
    reputation = 0;
    locationName = "";
    movementOrder = null;
    isAttacking = false;

    /** Rendering info */
    x;
    y;
    // On which slot do we display this general around a node?
    nodeSlot = 0;

    constructor(dbID, name, owner, locationName, reputation, isFortifying, isAttacking)
    {
        super(dbID);

        this.name = name;
        this.owner = owner;
        this.reputation = reputation;
        this.locationName = locationName;
        this.isFortifying = isFortifying;
        this.isAttacking = isAttacking;
    }

    /* Handle sprite position to display depending current state */
    getFirstSpritePosition()
    {
        if (this.isMoving())
        {
            let firstSprite;
            if (graphicsSettings.animate) {
                firstSprite = this.isTurnedLeft() ? "walk_left_1" : "walk_right_1";
            }
            else {
                firstSprite = this.isTurnedLeft() ? "walk_left_2" : "walk_right_2";
            }
            return spritesData["generals"]["sprites"][firstSprite] / 9;
        }
        else if(this.isAttacking)
        {
            let firstSprite;
            if (graphicsSettings.animate) {
                firstSprite = this.isTurnedLeft() ? "fight_left_1" : "fight_right_1";
            }
            else {
                firstSprite = this.isTurnedLeft() ? "fight_left_2" : "fight_right_2";
            }
            return spritesData["generals"]["sprites"][firstSprite] / 9;
        }

        if (this.isFortifying) {
            return spritesData["generals"]["sprites"]["fortify"] / 9;
        }
        else {
            return spritesData["generals"]["sprites"]["stand"];
        }
    }

    getNbFrames()
    {
        if (this.isMoving()) return 2.;
        else if(this.isAttacking) return 2.;
        return 1.;
    }

    /* When updating the location, we verify how many other generals are at the same place to split them around the node */
    setNodeLocation(nodeName)
    {
        if(this.locationName !== nodeName)
        {
            return;
        }

        if(this.locationName !== "")
        {
            let index = nodesDict[this.locationName].generals.indexOf(this);

            if(index !== -1)
            {
                nodesDict[this.locationName].generals.splice(index);
                for(let general of nodesDict[this.locationName].generals)
                {
                    if(general.nodeSlot > this.nodeSlot)
                    {
                        general.nodeSlot -= 1;
                    }
                }
            }
        }

        this.locationName = nodeName;

        if(this.locationName !== "")
        {
            let index = nodesDict[this.locationName].generals.indexOf(this);
            if(index === -1)
            {
                nodesDict[this.locationName].generals.push(this);
                this.nodeSlot = nodesDict[this.locationName].generals.length - 1;
            }
        }
    }

    setLocation(x, y)
    {
        this.x = x;
        this.y = y;
    }

    setMovementOrder(movementOrderJson)
    {
        const destination = movementOrderJson.destination;
        const section = movementOrderJson.currentSection;
        const movementEndDate = movementOrderJson.movementEndDate;

        this.movementOrder = new MovementOrder(
            destination,
            section.start,
            section.end,
            section.percentage,
            movementEndDate,
        );
    }

    updateLocation()
    {
        let x = 0;
        let y = 0;
        if (this.isMoving())
        {
            const sectionStart = this.movementOrder.sectionStart;
            const sectionEnd = this.movementOrder.sectionEnd;
            const startId = nodeNameToLocalId[sectionStart];
            const endId = nodeNameToLocalId[sectionEnd];
            const vectorLink = vectorsLinksDict[startId][endId];

            //TODO: remove this when server side movement will be fully implemented.
            if (vectorLink !== undefined)
            {
                const x1  = MAP_SCALE_FACTOR * (nodesDict[sectionStart].x + 5. * vectorLink[0]);
                const y1  = MAP_SCALE_FACTOR * (nodesDict[sectionStart].y + 2. * vectorLink[1]);
                const x2  = MAP_SCALE_FACTOR * (nodesDict[sectionEnd].x - 5. * vectorLink[0]);
                const y2  = MAP_SCALE_FACTOR * (nodesDict[sectionEnd].y - 2. * vectorLink[1]);

                let travelProgression = this.movementOrder.sectionProgress;
                x = Math.round(x1 * (1. - travelProgression) + x2 * travelProgression) - 13;
                y = Math.round(y1 * (1. - travelProgression) + y2 * travelProgression) - 15;
            }
            else
            {
                console.log("Failed to generate a Vector Link from MovementOrder Data...");
            }
        }
        else
        {
            x = MAP_SCALE_FACTOR * nodesDict[this.locationName].x;
            y = MAP_SCALE_FACTOR * nodesDict[this.locationName].y;
        }

        this.setLocation(x, y);
    }

    isMoving() { return this.movementOrder != null; }

    isTurnedLeft() {
        if (this.isMoving())
        {
            return this.movementOrder.isMovingLeft;
        }

        return nodesDict[this.locationName].x < this.getX();
    }

    getX()
    {
        if (this.isMoving()) return this.x;
        let nbGeneralsInSlot = nodesDict[this.locationName].generals.length;
        return Math.floor(this.x + Math.cos(- 2. * Math.PI * this.nodeSlot / Math.max(11, nbGeneralsInSlot) - Math.PI / 2.) * 48 - 20);
    }

    getY()
    {
        if (this.isMoving()) return this.y;
        let nbGeneralsInSlot = nodesDict[this.locationName].generals.length;
        return Math.floor(this.y + Math.sin(- 2. * Math.PI * this.nodeSlot / Math.max(11, nbGeneralsInSlot) - Math.PI / 2.) * 32 - 16);
    }
}

// TODO Right now, we only handle current section when the player refreshes the page
class MovementOrder
{
    // Final destination of the movement
    destinationName = "";
    // current section start
    sectionStart = "";
    // current section end
    sectionEnd = "";
    // Travel percentage on current section
    sectionProgress = 0.0;
    movementEndDate = "";

    // Cached value for rendering
    isMovingLeft = false;

    constructor(destination, sectionStart, sectionEnd, sectionProgress, movementEndDate)
    {
        this.destinationName = destination;
        this.sectionStart = sectionStart;
        this.sectionEnd = sectionEnd;
        this.sectionProgress = sectionProgress;
        this.movementEndDate = movementEndDate;

        this.isMovingLeft = nodesDict[sectionEnd].x < nodesDict[sectionStart].x;
    }
}

class GeneralsMeshes
{
    firstGeneralColorId;
    vertexBuffer;
    selectionVertexBuffer;
    indexBuffer;
    selectionIndexBuffer;
    instancesVertexBuffer;
    selectionInstanceVertexBuffer;
    generalsMesh;
    generalsSelectionMesh;

    constructor(firstGeneralColorId)
    {
        this.firstGeneralColorId = firstGeneralColorId;
        const spriteSize = spritesData["generals"]["sprites_size"];
        const y_shadow = spriteSize / 3.

        this.vertexBuffer = new VertexBuffer(
        [
            0., 0., 0., 0., 0.,
            spriteSize, 0., 0., 1./9., 0.,
            0., y_shadow, y_shadow, 0., 1./3.,
            spriteSize, y_shadow, y_shadow, 1./9, 1./3.,
            0., spriteSize, y_shadow, 0., 1.,
            spriteSize, spriteSize, y_shadow, 1./9, 1.
        ]);
        this.selectionVertexBuffer = new VertexBuffer(
        [
            0., spriteSize * 0.25,
            spriteSize * 0.75, spriteSize * 0.25,
            0., spriteSize,
            spriteSize * 0.75, spriteSize
        ]);
        this.indexBuffer = new IndexBuffer([0, 1, 3, 0, 3, 2, 2, 3, 5, 2, 5, 4]);
        this.selectionIndexBuffer = new IndexBuffer([0, 1, 3, 0, 3, 2]);
        
        this.instancesVertexBuffer = new VertexBuffer(this.#getInstancesData(), gl.DYNAMIC_DRAW);
        this.selectionInstanceVertexBuffer = new VertexBuffer(this.#getselectionInstancesData(), gl.DYNAMIC_DRAW);

        this.generalsMesh = new InstancedIndicedMesh2d(
            this.vertexBuffer, this.instancesVertexBuffer, this.indexBuffer, shaders['generals']
        );
        this.generalsSelectionMesh = new InstancedIndicedMesh2d(
            this.selectionVertexBuffer, this.selectionInstanceVertexBuffer, this.selectionIndexBuffer, shaders['generalsSelection']
        );
    }

    updateInstances()
    {
        gl.bindBuffer(gl.ARRAY_BUFFER, this.instancesVertexBuffer.vbo);
        gl.bufferSubData(gl.ARRAY_BUFFER, 0, new Float32Array(this.#getInstancesData()));

        gl.bindBuffer(gl.ARRAY_BUFFER, this.selectionInstanceVertexBuffer.vbo);
        gl.bufferSubData(gl.ARRAY_BUFFER, 0, new Float32Array(this.#getselectionInstancesData()));

        gl.bindBuffer(gl.ARRAY_BUFFER, null);
    }

    updateGeneralPositionAndSprites(generalId, newX, newY, firstSpritePosition, nbFrames)
    {
        const offset = 5 * generalId * Float32Array.BYTES_PER_ELEMENT;
        gl.bindBuffer(gl.ARRAY_BUFFER, this.instancesVertexBuffer.vbo);
        gl.bufferSubData(gl.ARRAY_BUFFER, offset, new Float32Array([newX, newY, firstSpritePosition, nbFrames]));

        const selectionOffset = 2 * generalId * Float32Array.BYTES_PER_ELEMENT;
        gl.bindBuffer(gl.ARRAY_BUFFER, this.selectionInstanceVertexBuffer.vbo);
        gl.bufferSubData(gl.ARRAY_BUFFER, selectionOffset, new Float32Array([newX, newY]));

        gl.bindBuffer(gl.ARRAY_BUFFER, null);
    }

    #getInstancesData()
    {
        let instancesData = [];
        for (let i = 0; i < allGenerals.length; i++) {
            //TODO : vassal color
            let hue;
            if (allGenerals[i].owner.isEquals(myLord))
            {
                hue = graphicsSettings.myGeneralsHue / 360.;
            }
            else
            {
                hue = graphicsSettings.otherGeneralsHue / 360.;
            }
            instancesData.push(
                allGenerals[i].getX(), allGenerals[i].getY(), 
                allGenerals[i].getFirstSpritePosition(), allGenerals[i].getNbFrames(), 
                hue
            );        
        }
        return instancesData;
    }

    #getselectionInstancesData()
    {
        let selectionInstancesData = [];
        for (let i = 0; i < allGenerals.length; i++) {
            let colorId = this.firstGeneralColorId + i;
            let red = Math.floor(colorId / 256);
            let green = colorId % 256;
            selectionInstancesData.push(allGenerals[i].getX(), allGenerals[i].getY(), red / 255., green / 255.);
        }
        return selectionInstancesData;
    }
}


//#region Movement

function clearGeneralPathMesh()
{
    if(generalPathMesh !== undefined)
    {
        generalPathMesh.deleteGlObject();
        generalPathMesh.vertexBuffer.deleteGlObject();
        generalPathMesh = undefined;
    }
}

function updateGeneralPathMesh()
{
    clearGeneralPathMesh();

    const thickness = 2.;

    let pathVertices = [];

    let idCity1 = generalPath['path'][0];
    let idCity2 = generalPath['path'][1];
    let vectorLink1 = vectorsLinksDict[idCity1][idCity2];
    let vectorLink2;
    let x1 = MAP_SCALE_FACTOR * mapData["cities"][idCity1]["x"];
    let y1 = MAP_SCALE_FACTOR * mapData["cities"][idCity1]["y"];
    let dx1 = - thickness * vectorLink1[1];
    let dy1 = thickness * vectorLink1[0];
    let x2, y2, dx2, dy2;

    for(let i = 0; i < generalPath['path'].length - 2; i++)
    {
        let idCity3 = generalPath['path'][i + 2];
        x2 = MAP_SCALE_FACTOR * mapData["cities"][idCity2]["x"];
        y2 = MAP_SCALE_FACTOR * mapData["cities"][idCity2]["y"];
        vectorLink1 = vectorsLinksDict[idCity2][idCity1];
        vectorLink2 = vectorsLinksDict[idCity2][idCity3];

        let crossProduct = vectorLink1[0] * vectorLink2[1] - vectorLink1[1] * vectorLink2[0];

        if(Math.abs(crossProduct) < 0.01)
        {
            dx2 = thickness * vectorLink2[1];
            dy2 = - thickness * vectorLink2[0];
        }
        else
        {
            dx2 = vectorLink2[0] + vectorLink1[0];
            dy2 = vectorLink2[1] + vectorLink1[1];
            let norm = Math.sqrt(dx2 * dx2 + dy2 * dy2);
            let cosAngle = vectorLink1[0] * vectorLink2[0] + vectorLink1[1] * vectorLink2[1];
            let angle = Math.acos(Math.max(-1., Math.min(1., cosAngle)));
            let norm_sin_half_angle = norm * Math.sin( angle/ 2.);
            dx2 = thickness * dx2 / norm_sin_half_angle;
            dy2 = thickness * dy2 / norm_sin_half_angle;

            if(crossProduct > 0.)
            {
                dx2 = -dx2;
                dy2 = -dy2;
            }
        }
        
        pathVertices.push(x1 + dx1, y1 + dy1, x1 - dx1, y1 - dy1, x2 - dx2, y2 - dy2);
        pathVertices.push(x1 + dx1, y1 + dy1, x2 - dx2, y2 - dy2, x2 + dx2, y2 + dy2);
        idCity1 = idCity2;
        idCity2 = idCity3;
        x1 = x2;
        y1 = y2;
        dx1 = dx2;
        dy1 = dy2;
    }

    x2 = MAP_SCALE_FACTOR * mapData["cities"][idCity2]["x"];
    y2 = MAP_SCALE_FACTOR * mapData["cities"][idCity2]["y"];
    if(generalPath['path'].length > 2)
    {
        dx2 = - thickness * vectorLink2[1];
        dy2 = thickness * vectorLink2[0];
    }
    else
    {
        dx2 = -thickness * vectorLink1[1];
        dy2 = thickness * vectorLink1[0];
    }


    pathVertices.push(x1 + dx1, y1 + dy1, x1 - dx1, y1 - dy1, x2 - dx2, y2 - dy2);
    pathVertices.push(x1 + dx1, y1 + dy1, x2 - dx2, y2 - dy2, x2 + dx2, y2 + dy2);

    generalPathMesh = new Mesh2d(new VertexBuffer(pathVertices), shaders['generalPath']);
}

function startDestinationSelection()
{
    if(!isSelectingDestination)
    {
        isSelectingDestination = true;
        movingGeneral = selectedGeneral;
        showDestinationSelectionFeedback(true);
        updateSideMenuActions();
    }
}

function extendGeneralPath()
{
    let previousStep;
    if(generalPath['path'].length === 0)
    {
        previousStep = cityNameToId[movingGeneral.locationName];
    }
    else
    {
        previousStep = generalPath['path'][generalPath['path'].length - 1];
    }
    let path = computePath(previousStep, cityNameToId[selectedNode.name]);
    generalPath['distance'] += path['distance'];
    if(generalPath['path'].length === 0)
    {
        generalPath['path'] = path['path'];
    }
    else
    {
        generalPath['path'].pop();
        generalPath['path'] = generalPath['path'].concat(path['path']);
    }

    if(generalPath['path'].length !== 0)
    {
        updateGeneralPathMesh();

        if(!graphicsSettings.animate)
        {
            mapRenderer.renderMap();
        }
    }
}


function stopDestinationSelection()
{
    clearGeneralPathMesh();
    isSelectingDestination = false;
    showDestinationSelectionFeedback(false);
    updateSideMenuActions();
    generalPath['distance'] = 0.;
    generalPath['path'] = [];

    if(!graphicsSettings.animate)
    {
        mapRenderer.renderMap();
    }
}

function generalConfirmMove()
{
    requestMoveToDestination();
    stopDestinationSelection();
}

function generalCancelMove()
{
    stopDestinationSelection();
}

function generalTurnBack()
{
    requestTurnBack();
}


/**
 * Render the path that the general will use in order to reach its destination
 * @param context
 */
function renderPathToDestination(context)
{
    if(isSelectingDestination && selectedNode !== null)
    {
        context.strokeStyle = "rgba(255,255,255,0.75)";
        context.lineWidth = 3;
        context.setLineDash([]);

        context.beginPath();
        context.moveTo(movingGeneral.x, movingGeneral.y);
        context.lineTo(selectedNode.x, selectedNode.y);
        context.stroke();
    }
}

function showDestinationSelectionFeedback(visible)
{
    let mapFeedback = document.querySelector(".map_feedback");
    let moveFeedback = mapFeedback.querySelector("#choose_destination");
    showDOM(mapFeedback, visible);
    showDOM(moveFeedback, visible);
}

function requestMoveToDestination()
{
    if (generalPath['path'].length !== 0) {
        let payload = {
            general : movingGeneral.dbID,
            path    : generalPath['path'],
        }
        ServerPost('/general/move/', payload, handleMoveResponse);
    }
}

function handleMoveResponse(data)
{
    const isSuccess = data['success'];
    const message = data['message'];
    if (isSuccess)
    {
        let general = null;
        const generalDBId = data['generalID'];
        for(let i = 0; i < myGeneralsLocalID.length; i++)
        {
            let currentGeneral = allGenerals[myGeneralsLocalID[i]];
            if(currentGeneral.dbID === generalDBId)
            {
                general = currentGeneral;
                break;
            }
        }

        if (general !== null)
        {
            general.setMovementOrder(data.movementOrder);
            general.updateLocation();

            refreshGeneralMeshes();
            if(!graphicsSettings.animate)
            {
                mapRenderer.renderMap();
            }
        }
    }

    showGeneralNotification(isSuccess, message);
}

function requestTurnBack()
{
    if(selectedGeneral != null) {
        let payload = {
            general : selectedGeneral.dbID
        }
        ServerPost('/general/turnBack/', payload, handleMoveResponse);
    }
}

//#endregion

//#region Attack
function requestAttack()
{
    if(selectedGeneral != null) {
        let payload = {
            general    : selectedGeneral.dbID,
        }

        ServerPost('/general/attack/', payload, handleAttackResponse);
    }
}

function handleAttackResponse(data)
{
    const isSuccess = data['success'];
    const message = data['message'];
    if (isSuccess)
    {
        location.assign('/map/'+worldId+'/general/'+selectedGeneral.dbID);
    }
    else
    {
        showGeneralNotification(isSuccess, message);
    }
}
function requestHarvest()
{
    if(selectedGeneral != null) {
        let payload = {
            general    : selectedGeneral.dbID,
        }

        ServerPost('/general/harvest/', payload, handleHarvestResponse);
    }
}


function handleHarvestResponse(data)
{
    const isSuccess = data['success'];
    const message = data['message'];
    if (isSuccess)
    {
        const newAmount = data['newAmount'];
        const nodeName = data['node'];
        if(newAmount > 0) {
            resourceSpots[nodeName].amount = newAmount;
        }
        else
        {
            delete resourceSpots[nodeName];

            // TODO Bug : Update resource instance data @Talsi help
        }
    }

    showGeneralNotification(isSuccess, message);
}


//#endregion

//#region Fortification

function requestFortification()
{
    let payload = {
        general : selectedGeneral.dbID,
    }

    ServerPost('/general/toggleFortification', payload, handleFortificationResponse);
}

function handleFortificationResponse(data)
{
    const isSuccess = data['success'];
    const message = data['message'];
    if (isSuccess) {
        // TODO BUG : Server should provide general cause selected general could have changed
        selectedGeneral.isFortifying = !selectedGeneral.isFortifying;

        closeWorldSideMenu();
        showGeneralNotification(isSuccess, message);

        refreshGeneralMeshes();
        if(!graphicsSettings.animate)
        {
            mapRenderer.renderMap();
        }
    }
}

//#endregion

function showGeneralNotification(isSuccess, message)
{
    SnackBar({
        message: message,
        status: isSuccess ? "success" : "error",
        container: '.map_frame',
        position: 'bc'
    });
}

//#region Pathfinding

function computePath(city_1, city_2)
{
    //see : https://levelup.gitconnected.com/finding-the-shortest-path-in-javascript-dijkstras-algorithm-8d16451eea34

    let shortestDistanceNode = (distances, visited) => {
        // create a default value for shortest
        let shortest = null;
          
        // for each node in the distances object
        for (let node in distances) {
            // if no node has been assigned to shortest yet
            // or if the current node's distance is smaller than the current shortest
            let currentIsShortest =
                shortest === null || distances[node] < distances[shortest];
                  
            // and if the current node is in the unvisited set
            if (currentIsShortest && !visited.includes(node)) {
                // update shortest to be the current node
                shortest = node;
            }
        }
        return shortest;
    };

    let findShortestPath = (graph, startNode, endNode) => {
 
        // track distances from the start node using a hash object
        let distances = {};
        distances[endNode] = "Infinity";
        distances = Object.assign(distances, graph[startNode]);// track paths using a hash object
        let parents = { endNode: null };
        for (let child in graph[startNode]) {
            parents[child] = startNode;
        }
         
        // collect visited nodes
        let visited = [];// find the nearest node
        let node = shortestDistanceNode(distances, visited);
        
        // for that node:
        while (node) {
            // find its distance from the start node & its child nodes
            let distance = distances[node];
            let children = graph[node]; 
             
            // for each of those child nodes:
            for (let child in children) {
                // make sure each child node is not the start node
                if (String(child) === String(startNode)) {
                    continue;
                } else {
                    // save the distance from the start node to the child node
                    let newdistance = distance + children[child];// if there's no recorded distance from the start node to the child node in the distances object
                    // or if the recorded distance is shorter than the previously stored distance from the start node to the child node
                    if (!distances[child] || distances[child] > newdistance) {
                        // save the distance to the object
                        distances[child] = newdistance;
                        // record the path
                        parents[child] = node;
                    } 
                }
            }  
            // move the current node to the visited set
            visited.push(node);// move to the nearest neighbor node
            node = shortestDistanceNode(distances, visited);
        }
         
        // using the stored paths from start node to end node
        // record the shortest path
        let shortestPath = [endNode];
        let parent = parents[endNode];
        while (undefined !== parent) {
            shortestPath.push(parseInt(parent));
            parent = parents[parent];
        }
        shortestPath.reverse();
         
        //this is the shortest path
        let results = {
            distance: distances[endNode],
            path: shortestPath,
        };
        // return the shortest path & the end node's distance from the start node
        return results;
    };
    let shortestPath = findShortestPath(distancesDict, city_1, city_2);
    return shortestPath;
}

//#endregion