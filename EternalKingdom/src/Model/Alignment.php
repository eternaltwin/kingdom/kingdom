<?php

namespace App\Model;

// An alignement contains a list of Tiles of the the same element. A valid alignement has a minimum length of 3
class Alignment
{
    public int $gain = 0;

    /**
     * @param array<Tile> $tiles
     */
    public function __construct(public array $tiles, public int $element)
    {
    }

    public static function InvalidAlignment(): Alignment
    {
        return new Alignment([], -1);
    }

    public function getLength(): int
    {
        return count($this->tiles);
    }

    public function isValidAlignment(): bool
    {
        return $this->getLength() >= 3 && -1 != $this->element;
    }
}
