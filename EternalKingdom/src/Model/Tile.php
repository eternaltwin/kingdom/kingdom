<?php

namespace App\Model;

// Tile coordinates - (0,0) is top left corner
class Tile
{
    public $row;
    public $col;

    public function __construct(int $a_row, int $a_col)
    {
        $this->row = $a_row;
        $this->col = $a_col;
    }
}
