<?php

namespace App\Model;

/*Respawning tile info are used to feedback all respawned elements during a chain reaction,
some elements are only temporary since they could be destroyed by an alignement so we need to store them*/
class TileRespawnInfo
{
    public $row;
    public $col;
    public $elem;

    public function __construct(int $a_row, int $a_col, int $a_elem)
    {
        $this->row = $a_row;
        $this->col = $a_col;
        $this->elem = $a_elem;
    }
}
