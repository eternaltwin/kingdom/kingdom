<?php

namespace App\Model;

// Stores what happens after the player swapped two tiles
class GridChainReactionStep
{
    public $alignments = [];
    public $spawns = [];

    public function addAlignment(Alignment $alignment): void
    {
        $this->alignments[] = $alignment;
    }

    public function addRespawn(TileRespawnInfo $spawn): void
    {
        $this->spawns[] = $spawn;
    }
}
