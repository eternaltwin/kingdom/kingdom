<?php

namespace App\Model;

// Represent a swap between two tiles
class SwapCandidate
{
    public $tile1;
    public $tile2;

    public function __construct(Tile $a_tile1, Tile $a_tile2)
    {
        $this->tile1 = $a_tile1;
        $this->tile2 = $a_tile2;
    }
}
