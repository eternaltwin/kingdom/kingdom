<?php

namespace App\DataFixtures;

use App\Enum\Language;
use App\Enum\WorldSpeedType;
use App\Service\WorldManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class WorldDataFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    private readonly ConsoleOutput $output;

    public function __construct(private readonly WorldManager $worldManager)
    {
        $this->output = new ConsoleOutput();
    }

    #[\Override]
    public function load(ObjectManager $manager): void
    {
        // TODO FRONTEND Json Caching System
        // Pour charger la json de la carte une seule fois si on est vivant sur cette carte.
        // Js localStorage
        // OU
        // Js Cache de navigateur - CacheControl (Http Header) - https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control
        // Header pour que le cache dure un an:
        // Cache-Control "public,max-age=31536000,immutable";

        $progressBar = new ProgressBar($this->output, 4);

        $worldData1 = $this->worldManager->createWorldData('1');
        $manager->persist($worldData1);
        $worldData2 = $this->worldManager->createWorldData('2');
        $manager->persist($worldData2);
        $worldData6 = $this->worldManager->createWorldData('6');
        $manager->persist($worldData6);
        $worldData11 = $this->worldManager->createWorldData('11');
        $manager->persist($worldData11);
        $worldData14 = $this->worldManager->createWorldData('14');
        $manager->persist($worldData14);
        $progressBar->advance();

        $manager->flush();
        $progressBar->advance();

        $this->worldManager->createWorld($worldData1, 'Azigny', Language::FRENCH, WorldSpeedType::BLITZ, 20);
        $this->worldManager->createWorld($worldData2, 'Boruchir', Language::FRENCH);
        $this->worldManager->createWorld($worldData6, 'Tamuchord', Language::ENGLISH);
        $this->worldManager->createWorld($worldData11, 'Bolubum', Language::SPANISH);
        $this->worldManager->createWorld($worldData14, 'Pirmasching', Language::GERMAN);
        $progressBar->advance();

        $manager->flush();
        $progressBar->advance();
    }

    #[\Override]
    public function getDependencies()
    {
        return [ResourceFixtures::class];
    }

    #[\Override]
    public static function getGroups(): array
    {
        return ['world'];
    }
}
