<?php

namespace App\DataFixtures;

use App\Service\GameData\EKLogsLoader;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class LogEventFixtures extends Fixture implements FixtureGroupInterface
{
    public function __construct(private readonly EKLogsLoader $dataLoader)
    {
    }

    #[\Override]
    public function load(ObjectManager $manager): void
    {
        $this->dataLoader->loadGameData($manager);
    }

    #[\Override]
    public static function getGroups(): array
    {
        return ['prod', 'logEvent'];
    }
}
