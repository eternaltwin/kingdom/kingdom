<?php

namespace App\Twig;

use App\Entity\LogSystem\Log;
use App\Enum\LogParamType;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/*
 * Provides additional TWIG Functions or Filters for EternalKingdom.
 */

class AppExtension extends AbstractExtension
{
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    #[\Override]
    public function getFunctions(): array
    {
        return [
            new TwigFunction('formatLogData', $this->formatData(...)),
        ];
    }

    /**
     * Returns an array of parameters to use to format a log properly.
     * It matches expected static data vs dynamic data stored in the dynamic log!
     */
    public function formatData(Log $log): array
    {
        $formattedLogData = [];
        $dynamicData = $log->getDynamicData();

        foreach ($log->getEvent()->getExpectedData() as $staticData) {
            /** @var int|string|null $formattedParam */
            $formattedParam = null;
            $expectedParamName = $staticData['name'];

            switch ($staticData['type']) {
                case LogParamType::RAW:
                    $formattedParam = $dynamicData[$expectedParamName];
                    break;

                case LogParamType::ICON:
                    $formattedParam = $this->getIcon($staticData['path']);
                    break;

                case LogParamType::RESOURCE:
                    $formattedParam = $this->getResourceIcon($dynamicData[$expectedParamName]);
                    break;

                case LogParamType::UNIT:
                    $formattedParam = $this->getUnitIcon($dynamicData[$expectedParamName]);
                    break;

                case LogParamType::TRANSLATION:
                    $translation = $staticData['prefix'].$dynamicData[$expectedParamName].$staticData['suffix'];
                    $formattedParam = $this->translator->trans($translation, [], $staticData['domain']);
                    break;

                case LogParamType::NODE_LINK:
                    $worldId = array_key_exists('%worldId%', $dynamicData) ? $dynamicData['%worldId%'] : -1;
                    $nodeId = array_key_exists('%nodeId%', $dynamicData) ? $dynamicData['%nodeId%'] : -1;
                    $nodeName = array_key_exists('%nodeName%', $dynamicData) ? $dynamicData['%nodeName%'] : 'INVALID';
                    if (-1 != $worldId) {
                        $link = "<a href='/map/".$worldId.'/node/'.$nodeId."'>".$nodeName.'</a>';
                    } else {
                        $link = '<a>'.$nodeName.'</a>';
                    }
                    $formattedParam = $link;
                    break;

                case LogParamType::USER_LINK:
                    $userId = array_key_exists('%userId%', $dynamicData) ? $dynamicData['%userId%'] : -1;
                    $lordName = array_key_exists('%lord%', $dynamicData) ? $dynamicData['%lord%'] : 'INVALID';

                    if (array_key_exists('%title%', $dynamicData)) {
                        $title = $this->translator->trans('lord.titles.'.$dynamicData['%title%'], [], 'Common');
                        $linkText = $title.' '.$lordName;
                    } else {
                        $linkText = $lordName;
                    }

                    $link = -1 != $userId ? "<a href='/user/".$userId."'>".$linkText.'</a>' : '<a>'.$linkText.'</a>';
                    $formattedParam = $link;
                    break;

                case LogParamType::BATTLE_LINK:
                    $nodeName = array_key_exists('%nodeName%', $dynamicData) ? $dynamicData['%nodeName%'] : 'INVALID';
                    $battleId = array_key_exists('%battleId%', $dynamicData) ? $dynamicData['%battleId%'] : -1;
                    $battleTranslation = $this->translator->trans('header.battle', ['%location%' => $nodeName], 'Battle');

                    if (-1 != $battleId) {
                        $link = "<a href='/battle/".$battleId."'>".$battleTranslation.'</a>';
                    } else {
                        $link = '<a>'.$nodeName.'</a>';
                    }
                    $formattedParam = $link;
                    break;
            }

            assert(!is_null($formattedParam), sprintf('Parameter %s is missing for event %s', $expectedParamName, $log->getEvent()?->getEventName()));
            $formattedLogData[$expectedParamName] = $formattedParam;
        }

        // dump($array);

        return $formattedLogData;
    }

    private function getResourceIcon(string $resource): string
    {
        return $this->getIcon('small/res_'.$resource.'.gif');
    }

    private function getUnitIcon(string $unit): string
    {
        return $this->getIcon('small/u_'.$unit.'.gif');
    }

    private function getIcon(string $iconName): string
    {
        return "<img src='/img/icons/".$iconName."'>";
    }
}
