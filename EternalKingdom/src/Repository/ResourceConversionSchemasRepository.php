<?php

namespace App\Repository;

use App\Entity\ResourceConversionSchemas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ResourceConversionSchemas|null find($id, $lockMode = null, $lockVersion = null)
 * @method ResourceConversionSchemas|null findOneBy(array $criteria, array $orderBy = null)
 * @method ResourceConversionSchemas[]    findAll()
 * @method ResourceConversionSchemas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResourceConversionSchemasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ResourceConversionSchemas::class);
    }

    // /**
    //  * @return ResourceConversionSchemas[] Returns an array of ResourceConversionSchemas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ResourceConversionSchemas
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
