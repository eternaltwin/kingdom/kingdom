<?php

namespace App\Repository\Battle;

use App\Entity\Battle\Battle;
use App\Entity\Lord;
use App\Entity\World\WorldMapNode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Battle|null find($id, $lockMode = null, $lockVersion = null)
 * @method Battle|null findOneBy(array $criteria, array $orderBy = null)
 * @method Battle[]    findAll()
 * @method Battle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BattleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Battle::class);
    }

    public function findOngoingBattles(int $worldId)
    {
        $qb = $this->createQueryBuilder('battle')
            ->leftJoin(WorldMapNode::class, 'node', Join::WITH, 'battle.location = node.id')
            ->andWhere('node.owningWorld = :worldId')
            ->andWhere('battle.isFinished = FALSE')
            ->setParameter('worldId', $worldId);

        return $qb->getQuery()->getResult();
    }

    /**
     * @return array<Battle>
     */
    public function findPendingBattleOnTerritoriesOwnedBy(Lord $lord): array
    {
        $qb = $this->createQueryBuilder('battle')
            ->join('battle.location', 'node', 'WITH', 'node.id = battle.location')
            ->where('battle.isFinished = FALSE')
            ->andWhere('node.sovereign = :lordId')
            ->setParameter('lordId', $lord->getId());

        return $qb->getQuery()->getResult();
    }
}
