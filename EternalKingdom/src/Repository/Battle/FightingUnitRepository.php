<?php

namespace App\Repository\Battle;

use App\Entity\Battle\FightingUnit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FightingUnit|null find($id, $lockMode = null, $lockVersion = null)
 * @method FightingUnit|null findOneBy(array $criteria, array $orderBy = null)
 * @method FightingUnit[]    findAll()
 * @method FightingUnit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FightingUnitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FightingUnit::class);
    }
}
