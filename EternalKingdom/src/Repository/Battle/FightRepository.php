<?php

namespace App\Repository\Battle;

use App\Entity\Battle\Battle;
use App\Entity\Battle\Fight;
use App\Entity\Battle\FightingUnit;
use App\Entity\World\WorldMap;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Fight|null find($id, $lockMode = null, $lockVersion = null)
 * @method Fight|null findOneBy(array $criteria, array $orderBy = null)
 * @method Fight[]    findAll()
 * @method Fight[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FightRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Fight::class);
    }

    public function tickDamage(WorldMap $worldMap): void
    {
        $connection = $this->getEntityManager()->getConnection();
        $sql = $connection->prepare(
            'WITH damages AS (
                SELECT f.defending_unit_id unit_id, (FLOOR(RANDOM() * 2) + 1 + attacker.damage_bonus) damages
                FROM fight f
                INNER JOIN battle b ON b.id = f.battle_id
                INNER JOIN fighting_unit attacker ON attacker.id = f.attacking_unit_id
                WHERE b.world_map_id = :worldId
                UNION
                SELECT f.attacking_unit_id unit_id, (FLOOR(RANDOM() * 2) + 1 + defender.damage_bonus) damages
                FROM fight f
                INNER JOIN battle b ON b.id = f.battle_id
                INNER JOIN fighting_unit defender ON defender.id = f.defending_unit_id
                WHERE b.world_map_id = :worldId
            )
            UPDATE fighting_unit u
            SET remaining_health = u.remaining_health - dmg.damages
            FROM damages dmg
            WHERE dmg.unit_id = u.id;'
        );
        $sql->bindValue(':worldId', $worldMap->getId());
        $sql->executeQuery();
    }

    // TODO If we want to use it, add the world parameter & update the query
    public function tickDamage2(): void
    {
        $connection = $this->getEntityManager()->getConnection();
        $connection->executeQuery(
            'UPDATE fighting_unit u
            SET remaining_health = def.remaining_health - (FLOOR(RANDOM() * 2) + 1 + atk.damage_bonus)
            FROM fighting_unit def
            INNER JOIN fight f ON f.defending_unit_id = def.id
            INNER JOIN fighting_unit atk ON atk.id = f.attacking_unit_id
            WHERE u.id = def.id;'
        );

        $connection->executeQuery(
            'UPDATE fighting_unit u
                SET remaining_health = atk.remaining_health - (FLOOR(RANDOM() * 2) + 1 + def.damage_bonus)
                FROM fighting_unit atk
                INNER JOIN fight f ON f.attacking_unit_id = atk.id
                INNER JOIN fighting_unit def ON def.id = f.defending_unit_id
                WHERE u.id = atk.id;'
        );
    }

    public function findFightsByWorld(WorldMap $worldMap): array
    {
        return $this->createQueryBuilder('fight')
            ->join(Battle::class, 'battle', Join::WITH, 'fight.battle = battle.id')
            ->where('battle.worldMap = :world')
            ->setParameter('world', $worldMap)
            ->getQuery()
            ->getResult();
    }

    public function findAllFightsWithDeadUnits(WorldMap $worldMap): array
    {
        return $this->createQueryBuilder('fight')
            ->select('fight')
            ->join(Battle::class, 'battle', Join::WITH, 'fight.battle = battle.id')
            ->join(FightingUnit::class, 'attacking_unit', Join::WITH, 'fight.attackingUnit = attacking_unit.id')
            ->join(FightingUnit::class, 'defending_unit', Join::WITH, 'fight.defendingUnit = defending_unit.id')
            ->where('battle.worldMap = :world')
            ->andWhere('attacking_unit.remainingHealth <= 0')
            ->orWhere('defending_unit.remainingHealth <= 0')
            ->setParameter('world', $worldMap)
            ->getQuery()
            ->getResult();
    }
}
