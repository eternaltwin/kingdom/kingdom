<?php

namespace App\Repository\Battle;

use App\Entity\Battle\Battle;
use App\Entity\Battle\EngagedArmy;
use App\Entity\Battle\Fight;
use App\Entity\Battle\FightingUnit;
use App\Entity\Battle\FightSurvivor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FightSurvivor|null find($id, $lockMode = null, $lockVersion = null)
 * @method FightSurvivor|null findOneBy(array $criteria, array $orderBy = null)
 * @method FightSurvivor[]    findAll()
 * @method FightSurvivor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FightSurvivorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FightSurvivor::class);
    }

    /**
     * Find all fight survivors in the specified Battle.
     * @param Battle $battle
     *
     * @return array<FightSurvivor>
     */
    public function findFightSurvivors(Battle $battle): array
    {
        $query = $this->createQueryBuilder('survivor')
            ->select('survivor')
            ->join(EngagedArmy::class, 'army', Join::WITH, 'survivor.owningEngagedArmy = army.id')
            ->where('army.owningBattle = :battle')
            ->setParameter('battle', $battle);

        return $query->getQuery()->getResult();
    }

    public function deleteSurvivors(Battle $battle): void
    {
        $engagedArmiesId = [];
        foreach ($battle->getAllEngagedArmies() as $engagedArmy) {
            $engagedArmiesId[] = $engagedArmy->getId();
        }

        $query = $this->createQueryBuilder('unit')
            ->delete()
            ->where('unit.owningEngagedArmy IN (:ids)')
            ->setParameter('ids', $engagedArmiesId);

        $query->getQuery()->execute();
    }
}
