<?php

namespace App\Repository;

use App\Entity\UnitArmy;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UnitArmy|null find($id, $lockMode = null, $lockVersion = null)
 * @method UnitArmy|null findOneBy(array $criteria, array $orderBy = null)
 * @method UnitArmy[]    findAll()
 * @method UnitArmy[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UnitArmyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UnitArmy::class);
    }

    // /**
    //  * @return UnitArmy[] Returns an array of UnitArmy objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UnitArmy
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
