<?php

namespace App\Repository;

use App\Entity\Lord;
use App\Enum\HealthType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Lord|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lord|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lord[]    findAll()
 * @method Lord[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LordRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lord::class);
    }

    /**
     * Returns the list of alive players ordered by descending age.
     *
     * @return Lord[] Returns an array of Lord objects
     */
    public function findAllAliveLordsOrderByAge(int $worldId): array
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.worldMap = :id')
            ->andWhere('l.health != :health')
            ->setParameter('id', $worldId)
            ->setParameter('health', HealthType::TYPE_DEAD)
            ->addOrderBy('l.ageYear', 'DESC')
            ->addOrderBy('l.ageMonth', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Returns the list of alive players ordered by descending age.
     *
     * @return Lord[] Returns an array of Lord objects
     */
    public function findAllAliveLordsOrderByGlory(int $worldId, int $offset, int $limit): array
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.worldMap = :id')
            ->andWhere('l.health != :health')
            ->setParameter('id', $worldId)
            ->setParameter('health', HealthType::TYPE_DEAD)
            ->addOrderBy('l.glory', 'DESC')
            ->getQuery()
            ->setFirstResult($offset * $limit)
            ->setMaxResults($limit)
            ->getResult();
    }

    public function countAliveLordsInWorld(int $worldId): int
    {
        return $this->createQueryBuilder('l')
            ->select('count(l.id)')
            ->andWhere('l.worldMap = :id')
            ->andWhere('l.health != :health')
            ->setParameter('id', $worldId)
            ->setParameter('health', HealthType::TYPE_DEAD)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
