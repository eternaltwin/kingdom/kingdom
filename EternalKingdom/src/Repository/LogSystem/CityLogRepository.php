<?php

namespace App\Repository\LogSystem;

use App\Entity\LogSystem\CityLog;
use App\Entity\Lord;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CityLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method CityLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method CityLog[]    findAll()
 * @method CityLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CityLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CityLog::class);
    }

    public function findRecentGameLogs(Lord $lord, $timestamp)
    {
        $query = $this->createQueryBuilder('e')
            ->where('e.lord = :lord')
            ->orderBy('e.timestamp', 'DESC')
            ->setParameter('lord', $lord);

        if (null != $timestamp) {
            $query->andWhere('e.timestamp = :timestamp')
                ->setParameter('timestamp', $timestamp);
        }

        return $query->getQuery()->getResult();
    }

    public function findMostRecentDate(Lord $lord): ?CityLog
    {
        $query = $this->createQueryBuilder('e')
            ->where('e.lord = :lord')
            ->orderBy('e.timestamp', 'DESC')
            ->setParameter('lord', $lord)
            ->getQuery()
            ->setMaxResults(1);

        return $query->getResult()[0];
    }

    public function deleteLogs(Lord $lord): void
    {
        $query = $this->createQueryBuilder('log')
            ->delete()
            ->where('log.lord = :id')
            ->setParameter('id', $lord->getId());

        $query->getQuery()->execute();
    }
}
