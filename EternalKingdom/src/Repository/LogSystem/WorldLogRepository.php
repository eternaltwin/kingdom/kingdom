<?php

namespace App\Repository\LogSystem;

use App\Entity\LogSystem\WorldLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WorldLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorldLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorldLog[]    findAll()
 * @method WorldLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorldLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WorldLog::class);
    }
}
