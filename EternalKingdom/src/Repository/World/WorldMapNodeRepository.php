<?php

namespace App\Repository\World;

use App\Entity\City;
use App\Entity\World\WorldMap;
use App\Entity\World\WorldMapNode;
use App\Entity\World\WorldNodeData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WorldMapNode|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorldMapNode|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorldMapNode[]    findAll()
 * @method WorldMapNode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorldMapNodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WorldMapNode::class);
    }

    /**
     * Returns the list of available WorldMapNode that can be occupied by a new player.
     *
     * @return WorldMapNode[] Returns an array of WorldMapNode objects
     */
    public function findAvailableCities(int $worldId): array
    {
        $qb = $this->createQueryBuilder('node')
            ->innerJoin(WorldNodeData::class, 'nodeData', Join::WITH, 'node.data = nodeData.id')
            ->leftJoin(City::class, 'c', Join::WITH, 'node.id = c.node')
            ->andWhere('nodeData.isSpawnPoint = true')
            ->andWhere('node.owningWorld = :worldId')
            ->andWhere('c.id IS NULL')
            ->setParameter('worldId', $worldId);

        return $qb->getQuery()->getResult();
    }

    public function findClaimedNodes(int $worldId): array
    {
        $qb = $this->createQueryBuilder('node')
            ->where('node.owningWorld = :worldId')
            ->andWhere('node.sovereign IS NOT NULL')
            ->setParameter('worldId', $worldId);

        return $qb->getQuery()->getResult();
    }

    /**
     * Find all nodes of the specified path and returns a list of Nodes in the proper order.
     *
     * @param int[] $path
     *
     * @return WorldMapNode[]
     */
    public function findNodesRelatedToPath(WorldMap $worldMap, array $path): array
    {
        $qb = $this->createQueryBuilder('node')
            ->join(WorldNodeData::class, 'nodeData', Join::WITH, 'node.data = nodeData.id')
            ->where('node.owningWorld = :worldId')
            ->andWhere('nodeData.graphIndex IN (:path)')
            ->setParameter('worldId', $worldMap)
            ->setParameter('path', $path);

        /** @var WorldMapNode[] $result */
        $result = $qb->getQuery()->getResult();

        /** @var $pathDictionnary (Dictionary <Node Graph Index with WorldMapNode>) */
        $pathDictionnary = [];
        foreach ($result as $node) {
            $pathDictionnary[$node->getData()->getGraphIndex()] = $node;
        }

        $nodesInRightOrder = [];
        foreach ($path as $graphIndex) {
            $nodesInRightOrder[] = $pathDictionnary[$graphIndex];
        }

        return $nodesInRightOrder;
    }
}
