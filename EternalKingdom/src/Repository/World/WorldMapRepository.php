<?php

namespace App\Repository\World;

use App\Entity\World\WorldMap;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WorldMap|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorldMap|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorldMap[]    findAll()
 * @method WorldMap[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorldMapRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WorldMap::class);
    }

    public function findAllWorldsWithAvailableSlots(): array
    {
        $queryBuilder = $this->createQueryBuilder('world')
            ->select('world')
            ->leftJoin('world.lords', 'l')
            ->groupBy('world.id')
            ->having('count(l) < world.maxPlayerCount')
            ->orderBy('world.language');

        return $queryBuilder->getQuery()->execute();
    }

    public function findAllWorldsWithAvailableSlotsPerLanguage(string $language): array
    {
        $queryBuilder = $this->createQueryBuilder('world')
            ->select('world')
            ->where('world.language = :language')
            ->leftJoin('world.lords', 'l')
            ->groupBy('world.id')
            ->having('count(l) < world.maxPlayerCount')
            ->setParameter('language', $language);

        return $queryBuilder->getQuery()->getResult();
    }
}
