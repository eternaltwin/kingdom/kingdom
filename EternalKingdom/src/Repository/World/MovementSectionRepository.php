<?php

namespace App\Repository\World;

use App\Entity\World\MovementSection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MovementSection|null find($id, $lockMode = null, $lockVersion = null)
 * @method MovementSection|null findOneBy(array $criteria, array $orderBy = null)
 * @method MovementSection[]    findAll()
 * @method MovementSection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MovementSectionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MovementSection::class);
    }
}
