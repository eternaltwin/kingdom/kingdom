<?php

namespace App\Repository\World;

use App\Entity\World\WorldData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WorldData|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorldData|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorldData[]    findAll()
 * @method WorldData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorldDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WorldData::class);
    }

    public function getAllWorldDataNames(): array
    {
        $qb = $this->createQueryBuilder('worldData')
            ->select('worldData.mapFileName');

        return $qb->getQuery()->getSingleColumnResult();
    }
}
