<?php

namespace App\Repository\World;

use App\Entity\World\WorldNodeData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

// use Doctrine\ORM\Query\Expr\Join;
// use function Doctrine\ORM\QueryBuilder;

/**
 * @method WorldNodeData|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorldNodeData|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorldNodeData[]    findAll()
 * @method WorldNodeData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorldNodeDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WorldNodeData::class);
    }

    // Only kept that as an example of subquery!
    /*
     * Returns the list of available cities (not owned by any player).
     * @return WorldNodeData[] Returns an array of WorldNodeData objects
     */
    /*
    public function findAvailableNodes($worldId): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $sub = $this->getEntityManager()->createQueryBuilder();

        $sub = $sub->select("nd.id")
            ->from('App:World\WorldMapNode', 'node')
            ->join('node.data', 'nd', Join::WITH, 'node.data = nd.id')
            ->where('node.owner IS NULL')
            ->where('city.owningWorld = :worldId')
        ;

        $qb = $qb->select('node')
            ->from('App:World\WorldNodeData', 'node')
            ->where($qb->expr()->In('node.id', $sub->distinct()->getDQL()))
            ->setParameter('worldId', $worldId);

        return $qb->getQuery()->getResult();
    }
    */
}
