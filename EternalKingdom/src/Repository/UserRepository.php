<?php

namespace App\Repository;

use App\Entity\User;
use App\Kingdom\User\Interface\CurrentUserProviderInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

class UserRepository extends ServiceEntityRepository implements CurrentUserProviderInterface
{
    public function __construct(
        ManagerRegistry $registry,
        private readonly Security $security
    ) {
        parent::__construct($registry, User::class);
    }

    public function getCurrentUser(): ?User
    {
        $account = $this->security->getUser();
        if ($account instanceof User) {
            return $account;
        }

        return null;
    }

    public function findAllAlivePlayers(): array
    {
        return $this->createQueryBuilder('user')
            ->where('user.lord IS NOT NULL')
            ->getQuery()
            ->getResult();
    }

    public function findAllInactivePlayers(): array
    {
        return $this->createQueryBuilder('user')
            ->where('user.lord IS NOT NULL')
            ->andWhere('user.isInactiveInGame = TRUE')
            ->getQuery()
            ->getResult();
    }

    public function findMostRecentConnectedPlayers(int $days): int
    {
        return $this->createQueryBuilder('user')
            ->select('count(user.id)')
            ->where('user.lastConnection >= DATE_SUB(CURRENT_TIMESTAMP(), :days, \'DAY\')')
            ->setParameter('days', $days)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function updatePlayersInactivityStatus(int $days): void
    {
        $this->createQueryBuilder('user')
            ->update()
            ->set('user.isInactiveInGame', 'TRUE')
            ->where('user.lord IS NOT NULL')
            ->andWhere('user.isInactiveInGame = FALSE')
            ->andWhere('user.lastConnection < DATE_SUB(CURRENT_TIMESTAMP(), :days, \'DAY\')')
            ->setParameter('days', $days)
            ->getQuery()
            ->execute();
    }

    public function findAllPlayersWithoutLord(): array
    {
        return $this->createQueryBuilder('user')
            ->where('user.lord IS NULL')
            ->getQuery()
            ->getResult();
    }

    public function findAllPlayersWithRole(string $role): array
    {
        return $this->createQueryBuilder('user')
            ->where('JSON_GET_TEXT(user.roles,0) = :role')
            ->setParameter('role', $role)
            ->getQuery()
            ->getResult();
    }
}
