<?php

namespace App\Repository;

use App\Entity\City;
use App\Entity\World\WorldMapNode;
use App\Entity\World\WorldNodeData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method City|null find($id, $lockMode = null, $lockVersion = null)
 * @method City|null findOneBy(array $criteria, array $orderBy = null)
 * @method City[]    findAll()
 * @method City[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, City::class);
    }

    /**
     * Returns the list of alive players ordered by descending age.
     *
     * @return City[] Returns an array of Lord objects
     */
    public function getAllCitiesInWorld(int $worldId): array
    {
        return $this->createQueryBuilder('c')
            ->select(['nodeData.name', 'c.dungeonLevel', 'c.housesCount'])
            ->join(WorldMapNode::class, 'node', Join::WITH, 'c.node = node.id')
            ->join(WorldNodeData::class, 'nodeData', Join::WITH, 'node.data = nodeData.id')
            ->where('node.owningWorld = :id')
            ->setParameter('id', $worldId)
            ->getQuery()
            ->getResult();
    }
}
