<?php

namespace App\Security;

use App\Service\OAuthService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

/**
 * Symfony authenticator: https://symfony.com/doc/current/security/guard_authentication.html#step-2-create-the-authenticator-class.
 */
class OAuthTokenAuthenticator extends AbstractGuardAuthenticator
{
    public function __construct(private readonly OAuthService $oauthService, private readonly RouterInterface $router)
    {
    }

    #[\Override]
    public function supports(Request $request): bool
    {
        return 'oauth_callback' === $request->get('_route') && !empty($request->get('code'));
    }

    #[\Override]
    public function getCredentials(Request $request)
    {
        return $request->get('code');
    }

    #[\Override]
    public function getUser($credentials, UserProviderInterface $userProvider): ?UserInterface
    {
        if (null === $credentials) {
            // The token header was empty, authentication fails with HTTP Status
            // Code 401 "Unauthorized"
            return null;
        }

        return $this->oauthService->login($credentials);
    }

    #[\Override]
    public function checkCredentials($credentials, UserInterface $user): bool
    {
        return true;
    }

    #[\Override]
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return new Response('login error', Response::HTTP_UNAUTHORIZED);
    }

    #[\Override]
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey): ?Response
    {
        $authentificationState = $request->get('state');

        if (!$authentificationState) {
            $response = $this->router->generate('welcome');
        } else {
            $response = $this->router->generate('homepage');
        }

        return new RedirectResponse($response);
    }

    #[\Override]
    public function start(Request $request, ?AuthenticationException $authException = null): ?Response
    {
        return new RedirectResponse($this->oauthService->getAuthorizationUri('base', $request->getRequestUri()));
    }

    #[\Override]
    public function supportsRememberMe(): bool
    {
        return false;
    }
}
