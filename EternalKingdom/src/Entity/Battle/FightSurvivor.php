<?php

namespace App\Entity\Battle;

use App\Entity\Resources;
use Doctrine\ORM\Mapping as ORM;

/**
 * The role of this class is to keep track of Unit that survived a fight but lost some health.
 *
 * @ORM\Entity(repositoryClass=App\Repository\Battle\FightSurvivorRepository::class)
 */
class FightSurvivor implements \JsonSerializable
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Resources::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Resources $unitType;

    /**
     * @ORM\ManyToOne(targetEntity=EngagedArmy::class, inversedBy="fightSurvivors")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private EngagedArmy $owningEngagedArmy;

    /**
     * @ORM\Column(type="integer")
     */
    private int $remainingHealth;

    public function __construct(
        Resources $unitType,
        EngagedArmy $owningEngagedArmy,
        int $remainingHealth
    ) {
        $this->unitType = $unitType;
        $this->owningEngagedArmy = $owningEngagedArmy;
        $this->remainingHealth = $remainingHealth;
    }

    public function getUnitType(): Resources
    {
        return $this->unitType;
    }

    public function getOwningEngagedArmy(): EngagedArmy
    {
        return $this->owningEngagedArmy;
    }

    public function getRemainingHealth(): int
    {
        return $this->remainingHealth;
    }

    #[\Override]
    public function jsonSerialize(): array
    {
        return [
            'owningArmy' => $this->owningEngagedArmy->getArmyContainer()->getId(),
            'name' => $this->getUnitType()->getName(),
            'health' => $this->getRemainingHealth(),
        ];
    }
}
