<?php

namespace App\Entity\Battle;

use Doctrine\ORM\Mapping as ORM;

use App\Service\WorldManager;

/**
 * A fight between two units during a Battle.
 *
 * @ORM\Entity(repositoryClass=App\Repository\Battle\FightRepository::class)
 */
class Fight implements \JsonSerializable
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $startTime;

    /**
     * @ORM\ManyToOne(targetEntity=Battle::class, inversedBy="fights")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Battle $battle;

    /**
     * @ORM\OneToOne(targetEntity=FightingUnit::class, cascade={"persist", "remove"})
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private FightingUnit $attackingUnit;

    /**
     * @ORM\OneToOne(targetEntity=FightingUnit::class, cascade={"persist", "remove"})
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private FightingUnit $defendingUnit;

    public function __construct(
        Battle $battle,
        FightingUnit $attackingUnit,
        FightingUnit $defendingUnit
    ) {
        $this->startTime = new \DateTime();
        $this->battle = $battle;
        $this->attackingUnit = $attackingUnit;
        $this->defendingUnit = $defendingUnit;
    }

    public function getBattle(): Battle
    {
        return $this->battle;
    }

    public function getStartTime(): \DateTime
    {
        return $this->startTime;
    }

    public function getEstimatedDuration(): int
    {
        $estimatedTickToKillDefender = round($this->getDefendingUnit()->getRemainingHealth() / ($this->getAttackingUnit()->getDamageBonus() + 1.5));
        $estimatedTickToKillAttacker = round($this->getAttackingUnit()->getRemainingHealth() / ($this->getDefendingUnit()->getDamageBonus() + 1.5));

        return WorldManager::K_DELAY_BETWEEN_WORLD_UPDATE * min($estimatedTickToKillAttacker, $estimatedTickToKillDefender);
    }

    public function getAttackingUnit(): FightingUnit
    {
        return $this->attackingUnit;
    }

    public function getDefendingUnit(): FightingUnit
    {
        return $this->defendingUnit;
    }

    #[\Override]
    public function jsonSerialize(): array
    {
        return [
            'attackingUnit' => $this->attackingUnit,
            'defendingUnit' => $this->defendingUnit,
            'estimatedDuration' => $this->getEstimatedDuration(),
        ];
    }
}
