<?php

namespace App\Entity\Battle;

use App\Entity\Resources;
use App\Entity\UnitArmy;
use Doctrine\ORM\Mapping as ORM;

/**
 * Detailed Stats about each unit of an engaged army within a Battle.
 *
 * @ORM\Entity()
 */
class BattleUnitStat
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity = EngagedArmy::class, inversedBy = "stats")
     *
     * @ORM\JoinColumn(nullable = false)
     */
    private EngagedArmy $relatedArmy;

    /**
     * @ORM\ManyToOne(targetEntity=Resources::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Resources $unit;

    /**
     * @ORM\Column(type="integer")
     */
    private int $healthyCount = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private int $injuredCount = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private int $deadCount = 0;

    public function __construct(UnitArmy $unitArmy)
    {
        $this->unit = $unitArmy->getResource();
        $this->healthyCount = $unitArmy->getAmount();
    }

    public function setRelatedArmy(EngagedArmy $army)
    {
        $this->relatedArmy = $army;
    }

    public function getRelatedArmy(): EngagedArmy
    {
        return $this->relatedArmy;
    }

    public function getHealthyCount(): int
    {
        return $this->healthyCount;
    }

    public function setHealthyCount(int $healthyCount): void
    {
        $this->healthyCount = $healthyCount;
    }

    public function getInjuredCount(): int
    {
        return $this->injuredCount;
    }

    public function setInjuredCount(int $injuredCount): void
    {
        $this->injuredCount = $injuredCount;
    }

    public function getDeadCount(): int
    {
        return $this->deadCount;
    }

    public function setDeadCount(int $deadCount): void
    {
        $this->deadCount = $deadCount;
    }

    public function getTotalCount(): int
    {
        return $this->getHealthyCount() + $this->getInjuredCount() + $this->getDeadCount();
    }

    public function getUnit(): Resources
    {
        return $this->unit;
    }
}
