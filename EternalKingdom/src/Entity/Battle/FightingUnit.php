<?php

namespace App\Entity\Battle;

use App\Entity\ArmyContainer;
use App\Entity\Resources;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=App\Repository\Battle\FightingUnitRepository::class)
 */
class FightingUnit implements \JsonSerializable
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Resources::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Resources $unitType;

    /**
     * @ORM\ManyToOne(targetEntity=EngagedArmy::class, inversedBy="fighters")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private EngagedArmy $owningEngagedArmy;

    /**
     * @ORM\Column(type="integer")
     */
    private int $remainingHealth;

    /**
     * @ORM\Column(type="integer")
     */
    private int $damageBonus;

    public function __construct(
        Resources $unitType,
        EngagedArmy $owningEngagedArmy,
        int $initialHealth
    ) {
        $this->unitType = $unitType;
        $this->owningEngagedArmy = $owningEngagedArmy;
        $this->remainingHealth = $initialHealth;
        $this->damageBonus = 0;
    }

    public function getUnitType(): Resources
    {
        return $this->unitType;
    }

    public function getOwningEngagedArmy(): EngagedArmy
    {
        return $this->owningEngagedArmy;
    }

    public function getRemainingHealth(): int
    {
        return $this->remainingHealth;
    }

    public function applyDamage(int $damage): void
    {
        $this->remainingHealth = max(0, $this->remainingHealth - $damage);
    }

    public function getDamageBonus(): int
    {
        return $this->damageBonus;
    }

    public function setDamageBonus(int $damageBonus): void
    {
        $this->damageBonus = $damageBonus;
    }

    #[\Override]
    public function jsonSerialize(): array
    {
        // TODO Send damage bonus to display it in front!
        return [
            'owningArmy' => $this->owningEngagedArmy->getArmyContainer()->getId(),
            'name' => $this->getUnitType()->getName(),
            'health' => $this->getRemainingHealth(),
        ];
    }
}
