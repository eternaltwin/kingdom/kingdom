<?php

namespace App\Entity\Battle;

use App\Entity\ArmyContainer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Army used during a Battle.
 *
 * @ORM\Entity()
 */
class EngagedArmy
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type = "integer")
     */
    private int $id;

    /**
     * All engaged units in the battle.
     *
     * @ORM\OneToOne(targetEntity = ArmyContainer::class, cascade={"persist", "remove"})
     */
    private ArmyContainer $armyContainer;

    /**
     * @ORM\ManyToOne(targetEntity = Battle::class, inversedBy = "engagedArmies")
     *
     * @ORM\JoinColumn(nullable = false)
     */
    private Battle $owningBattle;

    /**
     * Reference to the army that should recover all the engaged units once the Battle ends.
     *
     * @ORM\OneToOne(targetEntity = ArmyContainer::class)
     *
     * @ORM\JoinColumn(nullable = true)
     */
    private ?ArmyContainer $linkedArmy;

    /**
     * @ORM\OneToMany(targetEntity=FightingUnit::class, mappedBy="owningEngagedArmy")
     */
    private Collection $fighters;

    /**
     * @ORM\OneToMany(targetEntity=FightSurvivor::class, mappedBy="owningEngagedArmy")
     */
    private Collection $fightSurvivors;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isAttacking;

    /**
     * @ORM\OneToMany(targetEntity=BattleUnitStat::class, mappedBy="relatedArmy", cascade={"persist", "remove"})
     *
     * @var Collection<BattleUnitStat>
     */
    private Collection $stats;

    public function __construct(
        Battle $owningBattle,
        ArmyContainer $linkedArmy,
        bool $isAttacking
    ) {
        $this->owningBattle = $owningBattle;
        $this->isAttacking = $isAttacking;
        $this->linkedArmy = $linkedArmy;
        $this->stats = new ArrayCollection();
        $this->fightSurvivors = new ArrayCollection();
        $this->fighters = new ArrayCollection();

        $this->armyContainer = new ArmyContainer();
        $this->armyContainer->overrideUnits($linkedArmy->getUnits());
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getOwningBattle(): Battle
    {
        return $this->owningBattle;
    }

    public function isAttacking(): bool
    {
        return $this->isAttacking;
    }

    public function getArmyContainer(): ArmyContainer
    {
        return $this->armyContainer;
    }

    public function getLinkedArmy(): ?ArmyContainer
    {
        return $this->linkedArmy;
    }

    public function clearLinkedArmy(): void
    {
        $this->linkedArmy = null;
    }

    public function getStats(): Collection
    {
        return $this->stats;
    }

    public function addBattleStat(BattleUnitStat $battleUnitStat): void
    {
        $battleUnitStat->setRelatedArmy($this);
        $this->stats->add($battleUnitStat);
    }

    public function getBattleStat(string $unitName): ?BattleUnitStat
    {
        foreach ($this->stats as $stat) {
            if ($stat->getUnit()->getName() == $unitName) {
                return $stat;
            }
        }

        return null;
    }

    /**
     * @return Collection<FightingUnit>
     */
    public function getFightingUnits(): Collection
    {
        return $this->fighters;
    }

    /**
     * @return Collection<FightSurvivor>
     */
    public function getFightSurvivors(): Collection
    {
        return $this->fightSurvivors;
    }
}
