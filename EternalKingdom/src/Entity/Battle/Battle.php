<?php

namespace App\Entity\Battle;

use App\Entity\ArmyContainer;
use App\Entity\General;
use App\Entity\World\WorldMap;
use App\Entity\World\WorldMapNode;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * A Battle is opposing two BattleSide on a specific location.
 *
 * @ORM\Entity(repositoryClass=App\Repository\Battle\BattleRepository::class)
 */
class Battle implements \JsonSerializable
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $startTime;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isFinished = false;

    /**
     * @ORM\OneToMany(targetEntity=General::class, mappedBy="battle", orphanRemoval=true, cascade={"persist", "remove"})
     *
     * @var Collection<General>
     */
    private Collection $engagedGenerals;

    /**
     * @ORM\OneToMany(targetEntity=EngagedArmy::class, mappedBy="owningBattle", cascade={"persist", "remove"})
     */
    private Collection $engagedArmies;

    /**
     * @ORM\OneToMany(targetEntity=Fight::class, mappedBy="battle", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private Collection $fights;

    /**
     * @ORM\ManyToOne(targetEntity=WorldMap::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private WorldMap $worldMap;

    /**
     * @ORM\ManyToOne(targetEntity=WorldMapNode::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private WorldMapNode $location;

    public function __construct(
        WorldMapNode $location,
    ) {
        $this->startTime = new \DateTime();

        $this->engagedGenerals = new ArrayCollection();
        $this->engagedArmies = new ArrayCollection();
        $this->fights = new ArrayCollection();

        $this->worldMap = $location->getOwningWorld();
        $this->location = $location;
    }

    #[\Override]
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'locationName' => $this->location->getData()->getName(),
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getWorldMap(): WorldMap
    {
        return $this->worldMap;
    }

    public function getLocation(): WorldMapNode
    {
        return $this->location;
    }

    public function getStartTime(): \DateTime
    {
        return $this->startTime;
    }

    public function isFinished(): bool
    {
        return $this->isFinished;
    }

    public function markFinished(): void
    {
        $this->isFinished = true;
    }

    /**
     * @return Collection<General>
     */
    public function getEngagedGenerals(): Collection
    {
        return $this->engagedGenerals;
    }

    public function getDefendingGenerals(): array
    {
        $defendingGenerals = [];
        $defendingArmies = $this->getDefensingArmies();
        $allGenerals = $this->getEngagedGenerals();

        foreach ($defendingArmies as $defendingArmy) {
            foreach ($allGenerals as $general) {
                if ($defendingArmy->getLinkedArmy() === $general->getArmyContainer()) {
                    $defendingGenerals[] = $general;
                }
            }
        }

        return $defendingGenerals;
    }

    public function engageGeneral(General $general, bool $isAttacking): void
    {
        $this->engagedGenerals->add($general);
        $general->setBattle($this);

        $this->engageArmy($general->getArmyContainer(), $isAttacking);
    }

    /**
     * @return Collection<EngagedArmy>
     */
    public function getAllEngagedArmies(): Collection
    {
        return $this->engagedArmies;
    }

    // TODO Rename Defending + twig
    /**
     * @return array<EngagedArmy>
     */
    public function getDefensingArmies(): array
    {
        $defendingArmies = [];
        /** @var EngagedArmy $engagedArmy */
        foreach ($this->engagedArmies as $engagedArmy) {
            if (!$engagedArmy->isAttacking()) {
                $defendingArmies[] = $engagedArmy;
            }
        }

        return $defendingArmies;
    }

    /**
     * @return array<EngagedArmy>
     */
    public function getAttackingArmies(): array
    {
        $attackingArmies = [];
        /** @var EngagedArmy $engagedArmy */
        foreach ($this->engagedArmies as $engagedArmy) {
            if ($engagedArmy->isAttacking()) {
                $attackingArmies[] = $engagedArmy;
            }
        }

        return $attackingArmies;
    }

    public function engageArmy(ArmyContainer $army, bool $isAttacking): void
    {
        $this->engagedArmies->add(new EngagedArmy($this, $army, $isAttacking));
    }

    /**
     * @return Collection<Fight>
     */
    public function getFights(): Collection
    {
        return $this->fights;
    }

    public function addFight(Fight $fight): void
    {
        $this->fights->add($fight);
    }
}
