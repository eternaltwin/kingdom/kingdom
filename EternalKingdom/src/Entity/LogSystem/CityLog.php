<?php

namespace App\Entity\LogSystem;

use App\Entity\Lord;
use Doctrine\ORM\Mapping as ORM;

/**
 * Log associated with a specific City (displayed in City).
 *
 * @ORM\Table(name="log_city")
 *
 * @ORM\Entity(repositoryClass=App\Repository\LogSystem\CityLogRepository::class)
 */
class CityLog extends Log
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Lord::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Lord $lord;

    public function __construct(
        Lord $lord,
        LogEvent $event,
        array $params
    ) {
        $this->lord = $lord;
        parent::__construct($event, $params);
    }

    public function getLord(): ?Lord
    {
        return $this->lord;
    }
}
