<?php

namespace App\Entity\LogSystem;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Base class for all Logs of the game.
 *
 * @ORM\MappedSuperclass()
 */
class Log
{
    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $timestamp;

    /**
     * @ORM\ManyToOne(targetEntity=LogEvent::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private LogEvent $event;

    /**
     * @var array<mixed>
     *
     * @ORM\Column(type="json", nullable=true)
     */
    private array $dynamicData;

    /**
     * @param array<mixed> $dynamicData
     */
    public function __construct(
        LogEvent $event,
        array $dynamicData
    ) {
        $this->event = $event;
        $this->dynamicData = $dynamicData;
        $this->timestamp = new \DateTime();
    }

    public function getEvent(): ?LogEvent
    {
        return $this->event;
    }

    public function getTimestamp(): ?\DateTimeInterface
    {
        return $this->timestamp;
    }

    public function getDynamicData(): ?array
    {
        return $this->dynamicData;
    }

    /**
     * TODO Only use that in a development environment since it can slow performance!
     *
     * @throws \Exception
     */
    public function checkDynamicParameter(string $paramName): mixed
    {
        if (false == array_key_exists($paramName, $this->getDynamicData())) {
            throw new \Exception("The Log '".$this->event->getEventName()."' doesn't contains the dynamic data '".$paramName."'. Please verify that your log call matches the definition in EKLogsLoader");
        }

        return $this->dynamicData[$paramName];
    }
}
