<?php

namespace App\Entity\LogSystem;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=App\Repository\LogSystem\LogEventRepository::class)
 */
class LogEvent
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * Both used as a unique identifier and as translation key.
     *
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * Expected dynamic data.
     *
     * @ORM\Column(type="json", nullable=true)
     */
    private array $expectedData = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEventName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getExpectedData(): ?array
    {
        return $this->expectedData;
    }

    public function setExpectedData(?array $expectedData): self
    {
        $this->expectedData = $expectedData;

        return $this;
    }
}
