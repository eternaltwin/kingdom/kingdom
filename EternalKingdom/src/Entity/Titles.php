<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TODO Rename Title (no s)
 * Title info / requirement.
 *
 * @ORM\Entity()
 */
class Titles
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $name;

    /**
     * Required glory.
     *
     * @ORM\Column(type="integer")
     */
    private int $glory;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getGlory(): ?int
    {
        return $this->glory;
    }

    public function setGlory(int $glory): self
    {
        $this->glory = $glory;

        return $this;
    }
}
