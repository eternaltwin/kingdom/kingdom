<?php

namespace App\Entity;

use App\Repository\ResourceConversionSchemasRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ResourceConversionSchemasRepository::class)
 */
class ResourceConversionSchemas
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Constructions::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Constructions $requiredBuilding;

    /**
     * @ORM\Column(type="integer")
     */
    private int $requiredLevel;

    /**
     * @ORM\ManyToOne(targetEntity=Resources::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Resources $requiredResource;

    /**
     * @ORM\Column(type="integer")
     */
    private int $requiredAmount;

    /**
     * @ORM\ManyToOne(targetEntity=Resources::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Resources $producedResource;

    /**
     * @ORM\Column(type="integer")
     */
    private int $producedAmount;

    public function getId(): int
    {
        return $this->id;
    }

    public function getRequiredBuilding(): Constructions
    {
        return $this->requiredBuilding;
    }

    public function setRequiredBuilding(?Constructions $requiredBuilding): self
    {
        $this->requiredBuilding = $requiredBuilding;

        return $this;
    }

    public function getRequiredLevel(): int
    {
        return $this->requiredLevel;
    }

    public function setRequiredLevel(int $requiredLevel): self
    {
        $this->requiredLevel = $requiredLevel;

        return $this;
    }

    public function getRequiredResource(): Resources
    {
        return $this->requiredResource;
    }

    public function setRequiredResource(?Resources $requiredResource): self
    {
        $this->requiredResource = $requiredResource;

        return $this;
    }

    public function getRequiredAmount(): ?int
    {
        return $this->requiredAmount;
    }

    public function setRequiredAmount(int $requiredAmount): self
    {
        $this->requiredAmount = $requiredAmount;

        return $this;
    }

    public function getProducedResource(): ?Resources
    {
        return $this->producedResource;
    }

    public function setProducedResource(?Resources $producedResource): self
    {
        $this->producedResource = $producedResource;

        return $this;
    }

    public function getProducedAmount(): int
    {
        return $this->producedAmount;
    }

    public function setProducedAmount(int $producedAmount): self
    {
        $this->producedAmount = $producedAmount;

        return $this;
    }
}
