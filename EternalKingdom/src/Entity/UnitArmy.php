<?php

namespace App\Entity;

use App\Repository\UnitArmyRepository;
use Doctrine\ORM\Mapping as ORM;

// TODO Rename ArmyUnit
/**
 * @ORM\Entity(repositoryClass=UnitArmyRepository::class)
 */
class UnitArmy
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=ArmyContainer::class, inversedBy="units")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private ArmyContainer $container;

    /**
     * @ORM\ManyToOne(targetEntity=Resources::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Resources $resource;

    /**
     * @ORM\Column(type="integer")
     */
    private int $amount;

    public function __construct(
        Resources $resource,
        int $amount
    ) {
        $this->resource = $resource;
        $this->amount = $amount;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContainer(): ArmyContainer
    {
        return $this->container;
    }

    public function setContainer(ArmyContainer $container): self
    {
        $this->container = $container;

        return $this;
    }

    public function getResource(): Resources
    {
        return $this->resource;
    }

    public function setResource(Resources $resource): self
    {
        $this->resource = $resource;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
}
