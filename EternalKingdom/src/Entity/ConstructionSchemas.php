<?php

namespace App\Entity;

use App\Repository\ConstructionSchemasRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConstructionSchemasRepository::class)
 */
class ConstructionSchemas
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Constructions::class, inversedBy="constructionSchemas")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Constructions $construction;

    /**
     * @ORM\Column(type="integer")
     */
    private int $level;

    /**
     * @ORM\ManyToOne(targetEntity=Resources::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Resources $resource;

    /**
     * @ORM\Column(type="integer")
     */
    private int $amount;

    public function getId(): int
    {
        return $this->id;
    }

    public function getConstruction(): ?Constructions
    {
        return $this->construction;
    }

    public function setConstruction(?Constructions $construction): self
    {
        $this->construction = $construction;

        return $this;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getResource(): ?Resources
    {
        return $this->resource;
    }

    public function setResource(?Resources $resource): self
    {
        $this->resource = $resource;

        return $this;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
}
