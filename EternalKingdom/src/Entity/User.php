<?php

namespace App\Entity;

use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 *
 * @ORM\Table(name="`user`", indexes = {@Index(name = "eternaltwin_idx", fields = {"userId"}) })
 */
class User implements UserInterface
{
    public const string ROLE_ADMIN = 'ROLE_ADMIN';
    public const string ROLE_MODERATOR = 'ROLE_MODERATOR';
    public const string ROLE_USER = 'ROLE_USER';

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="json")
     */
    private array $roles = [];

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private \DateTime $lastConnection;

    /* GAMEPLAY */

    /**
     * @ORM\OneToOne(targetEntity=Lord::class, mappedBy="user", cascade={"persist", "remove"})
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Lord $lord = null;

    /**
     * @ORM\Column(type="boolean", options={ "default" : 0 })
     */
    private bool $isInactiveInGame = false;

    /**
     * User EternalTwin id, it should be unique.
     *
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private string $userId;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private string $displayName;

    public function __construct(
        string $userId,
        string $displayName,
    ) {
        $this->lastConnection = new \DateTime();
        $this->userId = $userId;
        $this->displayName = $displayName;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getDisplayName(): ?string
    {
        return $this->displayName;
    }

    public function setDisplayName(string $displayName): self
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * @see UserInterface
     */
    #[\Override]
    public function getUsername(): string
    {
        return $this->displayName;
    }

    /**
     * @see UserInterface
     */
    #[\Override]
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    #[\Override]
    public function getPassword(): ?string
    {
        // not needed for apps that do not check user passwords
        return null;
    }

    /**
     * @see UserInterface
     */
    #[\Override]
    public function getSalt(): ?string
    {
        // not needed for apps that do not check user passwords
        return null;
    }

    /**
     * @see UserInterface
     */
    #[\Override]
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getUserIdentifier(): string
    {
        return $this->getUsername();
    }

    /* Gameplay Getter/Setter */

    public function getLord(): ?Lord
    {
        return $this->lord ?? null;
    }

    public function setLord(?Lord $lord): self
    {
        $this->lord = $lord;

        return $this;
    }

    public function getLastConnection(): \DateTime
    {
        return $this->lastConnection;
    }

    public function setLastConnectionDate(\DateTime $lastConnection): void
    {
        $this->lastConnection = $lastConnection;
    }

    public function isInactiveInGame(): bool
    {
        return $this->isInactiveInGame;
    }

    public function setIsInactiveInGame(bool $isInactiveInGame): void
    {
        $this->isInactiveInGame = $isInactiveInGame;
    }
}
