<?php

namespace App\Entity\World;

use Doctrine\ORM\Mapping as ORM;

/**
 * Resource spot represents a place where Lords can collect special resource until it is depleted.
 *
 * @ORM\Entity(repositoryClass=App\Repository\World\MovementSectionRepository::class)
 */
class MovementSection
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=MovementOrder::class, inversedBy="path")
     *
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private MovementOrder $owningOrder;

    /**
     * Travel duration in seconds.
     *
     * @ORM\Column(type="integer")
     */
    private int $travelDuration;

    /**
     * @ORM\ManyToOne(targetEntity=WorldMapNode::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private WorldMapNode $start;

    /**
     * @ORM\ManyToOne(targetEntity=WorldMapNode::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private WorldMapNode $end;

    public function __construct(
        WorldMapNode $start,
        WorldMapNode $end
    ) {
        $this->start = $start;
        $this->end = $end;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getOwningOrder(): MovementOrder
    {
        return $this->owningOrder;
    }

    public function setOwningOrder(MovementOrder $owningOrder): void
    {
        $this->owningOrder = $owningOrder;
    }

    public function getStart(): WorldMapNode
    {
        return $this->start;
    }

    public function getEnd(): WorldMapNode
    {
        return $this->end;
    }

    public function getTravelDuration(): int
    {
        return $this->travelDuration;
    }

    public function setTravelDuration(int $travelDuration): void
    {
        $this->travelDuration = $travelDuration;
    }
}
