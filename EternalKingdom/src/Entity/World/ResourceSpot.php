<?php

namespace App\Entity\World;

use App\Entity\Resources;
use Doctrine\ORM\Mapping as ORM;

// TODO Investigate if we can ensure unity thanks to @ORM\UniqueEntity
/**
 * Resource spot represents a place where Lords can collect special resource until it is depleted.
 *
 * @ORM\Entity(repositoryClass=App\Repository\World\ResourceSpotRepository::class)
 */
class ResourceSpot implements \JsonSerializable
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;
    /**
     * @ORM\ManyToOne(targetEntity=WorldMap::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private WorldMap $owningWorld;
    /**
     * @ORM\ManyToOne(targetEntity=WorldNodeData::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private WorldNodeData $node;
    /**
     * @ORM\ManyToOne(targetEntity=Resources::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Resources $resource;
    /**
     * @ORM\Column(type="integer")
     */
    private int $amount;

    public function __construct(
        WorldMap $owningWorld,
        WorldNodeData $node,
        Resources $resource,
        int $amount
    ) {
        $this->owningWorld = $owningWorld;
        $this->node = $node;
        $this->resource = $resource;
        $this->amount = $amount;
    }

    #[\Override]
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'locationName' => $this->node->getName(),
            'resource' => $this->resource->getName(),
            'amount' => $this->amount,
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNode(): WorldNodeData
    {
        return $this->node;
    }

    public function getResource(): Resources
    {
        return $this->resource;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function setAmount(int $newAmount): void
    {
        $this->amount = $newAmount;
    }
}
