<?php

namespace App\Entity\World;

use App\Entity\ArmyContainer;
use App\Entity\General;
use App\Entity\Lord;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=App\Repository\World\WorldMapNodeRepository::class)
 */
class WorldMapNode implements \JsonSerializable
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=WorldMap::class, inversedBy="nodes")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private WorldMap $owningWorld;

    /**
     * By default, this is empty
     * TODO players should be able to modify it when they died as Emperor!
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private ?string $name = null;

    /**
     * Tell if a player spawned on it.
     *
     * @ORM\OneToOne(targetEntity=Lord::class)
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Lord $owner = null;

    /**
     * Who own this place?
     *
     * @ORM\ManyToOne(targetEntity=Lord::class, inversedBy="kingdom")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Lord $sovereign = null;

    /**
     * Garrison (Different from the defense on a City).
     *
     * @ORM\OneToOne(targetEntity=ArmyContainer::class, cascade={"persist", "remove"})
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private ArmyContainer $armyContainer;

    /**
     * @ORM\OneToMany(targetEntity=General::class, mappedBy="location")
     * Generals located on the node
     */
    private Collection $generals;

    /**
     * @ORM\Column(type="integer")
     */
    private int $trade = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $hiddenTradeCounter = 0;

    /**
     * Static world node data.
     *
     * @ORM\ManyToOne(targetEntity=WorldNodeData::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private WorldNodeData $data;

    public function __construct(
        WorldNodeData $data,
    ) {
        $this->generals = new ArrayCollection();
        $this->armyContainer = new ArmyContainer();
        $this->data = $data;
    }

    #[\Override]
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'data' => $this->data,
            'customName' => $this->name ?: null,
            'ownerId' => $this->owner instanceof Lord ? $this->owner->getId() : -1,
            'sovereignId' => $this->sovereign instanceof Lord ? $this->sovereign->getId() : -1,
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOwningWorld(): ?WorldMap
    {
        return $this->owningWorld;
    }

    public function setOwningWorld(?WorldMap $owningWorld): self
    {
        $this->owningWorld = $owningWorld;

        return $this;
    }

    public function getName(): string
    {
        if (null !== $this->name) {
            return $this->name;
        }

        return $this->data->getName();
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOwner(): ?Lord
    {
        return $this->owner;
    }

    public function setOwner(?Lord $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getSovereign(): ?Lord
    {
        return $this->sovereign;
    }

    public function setSovereign(?Lord $sovereign): void
    {
        $this->sovereign = $sovereign;
    }

    public function getArmyContainer(): ?ArmyContainer
    {
        return $this->armyContainer;
    }

    public function setArmyContainer(ArmyContainer $armyContainer): self
    {
        $this->armyContainer = $armyContainer;

        return $this;
    }

    /**
     * @return Collection|General[]
     */
    public function getGenerals(): Collection
    {
        return $this->generals;
    }

    public function addGeneral(General $general): self
    {
        if (!$this->generals->contains($general)) {
            $this->generals[] = $general;
            $general->setLocation($this);
        }

        return $this;
    }

    public function removeGeneral(General $general): self
    {
        $this->generals->removeElement($general);

        return $this;
    }

    /**
     * Returns true if there is a general on this place, ready to fight.
     */
    public function isAnyFortifyingGeneralReady(): bool
    {
        /** @var General $general */
        foreach ($this->generals as $general) {
            if ($general->isFortifying()) {
                return $general->getArmyContainer()->hasAnyUnits();
            }
        }

        return false;
    }

    public function getTrade(): ?int
    {
        return $this->trade;
    }

    public function setTrade(int $trade): self
    {
        $this->trade = $trade;

        return $this;
    }

    public function getHiddenTradeCounter(): ?int
    {
        return $this->hiddenTradeCounter;
    }

    public function setHiddenTradeCounter(int $count): self
    {
        $this->hiddenTradeCounter = $count;

        return $this;
    }

    public function getData(): ?WorldNodeData
    {
        return $this->data;
    }

    public function setData(?WorldNodeData $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function doesAdjacencyExist(WorldMapNode $node): bool
    {
        return $this->getData()->isNodeAdjacent($node->getData());
    }
}
