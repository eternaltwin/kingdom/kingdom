<?php

namespace App\Entity\World;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;

/**
 * Static data about a world node.
 *
 * @ORM\Entity(repositoryClass=App\Repository\World\WorldNodeDataRepository::class)
 */
class WorldNodeData implements \JsonSerializable
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=WorldData::class, inversedBy="nodes")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private WorldData $world;

    /**
     * @ORM\ManyToMany(targetEntity=WorldNodeData::class)
     *
     * @JoinTable(name="world_map_node_adjacencies")
     */
    private Collection $adjacencies;

    /**
     * @ORM\Column(type="string")
     */
    private string $name;

    /**
     * @ORM\Column(type="integer")
     */
    private int $graphIndex;

    /**
     * @ORM\Column(type="integer")
     */
    private int $x;

    /**
     * @ORM\Column(type="integer")
     */
    private int $y;

    /**
     * Flag that indicates we can spawn here! If no player owns this node, it is occupied by Barbarians!
     *
     * @ORM\Column(type="boolean")
     */
    private bool $isSpawnPoint;

    public function __construct(
        string $name,
        int $graphIndex,
        int $x,
        int $y,
        bool $isSpawnPoint
    ) {
        $this->name = $name;
        $this->graphIndex = $graphIndex;
        $this->x = $x;
        $this->y = $y;
        $this->isSpawnPoint = $isSpawnPoint;
        $this->adjacencies = new ArrayCollection();
    }

    #[\Override]
    public function jsonSerialize(): array
    {
        return [
            'name' => $this->name,
            'x' => $this->x,
            'y' => $this->y,
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getGraphIndex(): int
    {
        return $this->graphIndex;
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }

    public function getWorld(): ?WorldData
    {
        return $this->world;
    }

    public function setWorld(?WorldData $world): void
    {
        $this->world = $world;
    }

    /**
     * @return Collection|self[]
     */
    public function getAdjacencies(): Collection
    {
        return $this->adjacencies;
    }

    public function addAdjacency(self $adjacency): self
    {
        if (!$this->adjacencies->contains($adjacency)) {
            $this->adjacencies[] = $adjacency;
        }

        return $this;
    }

    public function removeAdjacency(self $adjacency): self
    {
        $this->adjacencies->removeElement($adjacency);

        return $this;
    }

    /**
     * Verify if the specified node is connected to the current one.
     */
    public function isNodeAdjacent(WorldNodeData $nodeData): bool
    {
        $result = false;
        /** @var WorldNodeData $adjacentNodeData */
        foreach ($this->adjacencies as $adjacentNodeData) {
            if ($adjacentNodeData->getId() == $nodeData->getId()) {
                $result = true;
                break;
            }
        }

        if (!$result) {
            dump(sprintf('Node adjacency failed between %d and %d', $this->getId(), $nodeData->getId()));
        }

        return $result;
    }

    public function isSpawnPoint(): bool
    {
        return $this->isSpawnPoint;
    }
}
