<?php

namespace App\Entity\World;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Static data about a map.
 *
 * @ORM\Entity(repositoryClass=App\Repository\World\WorldDataRepository::class)
 */
class WorldData
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\OneToMany(targetEntity=WorldNodeData::class, mappedBy="world", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private Collection $nodes;

    /**
     * World data directory name.
     *
     * @ORM\Column(type="string")
     */
    private string $mapFileName;

    public function __construct(string $mapFileName)
    {
        $this->nodes = new ArrayCollection();
        $this->mapFileName = $mapFileName;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|WorldNodeData[]
     */
    public function getNodes(): Collection
    {
        return $this->nodes;
    }

    public function addNode(WorldNodeData $node): self
    {
        if (!$this->nodes->contains($node)) {
            $this->nodes[] = $node;
            $node->setWorld($this);
        }

        return $this;
    }

    public function removeNode(WorldNodeData $node): self
    {
        // set the owning side to null (unless already changed)
        if ($this->nodes->removeElement($node) && $node->getWorld() === $this) {
            $node->setWorld(null);
        }

        return $this;
    }

    public function getMapFileName(): string
    {
        return $this->mapFileName;
    }
}
