<?php

namespace App\Entity\World;

use App\Entity\General;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Describes the movement order asked by a player to move a general.
 *
 * @ORM\Entity(repositoryClass=App\Repository\World\MovementOrderRepository::class)
 */
class MovementOrder implements \JsonSerializable, \Stringable
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $startTime;

    /**
     * @ORM\Column(type="integer")
     */
    private int $currentMovementSection = 0;

    /**
     * @ORM\Column(type="float")
     */
    private float $initialProgression = 0.;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\General", inversedBy="movementOrder")
     *
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private General $instigator;

    /**
     * @ORM\OneToMany(targetEntity=MovementSection::class, mappedBy="owningOrder", cascade={"persist", "remove"})
     */
    private Collection $path;

    public function __construct(
        General $instigator,
        Collection $path,
    ) {
        $this->startTime = new \DateTime();
        $this->instigator = $instigator;
        $this->path = $path;

        /** @var MovementSection $section */
        foreach ($this->path as $section) {
            $section->setOwningOrder($this);
        }
    }

    #[\Override]
    public function jsonSerialize(): array
    {
        return [
            'destination' => $this->getDestination()->getName(),
            'currentSection' => $this->getCurrentSectionInfo(),
            'movementEndDate' => $this->getTravelEndDateAsString(),
        ];
    }

    public function getInstigator(): General
    {
        return $this->instigator;
    }

    public function getStartTime(): \DateTime
    {
        return $this->startTime;
    }

    public function getOrigin(): WorldMapNode
    {
        /** @var MovementSection $firstSection */
        $firstSection = $this->path[0];

        return $firstSection->getStart();
    }

    public function getDestination(): WorldMapNode
    {
        /** @var MovementSection $lastSection */
        $lastSection = $this->path[count($this->path) - 1];

        return $lastSection->getEnd();
    }

    public function setCurrentMovementSection(int $currentMovementSection): void
    {
        $this->currentMovementSection = $currentMovementSection;
    }

    public function getCurrentMovementSection(): int
    {
        return $this->currentMovementSection;
    }

    public function getPath(): Collection
    {
        return $this->path;
    }

    public function getTravelDuration(): int
    {
        $total = 0;
        /** @var MovementSection $section */
        foreach ($this->path as $section) {
            $total += $section->getTravelDuration();
        }

        return $total;
    }

    public function getTravelEndTime(): int
    {
        return (int) $this->startTime->getTimestamp() + $this->getTravelDuration() * (1. - $this->initialProgression);
    }

    public function getTravelEndDateAsString(): string
    {
        return (new \DateTime())->setTimestamp($this->getTravelEndTime())->format('Y-m-d H:i:s');
    }

    public function getRemainingTravelDuration(): int
    {
        $now = new \DateTime();

        return max(0, $this->getTravelEndTime() - $now->getTimestamp());
    }

    /**
     * Returns info about the current travelled path section.
     *
     * @return array (Section index, origin, destination, section percentage)
     */
    public function getCurrentSectionInfo(): array
    {
        $now = new \DateTime();
        $currentTime = $now->getTimestamp();

        $sectionIndex = 0;
        $percentage = 0.;
        $sectionOrigin = $this->getOrigin();
        $sectionDestination = $this->getDestination();
        $sectionStartTime = $this->startTime->getTimestamp();

        if ($this->initialProgression > 0) {
            assert(1 == count($this->path));
            $sectionOrigin = $this->getOrigin();
            $sectionDestination = $this->getDestination();
            $sectionEndTime = $this->getTravelEndTime();
            $percentage = $this->initialProgression + (1. - $this->initialProgression) * ($currentTime - $sectionStartTime) / ($sectionEndTime - $sectionStartTime);
        } else {
            $counter = count($this->path);
            for ($i = 0; $i < $counter; ++$i) {
                /** @var MovementSection $currentSection */
                $currentSection = $this->path[$i];
                $sectionEndTime = $sectionStartTime + $currentSection->getTravelDuration();
                if ($currentTime >= $sectionStartTime && $currentTime < $sectionEndTime) {
                    $sectionIndex = $i;
                    $sectionOrigin = $currentSection->getStart();
                    $sectionDestination = $currentSection->getEnd();
                    $percentage = ($currentTime - $sectionStartTime) / ($sectionEndTime - $sectionStartTime);
                    break;
                }

                $sectionStartTime += $this->path[$i]->getTravelDuration();
            }
        }

        $percentage = max(0, min(1.0, $percentage));

        return [
            'index' => $sectionIndex,
            'start' => $sectionOrigin->getData()->getName(),
            'end' => $sectionDestination->getData()->getName(),
            'percentage' => $percentage,
        ];
    }

    public function setInitialProgression(float $initialProgression): void
    {
        $this->startTime = new \DateTime();
        $this->initialProgression = $initialProgression;
    }

    #[\Override]
    public function __toString(): string
    {
        return sprintf('MovementOrder of %s moving from %s to %s', $this->getInstigator()->getName(), $this->getOrigin()->getName(), $this->getDestination()->getName());
    }
}
