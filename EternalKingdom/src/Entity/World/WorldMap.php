<?php

namespace App\Entity\World;

use App\Entity\Lord;
use App\Enum\WorldSpeedType;
use App\Service\WorldManager;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * One Game containing some Lords.
 *
 * @ORM\Entity(repositoryClass=App\Repository\World\WorldMapRepository::class)
 */
class WorldMap implements \JsonSerializable
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=Lord::class, mappedBy="worldMap")
     */
    private Collection $lords;

    /**
     * @ORM\OneToMany(targetEntity=WorldMapNode::class, mappedBy="owningWorld", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private Collection $nodes;

    /**
     * Last World Update.
     *
     * @ORM\Column(type="datetime")
     */
    private ?\DateTime $lastUpdateDate;

    /**
     * Resource spots respawn time.
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTime $resourceRespawnTime = null;

    // TODO World Scale

    // TODO Private World!

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private string $language;

    /**
     * @ORM\ManyToOne(targetEntity=WorldData::class, fetch="EAGER")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private WorldData $data;

    /**
     * @ORM\Column(type="integer", options={ "default" : 1 })
     */
    private int $speed;

    /**
     * @ORM\Column(type="integer")
     */
    private int $maxPlayerCount;

    public function __construct(
        string $name,
        string $language,
        WorldData $data,
        int $speed,
        int $maxPlayerCount
    ) {
        $this->name = $name;
        $this->language = $language;
        $this->data = $data;
        $this->speed = $speed;
        $this->maxPlayerCount = $maxPlayerCount;
        $this->createdAt = new \DateTime();
        $this->lastUpdateDate = new \DateTime();
        $this->nodes = new ArrayCollection();
        $this->lords = new ArrayCollection();
    }

    #[\Override]
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * Returns the maximum number of players that can spawn in this world.
     */
    public function getMaxPlayersCount(): int
    {
        return $this->maxPlayerCount;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getLastUpdateDate(): ?DateTime
    {
        return $this->lastUpdateDate;
    }

    public function refreshLastUpdateDate(): void
    {
        $this->lastUpdateDate = new \DateTime();
    }

    /**
     * Return alive Lords in this World.
     *
     * @return Collection|Lord[]
     */
    public function getLords(): Collection
    {
        return $this->lords;
    }

    /**
     * Returns alive Lords count.
     */
    public function getLordsCount(): int
    {
        return count($this->getLords());
    }

    public function addPlayer(Lord $lord): self
    {
        if (!$this->lords->contains($lord)) {
            $this->lords->add($lord);
            $lord->setWorldMap($this);
        }

        return $this;
    }

    public function removePlayer(Lord $lord): self
    {
        // Note: We have to do that on both side else Symfony doesn't persist the removal!
        if ($this->lords->removeElement($lord) && $lord->getWorldMap() === $this) {
            $lord->setWorldMap(null);
        }

        return $this;
    }

    public function hasEmptySlot(): bool
    {
        return $this->getLordsCount() < $this->getMaxPlayersCount();
    }

    /**
     * @return Collection|WorldMapNode[]
     */
    public function getNodes(): Collection
    {
        return $this->nodes;
    }

    public function addNode(WorldMapNode $node): self
    {
        if (!$this->nodes->contains($node)) {
            $this->nodes[] = $node;
            $node->setOwningWorld($this);
        }

        return $this;
    }

    public function removeNode(WorldMapNode $node): self
    {
        // set the owning side to null (unless already changed)
        if ($this->nodes->removeElement($node) && $node->getOwningWorld() === $this) {
            $node->setOwningWorld(null);
        }

        return $this;
    }

    public function getData(): ?WorldData
    {
        return $this->data;
    }

    public function setData(?WorldData $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getSpeed(): int
    {
        return $this->speed;
    }

    public function getSpeedName(): string
    {
        return WorldSpeedType::speedName[$this->speed];
    }

    public function getMapSpeedMultiplier(): float
    {
        return WorldSpeedType::getSpeedMultiplier($this->speed);
    }

    public function getLastResourceRespawnTime(): ?\DateTime
    {
        return $this->resourceRespawnTime;
    }

    public function shouldRespawnResourceSpot(): bool
    {
        if ($this->resourceRespawnTime instanceof \DateTime) {
            $now = new \DateTime();

            return $now->getTimestamp() > $this->resourceRespawnTime->getTimestamp();
        }

        return false;
    }

    /**
     * Schedule the resource spot respawn time.
     */
    public function scheduleResourceRespawnTime(): void
    {
        if (!$this->resourceRespawnTime instanceof \DateTime) {
            $this->resourceRespawnTime = new \DateTime();
            $delayInSeconds = $this::getResourceRespawnDelay();
            $this->resourceRespawnTime->add(\DateInterval::createFromDateString(sprintf('%d seconds', $delayInSeconds)));
        }
    }

    public function clearResourceRespawnTimeSchedule(): void
    {
        $this->resourceRespawnTime = null;
    }

    private function getResourceRespawnDelay(): int
    {
        return WorldManager::RESOURCE_BASE_RESPAWN_DELAY * $this->getMapSpeedMultiplier();
    }
}
