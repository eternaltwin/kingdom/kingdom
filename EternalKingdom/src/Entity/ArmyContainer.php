<?php

namespace App\Entity;

use App\Repository\ArmyContainerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ArmyContainerRepository::class)
 */
class ArmyContainer
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\OneToMany(targetEntity=UnitArmy::class, mappedBy="container", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private Collection $units;

    public function __construct()
    {
        $this->units = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|UnitArmy[]
     */
    public function getUnits(): Collection
    {
        return $this->units;
    }

    public function overrideUnits(Collection|array $units): void
    {
        $this->units->clear();
        /** @var UnitArmy $unitArmy */
        foreach ($units as $unitArmy) {
            $this->addUnit(new UnitArmy($unitArmy->getResource(), $unitArmy->getAmount()));
        }
    }

    public function clearAllUnits(): void
    {
        $this->units->clear();
    }

    public function getUnit(string $unitName): ?UnitArmy
    {
        foreach ($this->units as $unitArmy) {
            if ($unitArmy->getResource()->getName() == $unitName) {
                return $unitArmy;
            }
        }

        return null;
    }

    public function addUnit(UnitArmy $army): self
    {
        if (!$this->units->contains($army)) {
            $this->units[] = $army;
            $army->setContainer($this);
        }

        return $this;
    }

    public function removeUnit(UnitArmy $army): self
    {
        $this->units->removeElement($army);

        return $this;
    }

    public function getUnitCount(string $unitName): int
    {
        $unitArmy = $this->getUnit($unitName);
        if ($unitArmy instanceof UnitArmy) {
            return $unitArmy->getAmount();
        }

        return 0;
    }

    public function getTotalUnitCount(): int
    {
        $total = 0;
        foreach ($this->units as $army) {
            $total += $army->getAmount();
        }

        return $total;
    }

    public function hasAnyUnits(): bool
    {
        return $this->getTotalUnitCount() > 0;
    }

    /**
     * Dump all units that are in this army.
     *
     * @param bool $obfuscated Optionally hide the real amount and replace it by "?"
     */
    public function toArray(bool $obfuscated): array
    {
        $units = [];
        foreach ($this->getUnits() as $army) {
            if (0 == $army->getAmount()) {
                continue;
            }

            if ($obfuscated) {
                // Should we limit this to something else?
                $amount = min(5, strlen((string) $army->getAmount()));
                $amountDisplayed = str_repeat('?', $amount);
            } else {
                $amountDisplayed = $army->getAmount();
            }

            $units[$army->getResource()->getName()] = $amountDisplayed;
        }

        return $units;
    }
}
