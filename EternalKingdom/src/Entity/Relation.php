<?php

namespace App\Entity;

use App\Enum\DiplomacyType;
use App\Repository\RelationRepository;
use Doctrine\ORM\Mapping as ORM;

// TODO On official kingdom, Relation is also used to store info about Suzerain tax!
// https://github.com/motion-twin/WebGamesArchives/blob/main/Muxxu/Kingdom/src/db/Relation.hx

/**
 * Describes the relationship between two Lords.
 *
 * @ORM\Entity(repositoryClass=RelationRepository::class)
 */
class Relation implements \JsonSerializable
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $diplomacyType = DiplomacyType::TYPE_NEUTRAL;

    /**
     * Can the target player cross our territories?
     *
     * @ORM\Column(type="boolean")
     */
    private bool $canCross = false;

    /**
     * Can the target player harvest on our territories?
     *
     * @ORM\Column(type="boolean")
     */
    private bool $canHarvest = false;

    /**
     * @ORM\ManyToOne(targetEntity=Lord::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Lord $lord;
    /**
     * @ORM\ManyToOne(targetEntity=Lord::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Lord $target;

    public function __construct(
        Lord $lord,
        Lord $target,
    ) {
        $this->lord = $lord;
        $this->target = $target;
    }

    #[\Override]
    public function jsonSerialize(): mixed
    {
        return [
            'lordId' => $this->lord->getId(),
            'diplomacyType' => $this->getDiplomacyType(),
            'canCross' => $this->canCross,
            'canHarvest' => $this->canHarvest,
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLord(): Lord
    {
        return $this->lord;
    }

    public function getTarget(): Lord
    {
        return $this->target;
    }

    public function getDiplomacyType(): string
    {
        return $this->diplomacyType;
    }

    public function setDiplomacyType(string $diplomacyType): void
    {
        if (!in_array($diplomacyType, DiplomacyType::getAvailableTypes())) {
            throw new \InvalidArgumentException('Invalid type');
        }

        $this->diplomacyType = $diplomacyType;
    }

    public function canCross(): bool
    {
        return $this->canCross;
    }

    public function setCanCross(bool $canCross): void
    {
        $this->canCross = $canCross;
    }

    public function canHarvest(): bool
    {
        return $this->canHarvest;
    }

    public function setCanHarvest(bool $canHarvest): void
    {
        $this->canHarvest = $canHarvest;
    }
}
