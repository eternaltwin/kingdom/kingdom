<?php

// src/Controller/HelpController.php

namespace App\Controller;

use App\Entity\Battle\Battle;
use App\Entity\Battle\Fight;
use App\Entity\Battle\FightSurvivor;
use App\Entity\General;
use App\Entity\LogSystem\BattleLog;
use App\Repository\Battle\BattleRepository;
use App\Repository\Battle\FightRepository;
use App\Repository\Battle\FightSurvivorRepository;
use App\Repository\GeneralRepository;
use App\Repository\LogSystem\BattleLogRepository;
use App\Service\BattleManager;
use App\Service\WorldManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Entry point for any Battle.
 */
class BattleController extends AbstractController
{
    public function __construct(
        private readonly ManagerRegistry $managerRegistry,
        private readonly BattleManager $battleManager,
        private readonly WorldManager $worldManager,
    ) {
    }

    /**
     * @Route("/battle/{id}")
     */
    public function battleIndex(int $id): Response
    {
        // TODO BattleStats Entity about each Soldier type : "healthy count + injured count + dead count"

        // TODO Config file to query unit info
        // - stats : Max health, attack min & max (will be random)
        // - Attack bonus against units : +X / array of unit name
        // - When attacking bonus : +X
        // - When defending bonus : +X
        // - Cancel all enemy bonus

        /** @var BattleRepository $battleRepo */
        $battleRepo = $this->managerRegistry->getRepository(Battle::class);
        if ($battle = $battleRepo->find($id)) {
            if (false === $battle->isFinished() && $this->battleManager->shouldEndBattle($battle)) {
                $this->battleManager->endBattle($battle);
            }

            $armiesId = [];
            foreach ($battle->getAllEngagedArmies() as $engagedArmy) {
                $armiesId[] = $engagedArmy->getArmyContainer()->getId();
            }

            /** @var GeneralRepository $generalRepo */
            $generalRepo = $this->managerRegistry->getRepository(General::class);
            $fightingGenerals = $generalRepo->findBy(['armyContainer' => $armiesId]);

            /** @var BattleLogRepository $logsRepo */
            $logsRepo = $this->managerRegistry->getRepository(BattleLog::class);
            $logs = $logsRepo->findBy(['battle' => $battle], ['timestamp' => 'DESC']);

            /** @var FightRepository $fightRepo */
            $fightRepo = $this->managerRegistry->getRepository(Fight::class);
            $fights = $fightRepo->findBy(['battle' => $battle]);

            /** @var FightSurvivorRepository $fightSurvivorRepo */
            $fightSurvivorRepo = $this->managerRegistry->getRepository(FightSurvivor::class);
            $fighters = $fightSurvivorRepo->findFightSurvivors($battle);
            
            $battleData = [
                'fights' => $fights,
                'fighters' => $fighters,
            ];

            $estimatedBattleEndDate = $this->battleManager->getEstimatedBattleEndDate($battle)->format('Y-m-d H:i:s');
            
            return $this->render(
                'Battle/battle.html.twig',
                [
                    'battle' => $battle,
                    'battleData' => json_encode($battleData),
                    'generals' => $fightingGenerals,
                    'logs' => $logs,
                    'estimatedBattleEndDate' => $estimatedBattleEndDate,
                ]
            );
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/battle/{id}/refreshAll")
     */
    public function refreshBattle(int $id): Response
    {
        $battleRepo = $this->managerRegistry->getRepository(Battle::class);
        if ($battle = $battleRepo->find($id)) {
            $this->worldManager->tickWorld($battle->getWorldMap());
        }

        return $this->redirectToRoute('app_battle_battleindex', ['id' => $id]);
    }
}
