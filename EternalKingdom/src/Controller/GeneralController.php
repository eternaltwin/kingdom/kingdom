<?php

namespace App\Controller;

use App\Entity\ArmyContainer;
use App\Entity\General;
use App\Entity\Relation;
use App\Entity\User;
use App\Entity\World\ResourceSpot;
use App\Kingdom\CheatSystem\CheatConfiguration;
use App\Repository\World\ResourceSpotRepository;
use App\Service\BattleManager;
use App\Service\CityManager;
use App\Service\GameDataProvider;
use App\Service\MovementManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Permits to the player to interact with their owned Generals.
 */
class GeneralController extends AbstractController
{
    private const int K_MAX_HARVESTABLE_AMOUNT = 50;

    public function __construct(
        private readonly ManagerRegistry $managerRegistry,
        private readonly CityManager $cityManager,
        private readonly MovementManager $movementManager,
        private readonly BattleManager $battleManager,
        private readonly GameDataProvider $gameDataProvider,
        private readonly TranslatorInterface $translator
    ) {
    }

    /**
     * Displays the Units Transfer page.
     *
     * @Route("/general/{generalId}/units")
     */
    public function unitsTransfer(int $generalId): Response
    {
        $response = new Response();

        $general = $this->validateGeneral($generalId);
        if (null !== $general) {
            $response = $this->render(
                'General/transferUnits.html.twig',
                ['general' => $general]
            );
            $response->setStatusCode(Response::HTTP_OK);
        }

        if (Response::HTTP_OK !== $response->getStatusCode()) {
            $response->setStatusCode(Response::HTTP_OK);
            $response = $this->forward("App\Controller\DefaultController::Index");
        }

        return $response;
    }

    /**
     * Confirm the Units Transfer (The request contains an array of the new amount of each units inside the WorldMapNode).
     *
     * @Route("/general/{generalId}/units/confirm", methods={"POST"})
     */
    public function confirmUnitsTransfer(Request $request, int $generalId): Response
    {
        $response = new Response();
        $response->setStatusCode(Response::HTTP_FORBIDDEN);

        $general = $this->validateGeneral($generalId);
        if (null !== $general) {
            $failReason = '';
            if ($this->canTransferUnits($general, $failReason)) {
                $armyContainer = $this->getRelevantArmyContainer($general);

                $payload = $request->request->all();
                foreach ($payload as $unitName => $amount) {
                    $territoryArmyUnit = $armyContainer->getUnit($unitName);
                    if ($territoryArmyUnit) {
                        $delta = $territoryArmyUnit->getAmount() - $amount;
                    } else {
                        $delta = -$amount;
                    }

                    if (0 != $delta) {
                        $generalArmyUnit = $general->getArmyContainer()->getUnit($unitName);
                        if ($territoryArmyUnit) {
                            $territoryArmyUnit->setAmount($territoryArmyUnit->getAmount() - $delta);
                        } else {
                            $armyContainer->addUnit($this->gameDataProvider->createArmyUnit($unitName, -$delta));
                        }

                        if ($generalArmyUnit) {
                            $generalArmyUnit->setAmount($generalArmyUnit->getAmount() + $delta);
                        } else {
                            $general->getArmyContainer()->addUnit($this->gameDataProvider->createArmyUnit($unitName, $delta));
                        }
                    }
                }

                $this->managerRegistry->getManager()->persist($armyContainer);
                $this->managerRegistry->getManager()->persist($general);
                $this->managerRegistry->getManager()->flush();

                $response = $this->redirectToRoute(
                    'app_map_focusGeneral',
                    ['worldId' => $general->getOwner()->getWorldMap()->getId(), 'generalId' => $general->getId()]
                );
                $response->setStatusCode(Response::HTTP_OK);
            } else {
                if (!empty($failReason)) {
                    $this->addFlash('error', $failReason);
                }
                $response = $this->redirectToRoute(
                    'app_map_focusGeneral',
                    [
                        'worldId' => $general->getOwner()->getWorldMap()->getId(),
                        'generalId' => $general->getId(),
                    ]
                );
            }
        }

        return $response;
    }

    /**
     * Returns true if the specified general is allowed to transfer units on the current location.
     */
    private function canTransferUnits(General $general, string &$failReason): bool
    {
        if ($this->battleManager->isLocationInWar($general->getLocation())) {
            $failReason = $this->translator->trans('map.actions.transferUnits.locationInWar', [], 'Notification');

            return false;
        }

        return null != $this->getRelevantArmyContainer($general);
    }

    /**
     * Return the army container of the current general location if relevant.
     */
    private function getRelevantArmyContainer(General $general): ArmyContainer
    {
        $node = $general->getLocation();

        // It's our city : we got access to our Defense
        $armyContainer = $general->getOwner() === $node->getOwner() ? $general->getOwner()->getCity()->getDefense() : null;

        // It's our territory or our vassal city : We got access to our Garrison
        if (null === $armyContainer) {
            $armyContainer = $general->getOwner() === $node->getSovereign() ? $node->getArmyContainer() : null;
        }

        return $armyContainer;
    }

    /**
     * Toggle the fortification state of a General.
     *
     * @Route("/general/toggleFortification", methods={"POST"})
     */
    public function toggleFortification(Request $request): Response
    {
        $response = new JsonResponse();
        $response->setStatusCode(Response::HTTP_FORBIDDEN);

        $payload = json_decode($request->request->get('json', []), true);
        $generalId = $payload['general'];

        $general = $this->validateGeneral($generalId);
        if (null !== $general) {
            $failReason = '';
            if ($this->canToggleFortification($general, $failReason)) {
                $general->setFortifying(!$general->isFortifying());

                $message = $general->isFortifying() ?
                    $this->translator->trans('map.actions.fortification.enabled', ['%name%' => $general->getName()], 'Notification') :
                    $this->translator->trans('map.actions.fortification.disabled', ['%name%' => $general->getName()], 'Notification');
                $responseData = [
                    'success' => true,
                    'message' => $message,
                ];

                $this->managerRegistry->getManager()->flush();
            }

            if (empty($responseData)) {
                $responseData = $this->generateDefaultResponseArray($failReason);
            }
            $response->setContent(json_encode($responseData));
            $response->setStatusCode(Response::HTTP_OK);
        }

        return $response;
    }

    private function canToggleFortification(General $general, string &$failReason): bool
    {
        // TODO Handle fortification on Allies
        $currentLocation = $general->getLocation();

        if (null !== $general->getMovementOrder() || null != $general->getBattle()) {
            $failReason = 'General is occupied (moving, in battle, ...)';

            return false;
        }

        return $general->getOwner() === $currentLocation->getSovereign();
    }

    /**
     * Move the specified general to the specified location if possible.
     *
     * @Route("/general/move/", methods={"POST"})
     */
    public function move(Request $request): JsonResponse
    {
        $response = new JsonResponse();
        $response->setStatusCode(Response::HTTP_FORBIDDEN);

        $payload = json_decode($request->request->get('json', []), true);
        $generalId = $payload['general'];
        $path = $payload['path'];

        $general = $this->validateGeneral($generalId);
        if (null !== $general) {
            $responseData = [];
            $failReason = '';
            if ($this->canMoveGeneral($general, $failReason)) {
                $this->movementManager->startMovement($general, $path);
                if (null !== $general->getMovementOrder()) {
                    $responseData = $this->generateMovementResponse($general);
                    $responseData['message'] = $this->translator->trans(
                        'map.actions.move.started',
                        [
                            '%location%' => $general->getMovementOrder()->getDestination()->getName(),
                        ],
                        'Notification'
                    );
                } else {
                    $failReason = 'Failed to start movement (Server logic)';
                }
            }

            if (empty($responseData)) {
                $responseData = $this->generateDefaultResponseArray($failReason);
            }

            assert(count($responseData) > 0);
            $response->setContent(json_encode($responseData));
            $response->setStatusCode(Response::HTTP_OK);
        }

        return $response;
    }

    private function canMoveGeneral(General $general, string &$failReason): bool
    {
        if (null != $general->getMovementOrder() || null != $general->getBattle() || $general->isFortifying()) {
            $failReason = 'General is occupied (moving, fortifying, within Battle ...)';

            return false;
        } elseif (!($general->hasAnArmy() || CheatConfiguration::CHEAT_ALLOW_MOVE_WITHOUT_ARMY)) {
            $failReason = $this->translator->trans('map.actions.move.noUnits', domain: 'Notification');

            return false;
        }

        return true;
    }

    /**
     * Ask the current general to turn back.
     *
     * @Route("/general/turnBack/", methods={"POST"})
     */
    public function turnBack(Request $request): JsonResponse
    {
        $response = new JsonResponse();
        $response->setStatusCode(Response::HTTP_FORBIDDEN);

        $payload = json_decode($request->request->get('json', []), true);
        $generalId = $payload['general'];

        $general = $this->validateGeneral($generalId);
        if (null !== $general && null !== $general->getMovementOrder()) {
            $this->movementManager->revertMovement($general);

            $responseData = $this->generateMovementResponse($general);
            $responseData['message'] = $this->translator->trans(
                'map.actions.move.turnBack',
                [
                    '%location%' => $general->getMovementOrder()->getDestination()->getName(),
                ],
                'Notification'
            );

            $response->setContent(json_encode($responseData));
            $response->setStatusCode(Response::HTTP_OK);
        }

        return $response;
    }

    /**
     * Request the specified general to attack its current location if possible.
     *
     * @Route("/general/attack/", methods={"POST"})
     *
     * @throws \Exception
     */
    public function attack(Request $request): JsonResponse
    {
        $response = new JsonResponse();
        $response->setStatusCode(Response::HTTP_FORBIDDEN);

        $payload = json_decode($request->request->get('json', []), true);
        $generalId = $payload['general'];

        $general = $this->validateGeneral($generalId);
        if (null !== $general) {
            $failReason = '';
            if ($this->canAttack($general, $failReason)) {
                // dump("General that attack : " . $general->getName());

                $location = $general->getLocation();
                // dump("Location to attack : " . $location->getName());

                $shouldTriggerBattle = $this->battleManager->shouldTriggerBattle($location);
                if ($shouldTriggerBattle) {
                    $this->battleManager->startBattle($general, $failReason);
                } else {
                    $this->battleManager->claimTerritory($general, wasDefendedWhenConquered: false);
                }
            }

            $responseData = $this->generateDefaultResponseArray($failReason);
            $response->setContent(json_encode($responseData));
            $response->setStatusCode(Response::HTTP_OK);
        }

        return $response;
    }

    private function canAttack(General $general, string &$failReason): bool
    {
        if (!$general->getArmyContainer()->hasAnyUnits()) {
            $failReason = $this->translator->trans('map.actions.attack.noUnits', [], 'Notification');

            return false;
        } elseif ($general->getLocation()->getSovereign() === $general->getOwner()) {
            $failReason = 'Current Lord already own this territory';

            return false;
        } elseif (false === $this->battleManager->canClaimTerritory($general, $failReason)) {
            return false;
        }

        return true;
    }

    // TODO joinPendingBattle (General + identify if it should Defend or Attack)

    /**
     * Request the specified general to harvest its current location if possible.
     *
     * @Route("/general/harvest/", methods={"POST"})
     */
    public function harvest(Request $request): JsonResponse
    {
        $response = new JsonResponse();
        $response->setStatusCode(Response::HTTP_FORBIDDEN);

        $payload = json_decode($request->request->get('json', []), true);
        $generalId = $payload['general'];

        $general = $this->validateGeneral($generalId);
        if (null !== $general) {
            $location = $general->getLocation();

            /** @var ResourceSpotRepository $nodeRepo */
            $nodeRepo = $this->managerRegistry->getManager()->GetRepository(ResourceSpot::class);
            $resourceSpot = $nodeRepo->findOneBy(['node' => $location->getData()->getId()]);

            $failReason = '';
            if ($this->canHarvest($general, $resourceSpot, $failReason)) {
                $city = $general->getOwner()->getCity();
                $resourceName = $resourceSpot->getResource()->getName();
                $maxStorages = $this->cityManager->calculateAndGetMaxStorages($city);
                $maxHarvestableAmount = $maxStorages[$resourceName];
                $playerStorage = $this->cityManager->findOrCreateStorage($resourceName, $city);

                $remainingSpaceInStorage = max($maxHarvestableAmount - $playerStorage->getAmount(), 0);
                // TODO Limit harvestable amount for player/general ?! (cooldown per lord / per general / per resource?)
                $collectedAmount = min(self::K_MAX_HARVESTABLE_AMOUNT, $remainingSpaceInStorage, $resourceSpot->getAmount());

                if ($collectedAmount > 0) {
                    $playerStorage->setAmount($playerStorage->getAmount() + $collectedAmount);
                    $resourceSpot->setAmount($resourceSpot->getAmount() - $collectedAmount);
                    $this->cityManager->logNodeHarvest($city, $general, $resourceSpot, $collectedAmount);

                    if (0 == $resourceSpot->getAmount()) {
                        $this->managerRegistry->getManager()->remove($resourceSpot);
                        $general->getOwner()->getWorldMap()->scheduleResourceRespawnTime();
                    }

                    $this->managerRegistry->getManager()->flush();

                    // TODO Limit harvestable amount for player/general ?! (cooldown per lord / per general / per resource?)
                    $responseData = [
                        'success' => true,
                        'node' => $resourceSpot->getNode()->getName(),
                        'newAmount' => $resourceSpot->getAmount(),
                        'message' => $this->translator->trans('map.actions.harvest.success', [
                            '%resourceName%' => $this->translator->trans('resources.'.$resourceName.'.name', [], 'City'),
                            '%amount%' => $collectedAmount,
                        ], 'Notification'),
                    ];
                } else {
                    $failReason = $this->translator->trans('map.actions.harvest.storageFull', [
                        '%resourceName%' => $this->translator->trans('resources.'.$resourceName.'.name', [], 'City'),
                        '%amount%' => $maxHarvestableAmount,
                    ], 'Notification');
                }
            }

            if (empty($responseData)) {
                $responseData = $this->generateDefaultResponseArray($failReason);
            }

            $response->setStatusCode(Response::HTTP_OK);
            $response->setContent(json_encode($responseData));
        }

        return $response;
    }

    /**
     * Verify if the current general can harvest the specified resource spot.
     * TODO Investigate the original source (General.hx)
     * It seems there is an extra rule : See canRecolt  (units.bid != null).
     */
    public function canHarvest(General $general, ?ResourceSpot $resourceSpot, string &$reason): bool
    {
        if (null === $resourceSpot || 0 === $resourceSpot->getAmount()) {
            $reason = "Resource spot doesn't exist or is empty";

            return false;
        }

        if (null !== $general->getMovementOrder()) {
            $reason = 'General is moving';

            return false;
        }

        $spotSovereign = $general->getLocation()->getSovereign();
        $canHarvest = $spotSovereign === $general->getOwner();

        if (!$canHarvest) {
            $relationRepo = $this->managerRegistry->getRepository(Relation::class);
            $relation = $relationRepo->findOneBy(['target' => $general->getOwner()]);
            if (null !== $relation) {
                $canHarvest = $relation->canHarvest();
            }
        }

        if (!$canHarvest) {
            $reason = $this->translator->trans('map.actions.harvest.noRights', [], 'Notification');
        }

        return $canHarvest;
    }

    /**
     * Verify if the General with the specified id is owned by the current user.
     * Also verify if the current Lord is alive!
     * Returns the General entity if everything is correct.
     */
    private function validateGeneral(int $generalId): ?General
    {
        /** @var User $user */
        $user = $this->getUser();
        if (null === $user) {
            return null;
        }

        $lord = $user->getLord();
        if (null === $lord || !$lord->isAlive()) {
            return null;
        }

        foreach ($lord->getGenerals() as $general) {
            if ($general->getId() === $generalId) {
                return $general;
            }
        }

        return null;
    }

    private function generateDefaultResponseArray(string $failReason): array
    {
        if (empty($failReason)) {
            $responseData = [
                'success' => true,
            ];
        } else {
            $responseData = [
                'success' => false,
                'message' => $failReason,
            ];
        }

        return $responseData;
    }

    private function generateMovementResponse(General $general): array
    {
        return [
            'success' => true,
            'generalID' => $general->getId(),
            'movementOrder' => $general->getMovementOrder(),
        ];
    }
}
