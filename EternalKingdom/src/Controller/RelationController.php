<?php

namespace App\Controller;

use App\Entity\Lord;
use App\Entity\Relation;
use App\Entity\User;
use App\Enum\DiplomacyType;
use App\Enum\LogEventType;
use App\Repository\LordRepository;
use App\Repository\RelationRepository;
use App\Service\GameLogManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Handle relation between Lord (Friend, Enemies, Rights, Vassalization, ...).
 */
class RelationController extends AbstractController
{
    public function __construct(
        private readonly ManagerRegistry $managerRegistry,
        private readonly GameLogManager $gameLogManager
    ) {
    }

    /**
     * @Route("relation/diplomacy/{lordId}/{diplomacyType}")
     *
     * @param string $diplomacyType (See: DiplomacyType)
     */
    public function changeDiplomacy(int $lordId, string $diplomacyType): Response
    {
        $relation = $this->findRelationBetweenCurrentLordAndTarget($lordId, createIfNotFound: true);
        if (!$relation instanceof Relation) {
            return $this->redirectToRoute('homepage');
        } else {
            $response = $this->redirectToTargetedUser($relation);

            if ($diplomacyType !== $relation->getDiplomacyType()) {
                $relation->setDiplomacyType($diplomacyType);
                $this->managerRegistry->getManager()->persist($relation);

                $this->managerRegistry->getManager()->flush();

                $logEventType = null;
                switch ($diplomacyType) {
                    case DiplomacyType::TYPE_FRIEND:
                        $logEventType = LogEventType::MANAGEMENT_DIPLOMACY_FRIEND;
                        break;
                    case DiplomacyType::TYPE_ENEMY:
                        $logEventType = LogEventType::MANAGEMENT_DIPLOMACY_ENEMY;
                        break;
                    case DiplomacyType::TYPE_NEUTRAL:
                        $logEventType = LogEventType::MANAGEMENT_DIPLOMACY_NEUTRAL;
                        break;
                }

                $this->logRelationEvent($logEventType, $relation);
            }
        }

        return $response;
    }

    /**
     * @Route("relation/crossRight/{lordId}/{isAllowed}")
     */
    public function changeCrossRight(int $lordId, bool $isAllowed): Response
    {
        $relation = $this->findRelationBetweenCurrentLordAndTarget($lordId, createIfNotFound: true);
        if (!$relation instanceof Relation) {
            return $this->redirectToRoute('homepage');
        } else {
            $response = $this->redirectToTargetedUser($relation);

            if ($relation->canCross() !== $isAllowed) {
                $relation->setCanCross($isAllowed);
                $this->managerRegistry->getManager()->persist($relation);
                $this->managerRegistry->getManager()->flush();

                $this->logRelationEvent(
                    $isAllowed ? LogEventType::MANAGEMENT_CROSS_RIGHT_ADDED : LogEventType::MANAGEMENT_CROSS_RIGHT_REMOVED,
                    $relation
                );
            }
        }

        return $response;
    }

    /**
     * @Route("relation/harvestRight/{lordId}/{isAllowed}")
     */
    public function changeHarvestRight(int $lordId, bool $isAllowed): Response
    {
        $relation = $this->findRelationBetweenCurrentLordAndTarget($lordId, createIfNotFound: true);
        if (!$relation instanceof Relation) {
            return $this->redirectToRoute('homepage');
        } else {
            $response = $this->redirectToTargetedUser($relation);

            if ($relation->canHarvest() !== $isAllowed) {
                $relation->setCanHarvest($isAllowed);
                $this->managerRegistry->getManager()->persist($relation);
                $this->managerRegistry->getManager()->flush();

                $this->logRelationEvent(
                    $isAllowed ? LogEventType::MANAGEMENT_HARVEST_RIGHT_ADDED : LogEventType::MANAGEMENT_HARVEST_RIGHT_REMOVED,
                    $relation
                );
            }
        }

        return $response;
    }

    /**
     * Find or create the relation between the current Lord and the specified Lord.
     */
    private function findRelationBetweenCurrentLordAndTarget(int $targetLordId, bool $createIfNotFound = false): ?Relation
    {
        /** @var LordRepository $lordRepository */
        $lordRepository = $this->managerRegistry->getRepository(Lord::class);
        $targetLord = $lordRepository->find($targetLordId);

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        return $this->findRelation($currentUser->getLord(), $targetLord, $createIfNotFound);
    }

    /**
     * Find or create the relation between the current Lord and the specified Lord.
     */
    private function findRelation(Lord $lord, Lord $target, bool $createIfNotFound = false): ?Relation
    {
        /** @var RelationRepository $relationRepository */
        $relationRepository = $this->managerRegistry->getRepository(Relation::class);
        $relation = $relationRepository->findOneBy(['lord' => $lord, 'target' => $target]);

        if (null === $relation && $createIfNotFound) {
            $relation = new Relation($lord, $target);
        }

        return $relation;
    }

    private function redirectToTargetedUser(Relation $relation): Response
    {
        return $this->redirectToRoute('user', ['id' => $relation->getTarget()->getUser()->getId()]);
    }

    private function logRelationEvent(string $logEventType, Relation $relation): void
    {
        $instigator = $relation->getLord();
        $this->gameLogManager->addKingdomLog(
            $relation->getTarget(),
            $logEventType,
            true,
            [
                '%title%' => $instigator->getTitle(),
                '%userId%' => $instigator->getUser()->getId(),
                '%lord%' => $instigator->getName(),
            ]
        );
    }
}
