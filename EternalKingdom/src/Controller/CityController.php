<?php

// src/Controller/CityController.php

namespace App\Controller;

use App\Entity\LogSystem\CityLog;
use App\Entity\Lord;
use App\Entity\User;
use App\Model\SwapCandidate;
use App\Model\Tile;
use App\Repository\LogSystem\CityLogRepository;
use App\Service\CityManager;
use App\Service\GridManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * CityController will display the City page
 * It permits to access the Match3 to increase resource stocks & population
 * and permits to make new buildings and improve the army.
 */
class CityController extends AbstractController
{
    /**
     * CityController constructor.
     */
    public function __construct(
        private readonly ManagerRegistry $managerRegistry,
        private readonly GridManager $gridManager,
        private readonly CityManager $cityManager,
    ) {
    }

    /**
     * City index page.
     *
     * @Route("/city")
     */
    public function city(): Response
    {
        return $this->GenerateCommonCityPageResponse('City/index.html.twig');
    }

    /**
     * Page that displays the list of constructions schemas.
     *
     * @Route("/city/chooseConstruction")
     */
    public function chooseConstruction(): Response
    {
        $lord = $this->getCurrentLord();
        if (!$lord instanceof Lord) {
            return $this->redirectToRoute('homepage');
        }

        $allParameters = [
            'constructionProjects' => $this->cityManager->getDisplayedProjects($lord->getCity()),
        ];

        return $this->GenerateCommonCityPageResponse('City/optionChooseBuilding.html.twig', $allParameters);
    }

    /**
     * Request needs the constructionName !
     *
     * @Route("/city/startConstruction/{name}_{level}")
     */
    public function startConstruction(string $name, int $level): Response
    {
        $lord = $this->getCurrentLord();
        if (!$lord instanceof Lord) {
            return $this->redirectToRoute('homepage');
        }

        if ($this->cityManager->startConstruction($lord->getCity(), $name, $level)) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('app_city_city');
        }

        if ($lord->getCity()->getStorageCount('worker') > 0) {
            return $this->redirectToRoute('app_city_chooseconstruction');
        } else {
            return $this->redirectToRoute('app_city_city');
        }
    }

    /**
     * @Route("/city/cancelConstruction")
     */
    public function cancelConstruction(): Response
    {
        $lord = $this->getCurrentLord();
        if (!$lord instanceof Lord) {
            return $this->redirectToRoute('homepage');
        }

        if ($this->cityManager->cancelConstruction($lord->getCity())) {
            $this->managerRegistry->getManager()->flush();
        }

        return $this->redirectToRoute('app_city_city');
    }

    /**
     * @Route("/city/chooseSoldierUpgrade")
     */
    public function chooseSoldierUpgrade(): Response
    {
        $lord = $this->getCurrentLord();
        if (!$lord instanceof Lord) {
            return $this->redirectToRoute('homepage');
        }

        $allParameters = [
            'upgradeSchemas' => $this->cityManager->getDisplayedConversionSchemas($lord->getCity(), 'army'),
        ];

        return $this->GenerateCommonCityPageResponse('City/optionUpgradeSoldier.html.twig', $allParameters);
    }

    /**
     * Request needs the producedResourceName !
     *
     * @Route("/city/upgradeSoldier/{resourceName}", methods={"POST"}))
     */
    public function upgradeSoldier(Request $request, string $resourceName): Response
    {
        $lord = $this->getCurrentLord();
        if (!$lord instanceof Lord) {
            return $this->redirectToRoute('homepage');
        }

        $amount = $request->request->getInt('count', 0);
        if ($this->cityManager->convertResources($lord->getCity(), $resourceName, $amount)) {
            $this->managerRegistry->getManager()->flush();
        }

        return $this->redirectToRoute('app_city_choosesoldierupgrade');
    }

    /**
     * Request needs the producedResourceName !
     *
     * @Route("/city/toggleRecruitment"))
     */
    public function toggleRecruitment(): Response
    {
        $lord = $this->getCurrentLord();
        if (!$lord instanceof Lord) {
            return $this->redirectToRoute('homepage');
        }

        $this->cityManager->toggleRecruitment($lord->getCity());
        $this->managerRegistry->getManager()->flush();

        return $this->redirectToRoute('app_city_choosesoldierupgrade');
    }

    /**
     * @Route("/city/chooseResourceConversion")
     */
    public function chooseResourceConversion(): Response
    {
        $lord = $this->getCurrentLord();
        if (!$lord instanceof Lord) {
            return $this->redirectToRoute('homepage');
        }

        $allParameters = [
            'conversionSchemas' => $this->cityManager->getDisplayedConversionSchemas($lord->getCity(), 'resource'),
        ];

        return $this->GenerateCommonCityPageResponse('City/optionConvertResources.html.twig', $allParameters);
    }

    /**
     * Request needs the producedResourceName!
     *
     * @Route("/city/convertResources/{resourceName}", methods={"POST"}))
     */
    public function convertResources(Request $request, string $resourceName): Response
    {
        $lord = $this->getCurrentLord();
        if (!$lord instanceof Lord) {
            return $this->redirectToRoute('homepage');
        }

        $amount = $request->request->getInt('count', 0);

        if ($this->cityManager->convertResources($lord->getCity(), $resourceName, $amount)) {
            $this->managerRegistry->getManager()->flush();
        }

        return $this->redirectToRoute('app_city_chooseresourceconversion');
    }

    /**
     * @Route("/city/chooseGeneralName")
     */
    public function chooseGeneralName(): Response
    {
        return $this->GenerateCommonCityPageResponse('City/optionRecruitGeneral.html.twig');
    }

    /**
     * Request needs the producedResourceName !
     *
     * @Route("/city/recruitGeneral", methods={"POST"}))
     */
    public function recruitGeneral(Request $request): Response
    {
        $lord = $this->getCurrentLord();
        if (!$lord instanceof Lord) {
            return $this->redirectToRoute('homepage');
        }

        $name = $request->request->getAlpha('name', '');
        if (strlen($name) > 0 && $this->cityManager->recruitGeneral($lord->getCity(), $name)) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('app_map_map', ['worldId' => $lord->getWorldMap()->getId()]);
        }

        return $this->redirectToRoute('app_city_city');
    }

    /**
     * Converts a pending citizen into the specified one
     * Request got the citizen job within a payload
     * It replies with the citizen job that has been created.
     *
     * @Route("/city/convertCitizen", methods={"POST"})
     */
    public function ConvertCitizen(Request $request): JsonResponse
    {
        $response = new JsonResponse();
        $response->setStatusCode(Response::HTTP_FORBIDDEN);

        $lord = $this->getCurrentLord();
        if ($lord instanceof Lord) {
            $payload = json_decode($request->request->get('json', []), true);
            $citizenId = $payload['citizenId'];

            if ($this->cityManager->convertCitizen($lord->getCity(), $citizenId)) {
                $this->managerRegistry->getManager()->flush();

                $array = [
                    'citizenType' => $citizenId,
                ];
                $response->setContent(json_encode($array));
                $response->setStatusCode(Response::HTTP_OK);
            }
        }

        return $response;
    }

    /**
     * Request needs the swap coordinates from client JSON paylood !
     *
     * @Route("/city/swapTiles", methods={"POST"})
     *
     * @throws \Exception
     */
    public function swapTiles(Request $request): JsonResponse
    {
        $response = new JsonResponse();
        $response->setStatusCode(Response::HTTP_FORBIDDEN);

        $lord = $this->getCurrentLord();
        if ($lord instanceof Lord && $lord->isAlive()) {
            $payload = json_decode($request->request->get('json', []), true);

            $tile1 = new Tile($payload['r1'], $payload['c1']);
            $tile2 = new Tile($payload['r2'], $payload['c2']);
            $swapCandidate = new SwapCandidate($tile1, $tile2);

            if ($this->gridManager->swap($lord->getCityRef(), $swapCandidate)) {
                // if the grid has been reset, $steps contains also a string of the new grid
                $steps = $this->gridManager->getChainReactionSteps();

                $this->cityManager->convertAlignments($lord->getCityRef(), $steps);
                $this->managerRegistry->getManager()->flush();

                $array = [
                    'steps' => $steps,
                    'resources' => $this->generateCityInfoAfterSwap($lord),
                ];
                $response->setContent(json_encode($array));
                $response->setStatusCode(Response::HTTP_OK);
            }
        }

        return $response;
    }

    /**
     * Request needs the swap coordinates from client JSON paylood !
     *
     * @Route("/city/fetchLastSwapLogs", methods={"GET"})
     */
    public function fetchLastSwapLogs(): Response
    {
        $lord = $this->getCurrentLord();
        if (!$lord instanceof Lord) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        /** @var CityLogRepository $logsRepo */
        $logsRepo = $this->managerRegistry->getRepository(CityLog::class);
        $mostRecentGameLog = $logsRepo->findMostRecentDate($lord);

        $logs = $logsRepo->findRecentGameLogs($lord, $mostRecentGameLog->getTimestamp());

        return new Response($this->render('City/Components/CityLogsList.html.twig', ['logs' => $logs])->getContent());
    }

    /**
     * Generates the city info after the swap triggered the full chain reaction.
     */
    private function generateCityInfoAfterSwap(Lord $lord): array
    {
        $resources = [];
        foreach ($lord->getCity()->getStorages() as $item) {
            $resourceInfo = [
                'name' => $item->getResource()->getName(),
                'amount' => $item->getAmount(),
            ];
            $resources[] = $resourceInfo;
        }

        $resourceInfo = [
            'name' => 'turn',
            'amount' => $lord->getCity()->getRemainingTurns(),
        ];
        $resources[] = $resourceInfo;

        $resourceInfo = [
            'name' => 'soldier',
            'amount' => $lord->getCity()->getDefense()->getUnitCount('soldier'),
        ];
        $resources[] = $resourceInfo;

        return $resources;
    }

    /**
     * Generate a common city page response.
     *
     * @param string $view            Template to display
     * @param array  $extraParameters Extra parameters to load with the template
     */
    private function GenerateCommonCityPageResponse(string $view, array $extraParameters = []): Response
    {
        $lord = $this->getCurrentLord();
        if (!$lord instanceof Lord) {
            return $this->redirectToRoute('homepage');
        }

        // TODO Only send a short amount of logs! Limit result!
        $gameLogRepository = $this->managerRegistry->GetRepository(CityLog::class);
        $cityLogs = $gameLogRepository->findBy(['lord' => $lord], ['timestamp' => 'DESC']);

        $this->initCityGrid($lord);
        $commonParameters = [
            'lord' => $lord,
            'maxAmounts' => $this->cityManager->calculateAndGetMaxStorages($lord->getCity()),
            'logs' => $cityLogs,
        ];
        $allParameters = array_merge($commonParameters, $extraParameters);

        return $this->render($view, $allParameters);
    }

    /**
     * Returns the current active Lord.
     */
    private function getCurrentLord(): ?Lord
    {
        // TODO for debug purpose, we could impersonate another lord here, but preferably we should use the Switch user feature of Symfony?
        /*
        $lordRepository = $this->managerRegistry->GetRepository(Lord::class);
        $lord = $lordRepository->find($id);
        */

        $user = $this->getUser();
        if (!$user instanceof User) {
            throw new \LogicException();
        }

        $lord = $user->getLord();
        if (!$lord || !$lord->isAlive()) {
            return null;
        }

        return $lord;
    }

    /**
     * Initializes a new city's grid & persist it.
     */
    private function initCityGrid(Lord $lord): void
    {
        $city = $lord->getCity();

        if (null === $city->getGrid() || '' === $city->getGrid() || '0' === $city->getGrid()) {
            $grid = $this->gridManager->initGrid($city);
            $lord->getCity()->setGrid(GridManager::convertGridToString($grid));
            $this->managerRegistry->getManager()->flush();
        }
    }
}
