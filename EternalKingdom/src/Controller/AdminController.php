<?php

namespace App\Controller;

use App\Entity\Battle\Battle;
use App\Entity\General;
use App\Entity\User;
use App\Entity\World\ResourceSpot;
use App\Entity\World\WorldMap;
use App\Entity\World\WorldMapNode;
use App\Repository\Battle\BattleRepository;
use App\Repository\GeneralRepository;
use App\Repository\UserRepository;
use App\Repository\World\ResourceSpotRepository;
use App\Repository\World\WorldMapNodeRepository;
use App\Repository\World\WorldMapRepository;
use App\Service\GameDataLoaderService;
use App\Service\WorldManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    private readonly GameDataLoaderService $gameDataLoaderService;

    // TODO Stats (Number of players, number of players in game)
    // Also about players : numbers of inactives (last connection since [<1d][1d - <=1w][>1w - <=1m][>1m])
    // In game stats : number of each titles total / per map
    // Average players per map

    public function __construct(
        private readonly ManagerRegistry $managerRegistry,
        private readonly WorldManager $worldManager
    ) {
        $this->gameDataLoaderService = new GameDataLoaderService();
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function adminIndex(): Response
    {
        $params = [];

        return $this->render('Admin/adminIndex.html.twig', $params);
    }

    /**
     * @Route("/admin/website", name="adminWebsite")
     */
    public function adminManageWebsite(): Response
    {
        return $this->render('Admin/adminWebsite.html.twig', []);
    }

    /**
     * @Route("/admin/world", name="adminWorlds")
     */
    public function adminWorldIndex(): Response
    {
        /** @var WorldMapRepository $repo */
        $repo = $this->managerRegistry->getRepository(WorldMap::class);
        $worlds = $repo->findBy([], ['language' => 'ASC', 'createdAt' => 'ASC']);

        return $this->render('Admin/adminWorld.html.twig', ['worlds' => $worlds]);
    }

    /**
     * @Route("/admin/worldNodes/index/{worldId}", name="adminWorldNodes")
     */
    public function adminWorldDetails(int $worldId): Response
    {
        /** @var WorldMapRepository $worldRepo */
        $worldRepo = $this->managerRegistry->getRepository(WorldMap::class);
        $world = $worldRepo->findOneBy(['id' => $worldId]);

        /** @var WorldMapNodeRepository $nodesRepo */
        $nodesRepo = $this->managerRegistry->getRepository(WorldMapNode::class);
        $nodes = $nodesRepo->findBy(['owningWorld' => $worldId], ['id' => 'ASC']);

        /** @var ResourceSpotRepository $resourceRepo */
        $resourceRepo = $this->managerRegistry->getRepository(ResourceSpot::class);
        $resourceSpots = $resourceRepo->findBy(['owningWorld' => $worldId]);

        $resourcesSpotsDict = [];
        foreach ($resourceSpots as $resourceSpot) {
            if (!array_key_exists($resourceSpot->getNode()->getId(), $resourcesSpotsDict)) {
                $resourcesSpotsDict[$resourceSpot->getNode()->getId()] = [];
            }
            $resourcesSpotsDict[$resourceSpot->getNode()->getId()][] = $resourceSpot;
        }

        return $this->render(
            'Admin/adminWorldDetails.html.twig',
            [
                'world' => $world,
                'nodes' => $nodes,
                'resourceSpots' => $resourcesSpotsDict,
            ]
        );
    }

    /**
     * @Route("/admin/worldNodes/rename", methods={"POST"}, name="resetWorldNodeName"))
     */
    public function AdminResetWorldNodeName(Request $request): Response
    {
        $response = new JsonResponse();
        $response->setStatusCode(Response::HTTP_FORBIDDEN);

        $payload = json_decode($request->request->get('json', '{}'), true);
        $nodeId = $payload['nodeId'];
        $newName = $payload['newName'];

        /** @var WorldMapNodeRepository $repo */
        $repo = $this->managerRegistry->getRepository(WorldMapNode::class);
        $node = $repo->find($nodeId);

        $node->setName($newName);
        $this->managerRegistry->getManager()->persist($node);
        $this->managerRegistry->getManager()->flush();

        $responseData = [
            'success' => true,
            'message' => 'Rename successful!',
        ];

        assert(count($responseData) > 0);
        $response->setContent(json_encode($responseData));
        $response->setStatusCode(Response::HTTP_OK);

        return $response;
    }

    /**
     * @Route("/admin/world/add", name="adminAddWorld")
     */
    public function adminAddWorld(): Response
    {
        // TODO Find all file names in the folder "public/maps/ !
        $maps = [];
        for ($i = 1; $i <= WorldManager::K_MAX_WORLD_ID; ++$i) {
            $maps[] = (string) $i;
        }

        // /** @var WorldDataRepository $repo */
        // $repo = $this->managerRegistry->getRepository(WorldData::class);
        $params['worldDataNames'] = $maps;

        return $this->render('Admin/adminAddWorld.html.twig', $params);
    }

    /**
     * @Route("/admin/gamedata/load", methods={"GET"}, name="adminLoadGameData"))
     */
    public function adminLoadGameData(): Response
    {
        $this->gameDataLoaderService->loadAllGameData($this->managerRegistry->getManager());

        dump('Data loaded');

        return $this->forward('App\Controller\AdminController::adminManageWebsite');
    }

    /**
     * @Route("/admin/world/create", methods={"POST"}, name="createWorld"))
     */
    public function AdminCreateWorld(Request $request): Response
    {
        $worldName = $request->request->getAlpha('worldName');
        // Right now, we can't getAlpha since the map names are integer, so it fails to convert them as string for some reason...
        $worldDataName = $request->request->get('worldDataName');
        $language = $request->request->getAlpha('lang');
        $speed = $request->request->getInt('speed');
        $slotCount = $request->request->getInt('slotCount', -1);

        if (
            '' !== $worldName
            && '' !== $worldDataName
            && '' !== $language
            && -1 !== $slotCount
        ) {
            if ($this->worldManager->generateNewWorld($worldDataName, $worldName, $language, $speed, $slotCount)) {
                dump('World generated successfully');
            } else {
                dump('World failed to create : Server error');
            }
        } else {
            dump('World failed to create : Missing parameters');
        }

        return $this->forward('App\Controller\AdminController::AdminWorldIndex');
    }

    /**
     * @Route("/admin/player", name="adminPlayers")
     */
    public function AdminPlayersIndex(): Response
    {
        /** @var UserRepository $repo */
        $repo = $this->managerRegistry->getRepository(User::class);
        $params['users'] = $repo->findAll();

        return $this->render('Admin/adminPlayers.html.twig', $params);
    }

    /**
     * @Route("/admin/player/filter/{type}", name="adminPlayersFiltered")
     */
    public function AdminPlayersFiltered(int $type): Response
    {
        $orderBy = [];
        switch ($type) {
            case 0:
                $orderBy = ['lastConnection' => 'DESC'];
                break;
            default:
        }

        /** @var UserRepository $repo */
        $repo = $this->managerRegistry->getRepository(User::class);
        $params['users'] = $repo->findBy([], $orderBy);

        return $this->render('Admin/adminPlayers.html.twig', $params);
    }

    /**
     * @Route("/admin/player/inGame/", name="adminPlayersInGame")
     */
    public function AdminPlayersInGame(): Response
    {
        /** @var UserRepository $repo */
        $repo = $this->managerRegistry->getRepository(User::class);
        $params['users'] = $repo->findAllAlivePlayers();

        return $this->render('Admin/adminInGamePlayers.html.twig', $params);
    }

    /**
     * @Route("/admin/moderator", name="adminModerators")
     */
    public function AdminModerator(): Response
    {
        /** @var UserRepository $repo */
        $repo = $this->managerRegistry->getRepository(User::class);
        $admins = $repo->findAllPlayersWithRole(User::ROLE_ADMIN);
        $moderators = $repo->findAllPlayersWithRole(User::ROLE_MODERATOR);
        $params['users'] = array_merge($admins, $moderators);

        return $this->render('Admin/adminModerators.html.twig', $params);
    }

    /**
     * @Route("/admin/promoteUser/{userId}", name="adminPromoteUser")
     */
    public function AdminPromoteUser(int $userId): Response
    {
        if ($this->isAdmin()) {
            /** @var UserRepository $repo */
            $repo = $this->managerRegistry->getRepository(User::class);
            /** @var User $user */
            $user = $repo->find($userId);
            if (null != $user) {
                $user->setRoles([User::ROLE_MODERATOR]);
                $this->managerRegistry->getManager()->persist($user);
                $this->managerRegistry->getManager()->flush();
            }
        }

        return $this->redirectToRoute('adminPlayers');
    }

    /**
     * @Route("/admin/revokeUser/{userId}", name="adminRevokeUser")
     */
    public function AdminRevokeUser(int $userId): Response
    {
        if ($this->isAdmin()) {
            /** @var UserRepository $repo */
            $repo = $this->managerRegistry->getRepository(User::class);
            /** @var User $user */
            $user = $repo->find($userId);
            if (null != $user) {
                $user->setRoles([User::ROLE_USER]);
                $this->managerRegistry->getManager()->persist($user);
                $this->managerRegistry->getManager()->flush();
            }
        }

        return $this->redirectToRoute('adminPlayers');
    }

    /**
     * @Route("/admin/generals", name="adminGenerals")
     */
    public function AdminGeneralIndex(): Response
    {
        /** @var GeneralRepository $repo */
        $repo = $this->managerRegistry->getRepository(General::class);
        $params['generals'] = $repo->findAll();

        return $this->render('Admin/adminGenerals.html.twig', $params);
    }

    /**
     * @Route("/admin/generals/rename", methods={"POST"}, name="renameGeneral"))
     */
    public function AdminRenameGeneral(Request $request): Response
    {
        $response = new JsonResponse();
        $response->setStatusCode(Response::HTTP_FORBIDDEN);

        $payload = json_decode($request->request->get('json', '{}'), true);
        $generalId = $payload['generalId'];
        $newName = $payload['newName'];

        $generalRepository = $this->managerRegistry->getRepository(General::class);
        $general = $generalRepository->find($generalId);

        $general->setName($newName);
        $this->managerRegistry->getManager()->persist($general);
        $this->managerRegistry->getManager()->flush();

        $responseData = [
            'success' => true,
            'message' => 'Rename successful!',
        ];

        assert(count($responseData) > 0);
        $response->setContent(json_encode($responseData));
        $response->setStatusCode(Response::HTTP_OK);

        return $response;
    }

    /**
     * @Route("/admin/battles/{worldId}", name="adminBattles", requirements={"worldId"="\d+"})
     */
    public function AdminBattleIndex(int $worldId = -1): Response
    {
        /** @var BattleRepository $repo */
        $repo = $this->managerRegistry->getRepository(Battle::class);

        $queryFilterParams = [];
        if ($worldId > -1) {
            $queryFilterParams['worldMap'] = $worldId;

            /** @var WorldMapRepository $worldRepo */
            $worldRepo = $this->managerRegistry->getRepository(WorldMap::class);
            $params['world'] = $worldRepo->findOneBy(['id' => $worldId]);
        }

        $params['battles'] = $repo->findBy($queryFilterParams, ['isFinished' => 'ASC', 'startTime' => 'DESC']);

        return $this->render('Admin/adminBattles.html.twig', $params);
    }

    /**
     * @Route("/admin/stats", name="adminStats")
     */
    public function AdminStatistics(): Response
    {
        /** @var UserRepository $repo */
        $repo = $this->managerRegistry->getRepository(User::class);

        $connectedSinceLastWeek = $repo->findMostRecentConnectedPlayers(7);
        $connectedSinceLastMonth = $repo->findMostRecentConnectedPlayers(30);

        $params = [
            'lastWeekCount' => $connectedSinceLastWeek,
            'lastMonthCount' => $connectedSinceLastMonth,
        ];

        return $this->render('Admin/adminStats.html.twig', $params);
    }

    /**
     * Only meant to debug Battle blocked in staging!
     *
     * @Route("/admin/battles/cleanup/{battleId}", name="cleanupBattle"))
     */
    public function AdminCleanupBattle(int $battleId): Response
    {
        $repo = $this->managerRegistry->getRepository(Battle::class);
        $battle = $repo->find($battleId);

        if (null !== $battle && $battle->isFinished()) {
            foreach ($battle->getEngagedGenerals() as $general) {
                $general->setBattle(null);
                $this->managerRegistry->getManager()->persist($general);
            }

            foreach ($battle->getAllEngagedArmies() as $engagedArmy) {
                $engagedArmy->clearLinkedArmy();
            }

            $this->managerRegistry->getManager()->persist($battle);
            $this->managerRegistry->getManager()->flush();
        }

        return $this->redirectToRoute('adminPlayers');
    }

    /**
     * @Route("/admin/error", name="adminError")
     *
     * @throws \Exception
     */
    public function AdminExceptionTest(): Response
    {
        throw new \Exception('Erreur test!');
    }

    private function isAdmin(): bool
    {
        return 'ROLE_ADMIN' === $this->getUser()?->getRoles()[0];
    }
}
