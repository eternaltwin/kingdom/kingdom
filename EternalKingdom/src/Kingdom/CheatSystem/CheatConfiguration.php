<?php

namespace App\Kingdom\CheatSystem;

class CheatConfiguration
{
    // Outgame
    public const bool CHEAT_CAN_SEE_SERVER_SELECTION_WHILE_PLAYING = true;

    // When spawning, you will be able to override your lord, city or armies. If you are alive, kill yourself first
    public const bool CHEAT_CUSTOM_LORD = false;
    public const bool CHEAT_CUSTOM_CITY = false;
    public const bool CHEAT_CUSTOM_ARMY = false;

    // Lord lifecycle
    public const bool CHEAT_IGNORE_LORD_INACTIVITY = true;

    /**
     * Allow to kill our Lord thanks to a button in the Management page or using the right URL.
     */
    public const bool CHEAT_FORCE_DEATH_ENABLED = true;

    // Grid
    /**
     * Will load a hard-coded grid when initializing a new one.
     */
    public const bool CHEAT_USE_STATIC_GRID = false;
    /**
     * Allow invalid swap in the city grid (for instance, two tiles that doesn't create an alignment of 3).
     */
    public const bool CHEAT_ALLOW_INVALID_GRID_SWAP = false;
    /**
     * Useful cheat to test grid reset logic after a swap.
     */
    public const bool CHEAT_ALWAYS_TRIGGER_GRID_RESET = false;

    // City
    /**
     * Will finish the current pending construction in one Hammer alignment.
     */
    public const bool CHEAT_INSTANT_CONSTRUCTION = false;

    /**
     * Playing One turn will add one extra year !
     */
    public const bool CHEAT_FASTER_AGING = false;

    public const bool CHEAT_IGNORE_HEALTH_DEGRADATION_COOLDOWN = false;

    // WorldMap Movements
    /**
     * Allow general movement without armies.
     */
    public const bool CHEAT_ALLOW_MOVE_WITHOUT_ARMY = false;
    /**
     * Skip the movement delay before reaching destination.
     */
    public const bool CHEAT_INSTANT_MOVE = false;

    // World Tick
    /**
     * Allow dev to skip the delay between each tick.
     */
    public const bool CHEAT_IGNORE_WORLD_TICK_DELAY = false;

    // Battle
    /**
     * Skip the Battle fights and end battle immediately.
     */
    public const bool CHEAT_INSTANT_BATTLE_END = false;
}
