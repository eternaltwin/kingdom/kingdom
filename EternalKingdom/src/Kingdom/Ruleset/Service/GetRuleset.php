<?php

namespace App\Kingdom\Ruleset\Service;

use App\Kingdom\Ruleset\CoreRuleset\CoreRuleset_OriginalKingdom;
use App\Kingdom\Ruleset\Ruleset;
use App\Kingdom\User\Interface\CurrentUserProviderInterface;

/**
 * Get the core ruleset corresponding to the world ruleset version, and get additionnal rulesets listed on the current WorldMap (unimplemented).
 *
 * Currently there is only one core ruleset version (1.0.0) and this one is always returned
 */
class GetRuleset
{
    public function __construct(
        /** @phpstan-ignore-next-line */
        private readonly CurrentUserProviderInterface $userRepository,
    ) {
    }

    public function exec(): Ruleset
    {
        // Here we can customize rules for map and lord, cause we have access to currentUser
        return new Ruleset(new CoreRuleset_OriginalKingdom());
    }
}
