<?php

namespace App\Kingdom\Ruleset;

use App\Kingdom\Lord\Enum\TitleEnum;
use App\Kingdom\Ruleset\CoreRuleset\CoreRuleset;

class Ruleset
{
    public function __construct(
        private readonly CoreRuleset $coreRuleset
    ) {
    }

    public function getTitleFromApogee(int $apogee): TitleEnum
    {
        $titles = $this->coreRuleset->getTitles();
        usort($titles, fn ($a, $b) => $b['glory'] - $a['glory']);

        foreach ($titles as $title) {
            if ($title['glory'] <= $apogee) {
                return $title['id'];
            }
        }

        $title = $titles[array_key_last($titles)];

        return $title['id'];
    }

    /**
     * How many fights can we have at maximum at the same time during a Battle?
     */
    public function getMaxSimultaneousFights(int $wallLevel): int
    {
        return max(0, $this->coreRuleset->getMaxSimultaneousFightsInBattle() - $wallLevel);
    }

    public function getUnitsInfo(string $unitName): array
    {
        return $this->coreRuleset->getUnitsInfo()[$unitName];
    }

    public function getAllUnitsInfo(): array
    {
        return $this->coreRuleset->getUnitsInfo();
    }
}
