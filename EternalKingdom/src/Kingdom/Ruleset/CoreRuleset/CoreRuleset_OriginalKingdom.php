<?php

namespace App\Kingdom\Ruleset\CoreRuleset;

use App\Kingdom\City\Enum\UnitEnum;
use App\Kingdom\Lord\Enum\TitleEnum;
use SebastianBergmann\CodeCoverage\Report\Xml\Unit;

/**
 * @phpstan-import-type Title from CoreRuleset
 */
class CoreRuleset_OriginalKingdom extends CoreRuleset
{
    /**
     * @return array<Title>
     */
    public function getTitles(): array
    {
        return [
            ['id' => TitleEnum::KNIGHT, 'name' => TitleEnum::KNIGHT->value, 'glory' => 1],
            ['id' => TitleEnum::LORD, 'name' => TitleEnum::LORD->value, 'glory' => 3],
            ['id' => TitleEnum::BARON, 'name' => TitleEnum::BARON->value, 'glory' => 7],
            ['id' => TitleEnum::VISCOUNT, 'name' => TitleEnum::VISCOUNT->value, 'glory' => 10],
            ['id' => TitleEnum::COUNT, 'name' => TitleEnum::COUNT->value, 'glory' => 15],
            ['id' => TitleEnum::MARQUIS, 'name' => TitleEnum::MARQUIS->value, 'glory' => 20],
            ['id' => TitleEnum::DUKE, 'name' => TitleEnum::DUKE->value, 'glory' => 30],
            ['id' => TitleEnum::PRINCE, 'name' => TitleEnum::PRINCE->value, 'glory' => 50],
            ['id' => TitleEnum::KING, 'name' => TitleEnum::KING->value, 'glory' => 75],
            ['id' => TitleEnum::EMPEROR, 'name' => TitleEnum::EMPEROR->value, 'glory' => 100],
        ];
    }

    public function getMaxSimultaneousFightsInBattle(): int
    {
        return 7;
    }

    // TODO Replace this array by array of class to ease use and remove keys!
    public function getUnitsInfo(): array
    {
        return [
            UnitEnum::SOLDIER->value => [
                self::healthKey => 50,
            ],
            UnitEnum::PIKEMAN->value => [
                self::healthKey => 70,
                self::strongAgainstKey => [UnitEnum::HORSEMAN->value, UnitEnum::KNIGHT->value],
                self::dmgBonusKey => 3,
            ],
            UnitEnum::ARCHER->value => [
                self::healthKey => 70,
                self::strongAgainstKey => [UnitEnum::PIKEMAN->value, UnitEnum::PALADIN->value],
                self::dmgBonusKey => 2,
            ],
            UnitEnum::HORSEMAN->value => [
                self::healthKey => 70,
                self::strongAgainstKey => [UnitEnum::ARCHER->value],
                self::dmgBonusKey => 2,
            ],
            UnitEnum::KNIGHT->value => [
                self::healthKey => 70,
                self::strongAgainstKey => [UnitEnum::SOLDIER->value],
                self::dmgBonusKey => 2,
            ],
            UnitEnum::MOUNTED_ARCHER->value => [
                self::healthKey => 80,
                self::abilityKey => self::cancelAnyBonus,
            ],
            UnitEnum::PALADIN->value => [
                self::healthKey => 100,
            ],
            UnitEnum::BALLISTA->value => [
                self::healthKey => 60,
                self::sideBonusKey => self::defenseSideKey,
                self::dmgBonusKey => 4,
            ],
            UnitEnum::CATAPULT->value => [
                self::healthKey => 60,
                self::sideBonusKey => self::attackSideKey,
                self::dmgBonusKey => 4,
            ],
        ];
    }
}
