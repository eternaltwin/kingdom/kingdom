<?php

namespace App\Kingdom\World\Service;

use App\Entity\Resources;
use App\Entity\World\WorldMap;
use App\Enum\Language;
use App\Enum\WorldSpeedType;
use App\Repository\ResourcesRepository;
use App\Repository\World\WorldMapRepository;
use App\Service\WorldManager;
use Doctrine\ORM\EntityManagerInterface;

class AutomaticWorldGenerator
{
    public const bool SETTING_ENABLE_AUTOMATIC_WORLD_GENERATION = true;

    // Cache
    private array $mapNamesPerLanguage = [];
    private array $mapData = [];

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly WorldManager $worldManager
    ) {
    }

    public function exec()
    {
        if ($this->canGenerateNewWorld()) {
            if ($this->worldManager->loadAllMapNames($this->mapNamesPerLanguage)) {
                foreach (Language::getAvailableLanguages() as $language) {
                    if ($this->shouldGenerateNewWorld($language)) {
                        $this->generateNewWorld($language);
                    }
                }
            }
        }
    }

    private function generateNewWorld(string $language): void
    {
        $worldDataId = $this->getRandomWorldDataIdPerLanguage($language);
        if ($worldDataId > 0) {
            $worldDataName = (string) $worldDataId;
            $mapDataLoaded = $this->worldManager->loadWorldData($worldDataName, $this->mapData);

            if ($mapDataLoaded) {
                $language = $this->mapData['language'];
                $slotCount = $this->countMaxSlotsInMap();

                /** @var WorldMapRepository $worldMapRepository */
                $worldMapRepository = $this->entityManager->getRepository(WorldMap::class);
                $worldCountForLanguage = $worldMapRepository->count(['language' => $language]);

                $mapNameCount = count($this->mapNamesPerLanguage[$language]);
                if ($worldCountForLanguage < $mapNameCount) {
                    $worldName = $this->mapNamesPerLanguage[$language][$worldCountForLanguage];
                } else {
                    $worldName = $language.$mapNameCount;
                }

                $this->worldManager->generateNewWorld($worldDataName, $worldName, $language, WorldSpeedType::REGULAR, $slotCount);
            }
        }
    }

    private function getRandomWorldDataIdPerLanguage(string $language): int
    {
        $bounds = [];
        switch ($language) {
            case Language::FRENCH:
                $bounds = [1, 5];
                break;
            case Language::ENGLISH:
                $bounds = [6, 10];
                break;
            case Language::SPANISH:
                $bounds = [11, 13];
                break;
            case Language::GERMAN:
                $bounds = [14, 15];
                break;
            default:
                break;
        }

        if ($bounds > 0) {
            return random_int($bounds[0], $bounds[1]);
        }

        return -1;
    }

    /**
     * Check if the world generation is enabled and the db is initialized to do so.
     */
    private function canGenerateNewWorld(): bool
    {
        $canGenerateNewWorld = false;

        /** @var ResourcesRepository $resourceRepo */
        $resourceRepo = $this->entityManager->getRepository(Resources::class);
        $resourceRepo = $resourceRepo->count([]);

        if ($resourceRepo > 0) {
            $canGenerateNewWorld = self::SETTING_ENABLE_AUTOMATIC_WORLD_GENERATION;
        }

        return $canGenerateNewWorld;
    }

    /**
     * We should generate a new world for a specific language
     * if there is no any free slot available for any world of this language!
     */
    private function shouldGenerateNewWorld(string $language): bool
    {
        $shouldGenerateNewWorld = false;

        /** @var WorldMapRepository $worldMapRepository */
        $worldMapRepository = $this->entityManager->getRepository(WorldMap::class);
        $worldWithAvailableSlots = $worldMapRepository->findAllWorldsWithAvailableSlotsPerLanguage($language);

        if (0 == count($worldWithAvailableSlots)) {
            $shouldGenerateNewWorld = true;
        }

        return $shouldGenerateNewWorld;
    }

    private function countMaxSlotsInMap(): int
    {
        $slotCount = 0;
        foreach ($this->mapData['cities'] as $city) {
            if ($city['cap']) {
                ++$slotCount;
            }
        }

        return $slotCount;
    }
}
