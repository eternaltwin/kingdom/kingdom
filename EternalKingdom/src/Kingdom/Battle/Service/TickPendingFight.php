<?php

namespace App\Kingdom\Battle\Service;

use App\Entity\Battle\Battle;
use App\Entity\Battle\Fight;
use App\Entity\Battle\FightingUnit;
use App\Entity\Battle\FightSurvivor;
use App\Entity\General;
use App\Entity\World\WorldMap;
use App\Enum\LogEventType;
use App\Kingdom\City\Enum\UnitEnum;
use App\Repository\Battle\FightRepository;
use App\Service\GameLogManager;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Each tick will execute one step of every single fights of the relevant Battles.
 * For each fight :
 * - Parse dead units & log them
 * - Store survivors
 * - Update BattleStats
 * - Trigger fights refill if needed.
 */
class TickPendingFight
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly GameLogManager $gameLogManager,
        private readonly RefillFights $refillPendingFightService
    ) {
    }

    /**
     * Execute one step of every single fights
     * For each fight, parse dead units, log them and refill new fights if relevant.
     *
     * @param WorldMap $worldMap from which world should we tick Battles?
     *
     * @return bool Do we still have fights to tick?
     */
    public function exec(WorldMap $worldMap): bool
    {
        /** @var FightRepository $fightRepo */
        $fightRepo = $this->entityManager->getRepository(Fight::class);

        $fightRepo->tickDamage($worldMap);
        $this->entityManager->flush();

        $allFights = $fightRepo->findAllFightsWithDeadUnits($worldMap);
        if (count($allFights) > 0) {
            $this->gameLogManager->beginLogTransaction();

            $battlesWithFinishedFights = [];
            foreach ($allFights as $fight) {
                /** @var FightingUnit $attackingUnit */
                $attackingUnit = $fight->getAttackingUnit();
                /** @var FightingUnit $attackingUnit */
                $defendingUnit = $fight->getDefendingUnit();

                if ($attackingUnit->getRemainingHealth() <= 0) {
                    $this->registerUnitDeath($fight->getBattle(), $attackingUnit);
                } else {
                    $this->registerFightSurvivor($attackingUnit);
                }

                if ($defendingUnit->getRemainingHealth() <= 0) {
                    $this->registerUnitDeath($fight->getBattle(), $defendingUnit);
                } else {
                    $this->registerFightSurvivor($defendingUnit);
                }

                $this->entityManager->remove($fight);

                if (false == array_key_exists($fight->getBattle()->getId(), $battlesWithFinishedFights)) {
                    $battlesWithFinishedFights[$fight->getBattle()->getId()] = $fight->getBattle();
                }
            }

            $this->gameLogManager->finishLogTransaction();

            foreach ($battlesWithFinishedFights as $battle) {
                if ($this->canRefillBattle($battle)) {
                    $this->refillPendingFightService->exec($battle);
                }
            }

            $this->entityManager->flush();

            $allFights = $fightRepo->findFightsByWorld($worldMap);

            return count($allFights) > 0;
        }

        return true;
    }

    private function canRefillBattle(Battle $battle): bool
    {
        if (null === $battle->getLocation()->getSovereign()) {
            return false;
        }

        return true;
    }

    private function registerUnitDeath(Battle $battle, FightingUnit $unit): void
    {
        $this->entityManager->remove($unit);

        /** @var General $owningGeneral */
        $owningGeneral = $battle->getEngagedGenerals()->findFirst(
            fn (int $i, General $general) => $general->getArmyContainer() === $unit->getOwningEngagedArmy()->getLinkedArmy()
        );

        $isConstruction = UnitEnum::BALLISTA->value == $unit->getUnitType()->getName() || UnitEnum::CATAPULT->value == $unit->getUnitType()->getName();

        if (null !== $owningGeneral) {
            $this->gameLogManager->addBattleLog(
                $battle,
                $isConstruction ? LogEventType::BATTLE_GENERAL_UNIT_DESTROYED : LogEventType::BATTLE_GENERAL_UNIT_DIED,
                false,
                [
                    '%unit%' => $unit->getUnitType()->getName(),
                    '%generalName%' => $owningGeneral->getName(),
                ]
            );
        } else {
            $locationOwner = $battle->getLocation()->getSovereign();
            if (null !== $locationOwner) {
                $this->gameLogManager->addBattleLog(
                    $battle,
                    $isConstruction ? LogEventType::BATTLE_NODE_OWNER_UNIT_DESTROYED : LogEventType::BATTLE_NODE_OWNER_UNIT_DIED,
                    false,
                    [
                        '%unit%' => $unit->getUnitType()->getName(),
                        '%userId%' => $locationOwner->getUser()->getId(),
                        '%title%' => $locationOwner->getTitle(),
                        '%lord%' => $locationOwner->getName(),
                    ]
                );
            } else {
                assert($battle->getLocation()->getData()->isSpawnPoint());
                $this->gameLogManager->addBattleLog(
                    $battle,
                    $isConstruction ? LogEventType::BATTLE_BARBARIAN_UNIT_DESTROYED : LogEventType::BATTLE_BARBARIAN_UNIT_DIED,
                    false,
                    [
                        '%unit%' => $unit->getUnitType()->getName(),
                    ]
                );
            }
        }

        $battleUnitStat = $unit->getOwningEngagedArmy()->getBattleStat($unit->getUnitType()->getName());
        if (null !== $battleUnitStat) {
            $battleUnitStat->setHealthyCount($battleUnitStat->getHealthyCount() - 1);
            $battleUnitStat->setDeadCount($battleUnitStat->getDeadCount() + 1);
        }
        $this->entityManager->persist($battleUnitStat);
    }

    private function registerFightSurvivor(FightingUnit $unit): void
    {
        $fightSurvivor = new FightSurvivor($unit->getUnitType(), $unit->getOwningEngagedArmy(), $unit->getRemainingHealth());
        $this->entityManager->persist($fightSurvivor);
    }
}
