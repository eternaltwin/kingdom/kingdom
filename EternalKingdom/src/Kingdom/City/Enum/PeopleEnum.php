<?php

namespace App\Kingdom\City\Enum;

enum PeopleEnum: string
{
    case MERCHANT = 'merchant';
    case FARMER = 'farmer';
    case WOODER = 'lumberjack';
    case WORKER = 'worker';
    case RECRUITER = 'recruiter';
    case UNEMPLOYED = 'citizen';
}
