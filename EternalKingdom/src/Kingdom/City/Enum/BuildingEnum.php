<?php

namespace App\Kingdom\City\Enum;

enum BuildingEnum: string
{
    case PALACE = 'palace';
    case CASERN = 'barracks';
    case FARM = 'farm';
    case MARKET = 'market';
    case HUT = 'hut';
    case WORKSHOP = 'workshop';
    case YARD = 'constructionSite';
    case STORE = 'attic';
    case FORGE = 'forge';
    case STABLE = 'stable';
    case FACTORY = 'factory';
    case HEADQUARTERS = 'headquarters';
    case WALL = 'wall';
    case GUARD_TOWER = 'guardTower';
    case MILITARY_ACADEMY = 'militaryAcademy';
    case ARCHERY = 'archery';
    case BUTCHER = 'butcher';
    case CAULDRON = 'cauldron';
}
