<?php

namespace App\Kingdom\City\Enum;

enum ResourceEnum: string
{
    case FOOD = 'wheat';
    case WOOD = 'wood';
    case GOLD = 'gold';
    case METAL = 'iron';
    case LIN = 'lin';
    case HORSE = 'horse';
}
