<?php

namespace App\Kingdom\Lord\Enum;

enum TitleEnum: string
{
    case KNIGHT = 'knight';     // 1
    case LORD = 'lord';       // 3
    case BARON = 'baron';      // 7
    case VISCOUNT = 'viscount';   // 10
    case COUNT = 'count';      // 15
    case MARQUIS = 'marquis';    // 20
    case DUKE = 'duke';       // 30
    case PRINCE = 'prince';     // 50
    case KING = 'king';       // 75
    case EMPEROR = 'emperor';    // 100

    public function index(): int
    {
        $index = array_search($this, self::cases());
        if (false === $index) {
            throw new \LogicException('Impossible case, this error is here to please phpstan');
        }

        return $index;
    }
}
