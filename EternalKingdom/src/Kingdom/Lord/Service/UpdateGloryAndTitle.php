<?php

namespace App\Kingdom\Lord\Service;

use App\Entity\Lord;
use App\Enum\LogEventType;
use App\Kingdom\Lord\Enum\TitleEnum;
use App\Kingdom\Ruleset\Service\GetRuleset;
use App\Service\GameLogManager;

class UpdateGloryAndTitle
{
    public function __construct(
        private readonly GetRuleset $getRuleset,
        private readonly GameLogManager $gameLogManager,
    ) {
    }

    public function exec(Lord $lord): void
    {
        $ruleset = $this->getRuleset->exec();

        $oldTitle = $lord->getTitle();

        $currentGlory = $this->calculateGloryForLord($lord);
        $lord->setGlory($currentGlory, $ruleset);

        if ($lord->getTitle() !== $oldTitle) {
            $this->logPromotion($lord, $oldTitle);
        }
    }

    private function calculateGloryForLord(Lord $lord): int
    {
        return count($lord->getKingdom());
    }

    private function logPromotion(Lord $lord, TitleEnum $oldTitle): void
    {
        $worldMap = $lord->getWorldMap();
        if (null === $worldMap) {
            throw new \LogicException('Lord sould have a world !');
        }

        $this->gameLogManager->beginLogTransaction();
        $this->gameLogManager->addKingdomLog(
            $lord,
            LogEventType::MANAGEMENT_PROMOTION,
            false,
            [
                '%title%' => $lord->getTitle(),
            ]
        );
        $this->gameLogManager->addWorldLog(
            $worldMap,
            LogEventType::WORLD_PROMOTION,
            false,
            [
                '%userId%' => $lord->getUser()->getId(),
                '%lord%' => $lord->getName(),
                '%title%' => $oldTitle,
                '%newTitle%' => $lord->getTitle(),
            ]
        );
        $this->gameLogManager->finishLogTransaction();
    }
}
