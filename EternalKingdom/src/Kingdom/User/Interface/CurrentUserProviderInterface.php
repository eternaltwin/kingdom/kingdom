<?php

namespace App\Kingdom\User\Interface;

use App\Entity\User;

interface CurrentUserProviderInterface
{
    public function getCurrentUser(): ?User;
}
