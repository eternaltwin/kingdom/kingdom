<?php

namespace App\Enum;

/**
 * Enum used to identify a log event.
 *
 * Class LogEventType
 */
class LogEventType
{
    // CITY
    public const string CITY_RESOURCE_PRODUCTION = 'city.resource.production';
    public const string CITY_RESOURCE_WASTE = 'city.resource.waste';
    public const string CITY_RESOURCE_CONVERSION = 'city.resource.conversion';
    public const string CITY_RESOURCE_CONSUMPTION_FOOD = 'city.consumption.food';
    public const string CITY_RESOURCE_CONSUMPTION_FOOD_GOLD = 'city.consumption.foodAndGold';

    public const string CITY_HAMMER_PROGRESS = 'city.hammer.progress';
    public const string CITY_HAMMER_NO_CONSTRUCTION = 'city.hammer.noConstruction';
    public const string CITY_HAMMER_PRODUCTION = 'city.hammer.production';

    public const string CITY_ARMY_NO_BARRACKS = 'city.army.noBarracks';
    public const string CITY_ARMY_NO_RECRUITMENT = 'city.army.noRecruitment';
    public const string CITY_ARMY_PRODUCTION = 'city.army.recruitment';
    public const string CITY_ARMY_CONVERSION = 'city.army.conversion';
    public const string CITY_ARMY_COST = 'city.army.cost';

    public const string CITY_CITIZEN_BIRTH = 'city.citizen.new';
    public const string CITY_CITIZEN_CONVERSION = 'city.citizen.conversion';
    public const string CITY_CITIZEN_STARVATION = 'city.citizen.starvation';

    public const string CITY_CONSTRUCTION_BUILDING_STARTED = 'city.construction.building.started';
    public const string CITY_CONSTRUCTION_BUILDING_FINISHED = 'city.construction.building.finished';
    public const string CITY_CONSTRUCTION_BUILDING_CANCELLED = 'city.construction.building.cancelled';
    public const string CITY_CONSTRUCTION_UNIT_STARTED = 'city.construction.unit.started';
    public const string CITY_CONSTRUCTION_UNIT_FINISHED = 'city.construction.unit.finished';
    public const string CITY_CONSTRUCTION_UNIT_CANCELLED = 'city.construction.unit.cancelled';

    public const string CITY_REPLAY = 'city.replay';
    public const string CITY_GRID_RESET = 'city.gridReseted';
    public const string CITY_GENERAL_RECRUITMENT = 'city.general.recruitment';
    public const string CITY_GENERAL_HARVEST = 'city.general.harvest';

    // MANAGEMENT
    public const string MANAGEMENT_SPAWN = 'management.spawn';
    public const string MANAGEMENT_DEATH_INACTIVITY = 'management.death.inactivity';
    public const string MANAGEMENT_DEATH_NATURAL = 'management.death.natural';
    public const string MANAGEMENT_PROMOTION = 'management.promotion';
    public const string MANAGEMENT_HEALTH_DEGRADATION_1 = 'management.healthDegradation.excellentToGood';
    public const string MANAGEMENT_HEALTH_DEGRADATION_2 = 'management.healthDegradation.goodToBad';
    public const string MANAGEMENT_DIPLOMACY_FRIEND = 'management.diplomacy.friend';
    public const string MANAGEMENT_DIPLOMACY_ENEMY = 'management.diplomacy.enemy';
    public const string MANAGEMENT_DIPLOMACY_NEUTRAL = 'management.diplomacy.neutral';
    public const string MANAGEMENT_CROSS_RIGHT_ADDED = 'management.crossRight.added';
    public const string MANAGEMENT_CROSS_RIGHT_REMOVED = 'management.crossRight.removed';
    public const string MANAGEMENT_HARVEST_RIGHT_ADDED = 'management.harvestRight.added';
    public const string MANAGEMENT_HARVEST_RIGHT_REMOVED = 'management.harvestRight.removed';
    public const string MANAGEMENT_BATTLE_INSTIGATED_AGAINST_BARBARIAN = 'management.battle.instigated.againstBarbarian';
    public const string MANAGEMENT_BATTLE_INSTIGATED_AGAINST_PLAYER = 'management.battle.instigated.againstPlayer';
    public const string MANAGEMENT_BATTLE_DEFENSE_ON_TERRITORY = 'management.battle.defense.onOurTerritory';
    public const string MANAGEMENT_BATTLE_DEFENSE_ON_CAPITAL_CITY = 'management.battle.defense.onOurCapitalCity';
    public const string MANAGEMENT_CONQUEST_NO_RESISTANCE = 'management.conquest.noResistance';
    public const string MANAGEMENT_CONQUEST_WITH_BATTLE = 'management.conquest.withBattle';
    public const string MANAGEMENT_CONQUEST_FROM_VASSAL = 'management.conquest.fromVassal';
    public const string MANAGEMENT_TERRITORY_GOT_STOLEN = 'management.territoryGotStolen';
    public const string MANAGEMENT_BATTLE_WON = 'management.battle.won';
    public const string MANAGEMENT_BATTLE_LOST = 'management.battle.lost';

    // WORLD
    // TODO Rename those two logs WORLD_PLAYER_SPAWN_FREE & WORLD_PLAYER_SPAWN_AS_VASSAL
    public const string WORLD_SPAWN = 'world.spawn.free';
    public const string WORLD_SPAWN_VASSAL = 'world.spawn.vassal';
    public const string WORLD_DEATH_INACTIVITY = 'world.death.inactivity';
    public const string WORLD_DEATH_NATURAL = 'world.death.natural';
    public const string WORLD_RESOURCE_SPAWN = 'world.resource.spawn';
    public const string WORLD_PROMOTION = 'world.promotion';
    public const string WORLD_CONQUEST_NO_RESISTANCE = 'world.conquest.noResistance';
    public const string WORLD_CONQUEST_WITH_BATTLE = 'world.conquest.withBattle';

    // BATTLE
    public const string BATTLE_GARRISON_JOINED = 'battle.defense.garrisonJoined';
    public const string BATTLE_BARBARIAN_JOINED = 'battle.defense.barbarianJoined';
    public const string BATTLE_GENERAL_JOINED_DEFENSE = 'battle.defense.generalJoined';
    public const string BATTLE_GENERAL_JOINED_ATTACK = 'battle.attack.generalJoined';
    public const string BATTLE_DEFENSE_WON = 'battle.defense.won';
    public const string BATTLE_ATTACK_WON = 'battle.attack.won';
    public const string BATTLE_NODE_OWNER_UNIT_DIED = 'battle.nodeOwner.unit.died';
    public const string BATTLE_NODE_OWNER_UNIT_DESTROYED = 'battle.nodeOwner.unit.destroyed';
    public const string BATTLE_GENERAL_UNIT_DIED = 'battle.general.unit.died';
    public const string BATTLE_GENERAL_UNIT_DESTROYED = 'battle.general.unit.destroyed';
    public const string BATTLE_GENERAL_DEFEATED = 'battle.general.defeated';
    public const string BATTLE_BARBARIAN_UNIT_DIED = 'battle.barbarian.unit.died';
    public const string BATTLE_BARBARIAN_UNIT_DESTROYED = 'battle.barbarian.unit.destroyed';
}
