<?php

namespace App\Enum;

/**
 * Enum used to represent the different Languages available.
 */
class Language
{
    public const ENGLISH = 'english';
    public const FRENCH = 'french';
    public const SPANISH = 'spanish';
    public const GERMAN = 'german';

    /**
     * @return array<string>
     */
    public static function getAvailableLanguages(): array
    {
        return [
            self::ENGLISH,
            self::FRENCH,
            self::SPANISH,
            self::GERMAN,
        ];
    }
}
