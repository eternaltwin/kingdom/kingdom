<?php

namespace App\Enum;

class DiplomacyType
{
    public const TYPE_NEUTRAL = 'neutral';
    public const TYPE_FRIEND = 'friend';
    public const TYPE_ENEMY = 'enemy';

    /**
     * @return array<string>
     */
    public static function getAvailableTypes(): array
    {
        return [
            self::TYPE_NEUTRAL,
            self::TYPE_FRIEND,
            self::TYPE_ENEMY,
        ];
    }
}
