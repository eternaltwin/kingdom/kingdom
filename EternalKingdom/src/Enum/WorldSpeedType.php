<?php

namespace App\Enum;

/**
 * Enum used to represent the different Languages available.
 *
 * Class ResourceEnum
 */
class WorldSpeedType
{
    public const BLITZ = 0;
    public const QUICK = 1;
    public const REGULAR = 2;
    public const SLOW = 3;

    // 1 to 3
    // 5 to 15
    // 10 to 30
    // 20 to 60
    private const array speedMultiplier = [1.0, 5.0, 10.0, 20.0];
    public const speedName = ['blitz', 'quick', 'regular', 'slow'];

    /**
     * @return array<string>
     */
    public static function getAvailableLanguages(): array
    {
        return [
            self::BLITZ,
            self::QUICK,
            self::REGULAR,
            self::SLOW,
        ];
    }

    public static function getSpeedMultiplier(int $speedType): int
    {
        return WorldSpeedType::speedMultiplier[$speedType];
    }
}
