<?php

namespace App\Enum;

/**
 * Enum used to identify the construction type.
 *
 * Class ConstrucionEnum
 */
class ConstructionType
{
    public const TYPE_BUILDING = 'building';
    public const TYPE_UNIT = 'unit';

    /**
     * @return array<string>
     */
    public static function getAvailableTypes(): array
    {
        return [
            self::TYPE_BUILDING,
            self::TYPE_UNIT,
        ];
    }
}
