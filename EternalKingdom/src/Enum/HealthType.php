<?php

namespace App\Enum;

/**
 * Enum used to represent the lord wealth.
 *
 * Class HealthType
 */
class HealthType
{
    public const string TYPE_EXCELLENT = 'excellent';
    public const string TYPE_GOOD = 'good';
    public const string TYPE_BAD = 'bad';
    public const string TYPE_DYING = 'dying';
    public const string TYPE_DEAD = 'dead';

    /** @var array<string> user friendly named type */
    protected static array $typeName = [
        self::TYPE_EXCELLENT => 'excellent',
        self::TYPE_GOOD => 'good',
        self::TYPE_BAD => 'bad',
        self::TYPE_DYING => 'dying',
        self::TYPE_DEAD => 'dead',
    ];

    public static function getTypeName(string $typeShortName): string
    {
        if (!isset(static::$typeName[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$typeName[$typeShortName];
    }

    /**
     * @return array<string>
     */
    public static function getAvailableTypes(): array
    {
        return [
            self::TYPE_EXCELLENT,
            self::TYPE_GOOD,
            self::TYPE_BAD,
            self::TYPE_DYING,
            self::TYPE_DEAD,
        ];
    }

    public static function getNextHealthType(string $type): string
    {
        return match ($type) {
            self::TYPE_EXCELLENT => self::TYPE_GOOD,
            self::TYPE_GOOD => self::TYPE_BAD,
            self::TYPE_BAD => self::TYPE_DYING,
            default => self::TYPE_DEAD,
        };
    }
}
