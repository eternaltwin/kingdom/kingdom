<?php

namespace App\Enum;

/**
 * Enum used to represent the resource types.
 *
 * Class ResourceEnum
 */
class ResourceType
{
    public const TYPE_RESOURCE = 'resource';
    public const TYPE_POPULATION = 'population';
    public const TYPE_ARMY = 'army';

    // TODO Add all resource names to avoid issue (storage, unit, citizen type, ...)

    /**
     * @return array<string>
     */
    public static function getAvailableTypes(): array
    {
        return [
            self::TYPE_RESOURCE,
            self::TYPE_POPULATION,
            self::TYPE_ARMY,
        ];
    }
}
