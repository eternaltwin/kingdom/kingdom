<?php

namespace App\Enum;

/**
 * Enum used to identify a log event parameter.
 *
 * Class LogParamType
 */
class LogParamType
{
    /** Will display the value as it is */
    public const string RAW = 'raw';

    /** Will display the resource icon with the specified resource name. Requires Resource Name */
    public const string RESOURCE = 'resource';

    /** Will display the unit icon with the specified unit name. Requires Unit Name */
    public const string UNIT = 'unit';

    /** Will display the translation with the static (translation info) and dynamic data. */
    public const string TRANSLATION = 'translation';

    /** Will display the specified icon. */
    public const string ICON = 'iconPath';

    /** Will display a link that open a World on the relevant node. (requires WorldID, NodeID and NodeName in the dynData) */
    public const string NODE_LINK = 'nodeId';

    /** Will display a link that open the User page of the relevant player. (requires UserID, Title and UserName in the dynData) */
    public const string USER_LINK = 'userLink';

    /** Will display a link to open a finished Battle (requires BattleID and NodeName in the dynData). */
    public const string BATTLE_LINK = 'battleLink';
}
