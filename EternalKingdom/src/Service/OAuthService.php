<?php

namespace App\Service;

use App\Entity\User;
use Eternaltwin\Auth\AccessTokenAuthContext;
use Eternaltwin\Client\Auth;
use Eternaltwin\Client\HttpEtwinClient;
use Eternaltwin\OauthClient\RfcOauthClient;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class OAuthService
{
    /** @var RfcOauthClient */
    private $oauthClient;

    public function __construct(
        private readonly string $etwinUri,
        string $authorizeUri,
        string $tokenUri,
        string $oauthCallback,
        string $clientId,
        string $clientSecret,
        private readonly UserServiceInterface $userService
    ) {
        $this->oauthClient = new RfcOauthClient(
            $authorizeUri,
            $tokenUri,
            $oauthCallback,
            $clientId,
            $clientSecret
        );
    }

    /**
     * Method that given a code ask to the eternal twin identity provider to get the user informations
     * If the user do not exist locally it create a new one.
     */
    public function login(string $code): User
    {
        try {
            $accessToken = $this->oauthClient->getAccessTokenSync($code);
        } catch (GuzzleException|\JsonException $e) {
            throw new UnauthorizedHttpException($e->getMessage());
        }

        $apiClient = new HttpEtwinClient($this->etwinUri);
        $apiUser = $apiClient->getSelf(Auth::fromToken($accessToken->getAccessToken()));

        if (!$apiUser instanceof AccessTokenAuthContext) {
            throw new \LogicException('Auth context not supported');
        }

        $userId = $apiUser->getUser()->getId()->getInner()->toString();
        $user = $this->userService->findUserByUserId($userId);

        if (null === $user) {
            $displayName = $apiUser->getUser()->getDisplayName()->getCurrent()->getValue()->toString();

            $isAdmin = false;
            if ($userId == $_ENV['ADMIN_ID']) {
                $isAdmin = true;
            }
            $user = $this->userService->createUser($userId, $displayName, $isAdmin);
        } else {
            $this->userService->reconnect($user);
        }

        return $user;
    }

    public function getAuthorizationUri(string $scope, ?string $state): string
    {
        return (string) $this->oauthClient->getAuthorizationUri($scope, $state ?? '');
    }
}
