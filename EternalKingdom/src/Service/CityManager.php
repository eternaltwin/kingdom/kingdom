<?php

namespace App\Service;

use App\Entity\BuiltBuildings;
use App\Entity\City;
use App\Entity\Constructions;
use App\Entity\General;
use App\Entity\Lord;
use App\Entity\ResourceConversionSchemas;
use App\Entity\Resources;
use App\Entity\Storage;
use App\Entity\UnitArmy;
use App\Entity\World\ResourceSpot;
use App\Enum\ConstructionType;
use App\Enum\HealthType;
use App\Enum\LogEventType;
use App\Enum\ResourceType;
use App\Kingdom\CheatSystem\CheatConfiguration;
use App\Model\Alignment;

/**
 * City manager aims to :
 * - Reacts when alignments are made
 * - Manage Resources, Population, Armies and buildings
 */
class CityManager
{
    // TODO Move that to TurnManager
    public const MAX_TURN_COUNT = 100;

    // Building Production Bonus per level (0 to 5)
    public const BUILDING_LEVEL_MULTIPLIER = [1, 1.3, 1.55, 1.75, 1.9, 2];
    // Alignment length bonus per size (3 to 8)
    public const ALIGNMENT_LENGTH_MULTIPLIER = [1, 1.25, 1.5, 2, 3, 4];

    // Aging system
    public const MINIMUM_AGE_TO_DECLINE = 30;
    public const MAXIMUM_AGE = 150;
    public const SAFE_WEEK_COUNT = 50;

    // Appearance of the capital: number of citizen for each tower appearance
    public const TOWER_STEPS = [1, 16, 25, 50, 115];
    // Total number of dices for houses between two levels of tower
    public const TOTAL_NB_DICES = 51;

    private City $city;
    private array $maxStorages;

    // Buffers for grid alignment calculations
    private array $steps;
    private array $cumulativeGains;

    private bool $hasReset = false;

    /**
     * CityController constructor.
     */
    public function __construct(public GameDataProvider $gameDataProvider, private readonly GameLogManager $gameLogMgr)
    {
        $this->gameDataProvider->loadDicts();
    }

    /**
     * Increments the specified villager type and removes one citizen.
     */
    public function convertCitizen(City $city, int $jobType): bool
    {
        if ($city->getStorageCount('citizen') <= 0) {
            return false;
        }

        // TODO We could replace that by an enum!
        $jobName = null;
        switch ($jobType) {
            case 0:
                $jobName = 'farmer';
                break;
            case 1:
                $jobName = 'lumberjack';
                break;
            case 2:
                $jobName = 'worker';
                break;
            case 3:
                $jobName = 'merchant';
                break;
            case 4:
                $jobName = 'recruiter';
                break;
            default:
                break;
        }

        if (null !== $jobName) {
            $storage = $this->findOrCreateStorage($jobName, $city);
            $storage->setAmount($storage->getAmount() + 1);
            $storage = $this->findOrCreateStorage('citizen', $city);
            $storage->setAmount($storage->getAmount() - 1);

            $this->addAndFlushCityLog($city, LogEventType::CITY_CITIZEN_CONVERSION, ['%jobName%' => $jobName]);

            return true;
        }

        return false;
    }

    /**
     * Triggered once the turn is over, update resources, population, buildings, army...
     *
     * @throws \Exception
     */
    public function convertAlignments(City $city, array $steps): void
    {
        $this->city = $city;
        $this->steps = $steps;
        $this->hasReset = false;

        $city->setRemainingTurns($city->getRemainingTurns() - 1);

        $this->convertAlignmentsToGains();

        if (!array_key_exists('replay', $this->cumulativeGains)) {
            $consumption = $this->consumeFood();
            $goldConsumption = $this->consumeGold();

            // TODO Bug: consumption doesn't take in account new citizens!! D:
            $this->collectAllGains();

            if ($goldConsumption > 0) {
                $this->addAndFlushCityLog(
                    $city,
                    LogEventType::CITY_RESOURCE_CONSUMPTION_FOOD_GOLD,
                    ['%foodAmount%' => $consumption, '%goldAmount%' => $goldConsumption]
                );
            } else {
                $this->addAndFlushCityLog(
                    $city,
                    LogEventType::CITY_RESOURCE_CONSUMPTION_FOOD,
                    ['%amount%' => $consumption]
                );
            }

            $this->makeOld();
        } else {
            $this->collectAllGains();
        }

        $this->gameLogMgr->beginLogTransaction();

        if ($city->getStorageCount('wheat') <= 0) {
            $this->triggerStarvation();
        }

        if ($city->getStorageCount('gold') <= 0) {
            $this->triggerBankrupt();
        }

        $this->gameLogMgr->finishLogTransaction();

        if ($this->hasReset) {
            $this->addAndFlushCityLog($city, LogEventType::CITY_GRID_RESET);
        }
    }

    /**
     * Cumulate all resource modifications from all alignments.
     */
    private function convertAlignmentsToGains(): void
    {
        $this->cumulativeGains = [];
        $stepCount = count($this->steps);

        for ($step = 0; $step < $stepCount; ++$step) {
            // Skip any step that doesn't have alignments (f.i. RESET) !
            if (!isset($this->steps[$step]->alignments)) {
                $this->hasReset = true;
                continue;
            }
            $alignmentCount = count($this->steps[$step]->alignments);

            for ($i = 0; $i < $alignmentCount; ++$i) {
                $alignment = $this->steps[$step]->alignments[$i]; // just to read value not modify it!
                $gain = 0;
                switch ($alignment->element) {
                    case 0:
                        $gain = $this->getWheatProduction($alignment);
                        $this->initOrIncreaseGain('wheat', $gain);
                        break;
                    case 1:
                        $gain = $this->getGoldProduction($alignment);
                        $this->initOrIncreaseGain('gold', $gain);
                        break;
                    case 2:
                        $gain = $this->GetWoodProduction($alignment);
                        $this->initOrIncreaseGain('wood', $gain);
                        break;
                    case 3:
                        if ($this->city->getConstructionProject() instanceof Constructions) {
                            $gain = $this->getHammerProduction($alignment);
                            $this->initOrIncreaseGain('hammer', $gain);
                        } elseif ($this->city->getBuildingLevel('constructionSite') > 0) {
                            $gain = $this->getConstructionSiteProduction($alignment);
                            $this->initOrIncreaseGain('resourceFromHammer', $gain);
                        } else {
                            $this->initOrIncreaseGain('hammer', $gain);
                        }
                        break;
                    case 4:
                        if ($this->city->getBuildingLevel('barracks') > 0 && $this->city->isRecruiting()) {
                            $gain = $this->getSoldierProduction($alignment);
                        }
                        $this->initOrIncreaseGain('soldier', $gain);
                        break;
                    case 5:
                        $gain = 1;
                        $this->initOrIncreaseGain('citizen', $gain);
                        break;
                    case 6:
                        $gain = $this->getReplayProduction($alignment);
                        $this->initOrIncreaseGain('replay', $gain);
                        break;
                    default:
                        break;
                }

                $this->steps[$step]->alignments[$i]->gain = $gain;
            }
        }
    }

    private function initOrIncreaseGain($gainName, $gain): void
    {
        if (!array_key_exists($gainName, $this->cumulativeGains)) {
            $this->cumulativeGains[$gainName] = $gain;
        } else {
            $this->cumulativeGains[$gainName] += $gain;
        }
    }

    /* Production Formulas */

    private function getWheatProduction(Alignment $alignment): int
    {
        $jobCount = $this->city->getStorageCount('farmer');
        $buildingMultiplier = CityManager::BUILDING_LEVEL_MULTIPLIER[$this->city->getBuildingLevel('farm')];
        $alignmentMultiplier = CityManager::ALIGNMENT_LENGTH_MULTIPLIER[$alignment->getLength() - 3];

        return floor(6 * $buildingMultiplier * $alignmentMultiplier * $jobCount);
    }

    private function getGoldProduction(Alignment $alignment): int
    {
        $jobCount = $this->city->getStorageCount('merchant');
        $buildingMultiplier = CityManager::BUILDING_LEVEL_MULTIPLIER[$this->city->getBuildingLevel('market')];
        $alignmentMultiplier = CityManager::ALIGNMENT_LENGTH_MULTIPLIER[$alignment->getLength() - 3];

        return floor(max(1, 6 * $buildingMultiplier * $alignmentMultiplier * $jobCount));
    }

    private function getWoodProduction(Alignment $alignment): int
    {
        $jobCount = $this->city->getStorageCount('lumberjack');
        $buildingMultiplier = CityManager::BUILDING_LEVEL_MULTIPLIER[$this->city->getBuildingLevel('hut')];
        $alignmentMultiplier = CityManager::ALIGNMENT_LENGTH_MULTIPLIER[$alignment->getLength() - 3];

        return floor(max(1, 5 * $buildingMultiplier * $alignmentMultiplier * $jobCount));
    }

    private function getHammerProduction(Alignment $alignment): int
    {
        $jobCount = $this->city->getStorageCount('worker');
        $buildingMultiplier = CityManager::BUILDING_LEVEL_MULTIPLIER[$this->city->getBuildingLevel('workshop')];
        $alignmentMultiplier = CityManager::ALIGNMENT_LENGTH_MULTIPLIER[$alignment->getLength() - 3];

        return floor($buildingMultiplier * $alignmentMultiplier * $jobCount);
    }

    private function getConstructionSiteProduction(Alignment $alignment): int
    {
        $jobCount = $this->city->getStorageCount('worker');
        $alignmentMultiplier = CityManager::ALIGNMENT_LENGTH_MULTIPLIER[$alignment->getLength() - 3];
        $constructionSiteLevel = $this->city->getBuildingLevel('constructionSite');
        $buildingMultiplier = CityManager::BUILDING_LEVEL_MULTIPLIER[$this->city->getBuildingLevel('workshop')];

        return floor($alignmentMultiplier * $constructionSiteLevel * $buildingMultiplier * $jobCount);
    }

    private function getSoldierProduction(Alignment $alignment): int
    {
        if (0 === $this->city->getBuildingLevel('barracks') || !$this->city->isRecruiting()) {
            return 0;
        }

        $jobCount = $this->city->getStorageCount('recruiter');

        return floor($jobCount + $alignment->getLength() - 2);
    }

    private function getReplayProduction(Alignment $alignment): int
    {
        return $alignment->getLength() - 2;
    }

    /**
     * Add $cumulativeGains variable to stock resources.
     */
    private function collectAllGains(): void
    {
        $this->gameLogMgr->beginLogTransaction();
        $this->calculateMaxStorages($this->city);
        foreach ($this->cumulativeGains as $gainName => $gain) {
            // TODO Factorize & do a PostResourceChange with the switch
            switch ($gainName) {
                case 'wheat':
                case 'gold':
                case 'wood':
                    $this->collectResource($gainName, $gain);
                    break;
                case 'resourceFromHammer':
                    $this->collectResourceFromHammer($gain);
                    break;
                case 'hammer':
                    $this->collectHammer($gain);
                    break;
                case 'soldier':
                    $this->collectSoldier($gain);
                    break;
                case 'citizen':
                    $this->collectCitizen($gain);
                    break;
                case 'replay':
                    $this->collectReplay($gain);
                    break;
            }
            $this->gameLogMgr->finishLogTransaction();
        }
    }

    /**
     * Calculate true production amount and waste, log it and store.
     */
    private function collectResource(string $resource, int $gain): void
    {
        $storage = $this->findOrCreateStorage($resource);
        $maxPossible = $this->maxStorages[$resource] - $storage->getAmount();

        if ($gain > $maxPossible) {
            $production = $maxPossible;
            $waste = $gain - $maxPossible;
        } else {
            $production = $gain;
            $waste = 0;
        }

        $storage->setAmount(min($this->maxStorages[$resource], $storage->getAmount() + $production));

        if ($production > 0) {
            $this->addCityLog(LogEventType::CITY_RESOURCE_PRODUCTION, ['%production%' => $production, '%icon%' => $resource]);
        }
        if ($waste > 0) {
            $this->addCityLog(LogEventType::CITY_RESOURCE_WASTE, ['%waste%' => $waste, '%icon%' => $resource]);
        }
    }

    private function collectHammer(int $gain): void
    {
        $storage = $this->findOrCreateStorage('remainingHammers');
        $storage->setAmount($storage->getAmount() - $gain);

        if (($storage->getAmount() <= 0 || CheatConfiguration::CHEAT_INSTANT_CONSTRUCTION) && $this->city->getConstructionProject() instanceof Constructions) {
            $this->finishConstruction();
        }

        // TODO Find a way to order logs in a better way! Here we are forced to put this logic after to see the construction finished after the hammer progression log
        if ($this->city->getConstructionProject() instanceof Constructions && $gain > 0) {
            $this->addCityLog(LogEventType::CITY_HAMMER_PROGRESS, ['%amount%' => $gain]);
        } elseif (!$this->city->getConstructionProject() instanceof Constructions && 0 === $gain) {
            $this->addCityLog(LogEventType::CITY_HAMMER_NO_CONSTRUCTION, ['%amount%' => $gain]);
        }
    }

    private function collectResourceFromHammer(int $gain): void
    {
        $storage = $this->findOrCreateStorage('wheat');
        $storage->setAmount(min($this->maxStorages['wheat'], $storage->getAmount() + $gain));
        $storage = $this->findOrCreateStorage('gold');
        $storage->setAmount(min($this->maxStorages['gold'], $storage->getAmount() + $gain));

        $this->addCityLog(LogEventType::CITY_HAMMER_PRODUCTION, ['%amount%' => $gain]);
    }

    private function collectSoldier(int $gain): void
    {
        if (0 === $gain) {
            if (0 === $this->city->getBuildingLevel('barracks')) {
                $this->addCityLog(LogEventType::CITY_ARMY_NO_BARRACKS);
            } elseif (false === $this->city->isRecruiting()) {
                $this->addCityLog(LogEventType::CITY_ARMY_NO_RECRUITMENT);
            }
        } else {
            $unitArmy = $this->findOrCreateUnitArmy('soldier');
            $unitArmy->setAmount($unitArmy->getAmount() + $gain);
            $this->addCityLog(LogEventType::CITY_ARMY_PRODUCTION, ['%amount%' => $gain]);
        }
    }

    private function collectCitizen(int $gain): void
    {
        if ($gain < 0) {
            return;
        }

        $storage = $this->findOrCreateStorage('citizen');
        $storage->setAmount($storage->getAmount() + $gain);
        for ($i = 0; $i < $gain; ++$i) {
            $this->updateCityOnNewCitizen();
            $this->addCityLog(LogEventType::CITY_CITIZEN_BIRTH, ['%cityName%' => $this->city->getName()]);
        }
    }

    private function collectReplay(int $gain): void
    {
        if ($gain <= 0) {
            return;
        }

        $this->city->setRemainingTurns(min(100, $this->city->getRemainingTurns() + $gain));
        $this->addCityLog(LogEventType::CITY_REPLAY, ['%count%' => $gain]);
    }

    /**
     * Removes wheat consumption from wheat storage.
     */
    private function consumeFood(): int
    {
        $storage = $this->findOrCreateStorage('wheat');

        $consumption = $this->city->getPopulationCount();
        if (array_key_exists('citizen', $this->cumulativeGains)) {
            $consumption += $this->cumulativeGains['citizen'];
        }

        $storage->setAmount(max(0, $storage->getAmount() - $consumption));

        return $consumption;
    }

    /**
     * Removes gold consumption from gold storage.
     */
    private function consumeGold(): int
    {
        $armyCost = $this->city->getLord()->getTotalArmyCost();

        $storage = $this->findOrCreateStorage('gold');
        $storage->setAmount(max(0, $storage->getAmount() - $armyCost));

        // TODO
        // $this->addCityLog(LogEventType::CITY_ARMY_COST, ["%cost%" => $cost]);

        // TODO Also include commerce from all nodes

        return $armyCost;
    }

    /**
     * Grows the lord old by 1 month every 4 turns.
     */
    private function makeOld(): void
    {
        $lord = $this->city->getLord();
        $lord->setAgeMonth($lord->getAgeMonth() + 0.25);
        if ($lord->getAgeMonth() >= 12) {
            $lord->setAgeYear($lord->getAgeYear() + 1);
            $lord->setAgeMonth(0);
        }

        if (CheatConfiguration::CHEAT_FASTER_AGING) {
            $lord->setAgeYear($lord->getAgeYear() + 1);
        }

        $currentAgeInWeeks = $lord->getCurrentAgeInWeeks();
        $hasMinimumWeekToDegradeHealth = $currentAgeInWeeks > $lord->getAgeInWeeks(self::MINIMUM_AGE_TO_DECLINE);
        // dump('Has minimum age for health degradation : '.$hasMinimumWeekToDegradeHealth);
        $canRollForHealthDegradation = $hasMinimumWeekToDegradeHealth
            && $currentAgeInWeeks > ($lord->getLastWeekWithoutHealthIssue() + self::SAFE_WEEK_COUNT);
        if ($canRollForHealthDegradation || CheatConfiguration::CHEAT_IGNORE_HEALTH_DEGRADATION_COOLDOWN) {
            $maxAgeInWeeks = $lord->getAgeInWeeks(self::MAXIMUM_AGE);
            $randomMax = max(1, ($maxAgeInWeeks - $currentAgeInWeeks) >> 4);
            if (1 === random_int(1, $randomMax)) {
                $lord->setHealth(HealthType::getNextHealthType($lord->getHealth()));
                $lord->setLastWeekWithoutHealthIssue($currentAgeInWeeks);

                if (HealthType::TYPE_GOOD === $lord->getHealth()) {
                    $this->gameLogMgr->addKingdomLog($lord, LogEventType::MANAGEMENT_HEALTH_DEGRADATION_1, true);
                } elseif (HealthType::TYPE_BAD === $lord->getHealth()) {
                    $this->gameLogMgr->addKingdomLog($lord, LogEventType::MANAGEMENT_HEALTH_DEGRADATION_2, true);
                }
            }
        }
    }

    /**
     * Kill one random villager, except the last farmer.
     *
     * @throws \Exception
     */
    private function triggerStarvation(): void
    {
        $aliveCitizenTypes = [];
        foreach ($this->city->getStorages() as $storage) {
            if (ResourceType::TYPE_POPULATION === $storage->getResource()->getResourceType() && $storage->getAmount() > 0) {
                // Ignore last farmer!
                if (1 === $storage->getAmount() && 'farmer' === $storage->getResource()->getName()) {
                    continue;
                }
                $aliveCitizenTypes[] = $storage->getResource()->getName();
            }
        }

        if ([] === $aliveCitizenTypes) {
            return;
        }

        $starvationRand = 0;
        if (count($aliveCitizenTypes) > 1) {
            $starvationRand = random_int(0, count($aliveCitizenTypes) - 1);
        }

        $lostCitizenType = $aliveCitizenTypes[$starvationRand];
        $storage = $this->findOrCreateStorage($lostCitizenType);
        $storage->setAmount($storage->getAmount() - 1);
        $this->updateCityOnDeadCitizen();

        $storage = $this->findOrCreateStorage('wheat');
        $storage->setAmount($this->city->getPopulationCount());

        $this->addCityLog(LogEventType::CITY_CITIZEN_STARVATION, ['%jobName%' => $lostCitizenType]);
    }

    private function triggerBankrupt(): void
    {
        // TODO Lose one random unit after a warning ?
    }

    /**
     * Set the construction project to null, add the buildBuilding/Unit to DB.
     */
    private function finishConstruction(): void
    {
        $construction = $this->city->getConstructionProject();
        if (!$construction instanceof Constructions) {
            return;
        }

        if ('building' === $construction->getConstructionType()) {
            if ($this->city->getBuildingLevel($construction->getName()) > 0) {
                $building = $this->getBuiltBuilding($construction->getName());
                $building->setLevel($this->city->getProjectLevel());
            } else {
                $building = new BuiltBuildings($construction, $this->city->getProjectLevel());
                $this->city->addBuiltBuilding($building);
            }

            $this->city->setConstructionProject(null);
            $this->city->setProjectLevel(null);
        } elseif ('unit' === $construction->getConstructionType()) {
            $unitArmy = $this->findOrCreateUnitArmy($construction->getName());
            $unitArmy->setAmount($unitArmy->getAmount() + 1);
            $this->city->setConstructionProject(null);
            $this->city->setProjectLevel(null);
        }

        $translationKey = $this->getConstructionActionTranslationKey($construction, 'finished');
        // TODO Add the level as parameters here for building!
        $this->addCityLog($translationKey, ['%name%' => $construction->getName()]);
    }

    /**
     * Returns an array of ['construction' => $construction, 'level' => $level, 'buildable' => $buildable] arrays.
     */
    public function getDisplayedProjects(City $city): array
    {
        $constructionDict = $this->gameDataProvider->getConstructionDict();
        $requirementDict = $this->gameDataProvider->getConstructionRequirementDict();

        $displayedProjects = [];

        // TODO Clean that mess!
        /** @var Constructions $construction */
        foreach (array_values($constructionDict) as $construction) {
            if (!array_key_exists($construction->getName(), $requirementDict)) {
                throw new \LogicException(sprintf('The %s\'s project requirement doesn\'t exist', $construction->getName()));
            }

            if (ConstructionType::TYPE_BUILDING == $construction->getConstructionType()) {
                $currentBuildingLevel = $city->getBuildingLevel($construction->getName());
                $projectLevel = $currentBuildingLevel + 1;

                if ($projectLevel > 5) {
                    continue;
                }

                if (!array_key_exists($projectLevel, $requirementDict[$construction->getName()])) {
                    throw new \LogicException(sprintf('The %s\'s project requirement for level %d doesn\'t exist', $construction->getName(), $projectLevel));
                }

                $requirement = $requirementDict[$construction->getName()][$projectLevel];
            } else {
                $requirement = $requirementDict[$construction->getName()][1];
                $projectLevel = $city->getBuildingLevel($requirement['requiredConstruction']->value);
            }

            $requiredBuildingLevelOK = is_null($requirement['requiredConstruction'])
                || ($city->getBuildingLevel($requirement['requiredConstruction']->value) >= $requirement['requiredLevel']);
            if ($requiredBuildingLevelOK) {
                $buildable = $this->isABuildableConstruction($city, $construction, $projectLevel);
                $displayedProjects[] = [
                    'construction' => $construction,
                    'level' => $projectLevel,
                    'buildable' => $buildable,
                    'requirements' => $requirement,
                ];
            }
        }

        return $displayedProjects;
    }

    /**
     * Returns an array of boolean (buildable, 'title' => titleSatisfied, 'resourceName1' => enoughResource1, 'resourceName2' => enoughResource2...).
     *
     * @return bool[]
     */
    private function isABuildableConstruction(
        City $city,
        Constructions $construction,
        int $level,
    ): array {
        $buildable = [0 => true, 'title' => true];
        $requirementDict = $this->gameDataProvider->getConstructionRequirementDict();
        $currentTitle = $city->getLord()->getTitle();

        $requiredTitle = $requirementDict[$construction->getName()][$level]['requiredTitle'];
        if (null !== $requiredTitle) {
            $buildable['title'] = $currentTitle->index() >= $requiredTitle->index();
            $buildable[0] &= $buildable['title'];
        }

        foreach ($construction->getOrderedSchemas()[$level] as $schema) {
            $resourceName = $schema->getResource()->getName();
            $possessedAmount = $city->getStorageCount($resourceName);
            $requiredAmount = $construction->getOrderedSchemas()[$level][$resourceName]->getAmount();
            if ('remainingHammers' !== $resourceName) {
                $buildable[$resourceName] = $possessedAmount >= $requiredAmount;
                $buildable[0] &= $buildable[$resourceName];
            }
        }

        return $buildable;
    }

    /**
     * Consumes the required resources and start the construction project.
     */
    public function startConstruction(City $city, string $constructionName, int $level): bool
    {
        if ($city->getConstructionProject() instanceof Constructions || $city->getStorageCount('worker') <= 0) {
            return false;
        }

        /** @var Constructions $construction */
        $construction = $this->gameDataProvider->getConstructionDict()[$constructionName];
        $buildable = $this->isABuildableConstruction($city, $construction, $level)[0];
        if (!$buildable) {
            return false;
        }

        $city->setConstructionProject($construction);
        $city->setProjectLevel($level);

        foreach ($construction->getOrderedSchemas()[$level] as $schema) {
            $resourceName = $schema->getResource()->getName();
            $requiredAmount = $construction->getOrderedSchemas()[$level][$resourceName]->getAmount();
            if ('remainingHammers' === $resourceName) {
                $storage = $this->findOrCreateStorage('remainingHammers', $city);
                $storage->setAmount($requiredAmount);
            } elseif (in_array($resourceName, ['wheat', 'wood', 'gold', 'lin', 'iron', 'horse'])) {
                $storage = $this->findOrCreateStorage($resourceName, $city);
                if ($storage->getAmount() < $requiredAmount) {
                    return false;
                }

                $storage->setAmount($storage->getAmount() - $requiredAmount);
            } else {
                return false;
            }
        }

        $translationKey = $this->getConstructionActionTranslationKey($construction, 'started');
        $this->addAndFlushCityLog($city, $translationKey, ['%name%' => $construction->getName()]);

        return true;
    }

    /**
     * Stop the construction project and Give the required resources back.
     */
    public function cancelConstruction(City $city): bool
    {
        $construction = $city->getConstructionProject();
        if (!$construction instanceof Constructions) {
            return false;
        }

        $this->calculateMaxStorages($city);

        $level = $city->getProjectLevel();
        foreach ($construction->getOrderedSchemas()[$level] as $schema) {
            $resourceName = $schema->getResource()->getName();
            $requiredAmount = $construction->getOrderedSchemas()[$level][$resourceName]->getAmount();
            if (in_array($resourceName, ['wheat', 'wood', 'gold', 'lin', 'iron', 'horse'])) {
                $storage = $this->findOrCreateStorage($resourceName, $city);
                $storage->setAmount(min($this->maxStorages[$resourceName], $storage->getAmount() + $requiredAmount));
            } elseif ('remainingHammers' !== $resourceName) {
                return false;
            }
        }
        $city->setConstructionProject(null);

        $translationKey = $this->getConstructionActionTranslationKey($construction, 'cancelled');
        $this->addAndFlushCityLog($city, $translationKey, ['%name%' => $construction->getName()]);

        return true;
    }

    /**
     * Get the matching Storage Object if it exists, else create it with amount 0.
     */
    public function findOrCreateStorage(string $resourceName, ?City $city = null): Storage
    {
        if (!$city instanceof City) {
            $city = $this->city;
        }

        $storage = $city->findStorage($resourceName);
        if ($storage instanceof Storage) {
            return $storage;
        }

        $storage = $this->gameDataProvider->createStorage($resourceName, 0);
        $city->addStorage($storage);

        return $storage;
    }

    /**
     * Get the matching Construction Object if it has been built, else create one with level 0.
     */
    private function getBuiltBuilding(string $buildingName): ?BuiltBuildings
    {
        foreach ($this->city->getBuiltBuildings() as $building) {
            if ($building->getBuilding()->getName() === $buildingName) {
                return $building;
            }
        }
        $building = $this->gameDataProvider->createBuilding($buildingName, 0);
        $this->city->addBuiltBuilding($building);

        return $building;
    }

    /**
     * Calculates the current maxAmounts of each resources for the specified city.
     */
    private function calculateMaxStorages(City $city): void
    {
        $maxStorageDict = $this->gameDataProvider->getMaxStorageDict();
        $this->maxStorages = [
            'wheat' => $maxStorageDict['wheat'][$city->getBuildingLevel('attic')]->getMaxAmount(),
            'gold' => 9999,
            'wood' => $maxStorageDict['wood'][$city->getBuildingLevel('hut')]->getMaxAmount(),
            'iron' => $maxStorageDict['iron'][$city->getBuildingLevel('forge')]->getMaxAmount(),
            'horse' => $maxStorageDict['horse'][$city->getBuildingLevel('stable')]->getMaxAmount(),
            'lin' => $maxStorageDict['lin'][$city->getBuildingLevel('factory')]->getMaxAmount(),
        ];
    }

    public function calculateAndGetMaxStorages(City $city): array
    {
        $this->calculateMaxStorages($city);

        return $this->maxStorages;
    }

    /**
     * Returns an array filtered by resourceType (producedResourceName => [convertable, requiredResourceName => schemas object]) to be displayed in view.
     */
    public function getDisplayedConversionSchemas(City $city, string $filter): array
    {
        $conversionSchemaDict = $this->gameDataProvider->getConversionSchemaDict();
        $resourceDict = $this->gameDataProvider->getResourceDict();
        $displayedSchemas = [];
        foreach (array_keys($conversionSchemaDict) as $resourceName) {
            if ($resourceDict[$resourceName]->getResourceType() === $filter) {
                /* @var ResourceConversionSchemas $conversionSchema */
                $conversionSchema = $conversionSchemaDict[$resourceName];
                $buildingName = $conversionSchema['building'];

                for ($level = 5; $level > 0; --$level) {
                    foreach ($conversionSchema[$level] as $schema) {
                        if ($city->getBuildingLevel($buildingName) === $schema->getRequiredLevel()) {
                            if (!in_array($resourceName, array_keys($displayedSchemas))) {
                                $displayedSchemas[$resourceName] = [];
                            }
                            $displayedSchemas[$resourceName][0] = $this->canConvertResource($city, $resourceName, 1);
                            $displayedSchemas[$resourceName][$schema->getRequiredResource()->getName()] = $schema;
                        }
                    }
                }
            }
        }

        return $displayedSchemas;
    }

    /**
     * Add the produced resource and remove the required ones.
     */
    public function convertResources(City $city, string $producedResourceName, int $amount): bool
    {
        if ($amount > 0 && $this->canConvertResource($city, $producedResourceName, $amount)) {
            $this->city = $city;

            $conversionSchemaDict = $this->gameDataProvider->getConversionSchemaDict();
            $buildingName = $conversionSchemaDict[$producedResourceName]['building'];
            $schemas = $conversionSchemaDict[$producedResourceName][$city->getBuildingLevel($buildingName)];

            $resourceCostName = 'undefined';
            $resourceCostAmount = 0;
            foreach (array_keys($schemas) as $resourceName) {
                if ('resource' === $schemas[$resourceName]->getRequiredResource()->getResourceType()) {
                    $resourceCostName = $resourceName;
                    $storage = $this->findOrCreateStorage($resourceName, $city);
                    $resourceCostAmount = $amount * $schemas[$resourceName]->getRequiredAmount();
                    $storage->setAmount($storage->getAmount() - $resourceCostAmount);
                } elseif ('army' === $schemas[$resourceName]->getRequiredResource()->getResourceType()) {
                    $unitArmy = $this->findOrCreateUnitArmy($resourceName);
                    $resourceCostAmount = $amount * $schemas[$resourceName]->getRequiredAmount();
                    $unitArmy->setAmount($unitArmy->getAmount() - $resourceCostAmount);
                }
            }

            $key = array_keys($schemas)[0];
            if ('resource' === $schemas[$key]->getProducedResource()->getResourceType()) {
                $storage = $this->findOrCreateStorage($producedResourceName);
                $producedAmount = $amount * $schemas[$key]->getProducedAmount();
                $storage->setAmount($storage->getAmount() + $producedAmount);

                $this->addAndFlushCityLog(
                    $city,
                    LogEventType::CITY_RESOURCE_CONVERSION,
                    ['%inAmount%' => $resourceCostAmount, '%inIcon%' => $resourceCostName, '%outAmount%' => $producedAmount, '%outIcon%' => $producedResourceName]
                );
            } elseif ('army' === $schemas[$key]->getProducedResource()->getResourceType()) {
                $unitArmy = $this->findOrCreateUnitArmy($producedResourceName);
                $producedAmount = $amount * $schemas[$key]->getProducedAmount();
                $unitArmy->setAmount($unitArmy->getAmount() + $producedAmount);

                $this->addAndFlushCityLog(
                    $city,
                    LogEventType::CITY_ARMY_CONVERSION,
                    ['%soldierAmount%' => $amount, '%outAmount%' => $producedAmount, '%outIcon%' => $producedResourceName]
                );
            }

            return true;
        }

        return false;
    }

    /**
     * Returns true if the city has enough of the required resources to convert it into the specified one.
     */
    private function canConvertResource(City $city, string $resourceToProduce, int $amount): bool
    {
        $isConvertable = true;
        $conversionSchemaDict = $this->gameDataProvider->getConversionSchemaDict();
        $buildingName = $conversionSchemaDict[$resourceToProduce]['building'];
        if ($city->getBuildingLevel($buildingName) <= 0) {
            return false;
        }

        $schemas = $conversionSchemaDict[$resourceToProduce][$city->getBuildingLevel($buildingName)];
        foreach (array_keys($schemas) as $resourceName) {
            if ('resource' === $schemas[$resourceName]->getProducedResource()->getResourceType()) {
                $isConvertable &= ($city->getStorageCount($resourceName) >= $amount * $schemas[$resourceName]->getRequiredAmount());
                $this->calculateMaxStorages($city);
                $isConvertable &= ($city->getStorageCount($resourceToProduce) <= $this->maxStorages[$resourceToProduce] - $amount * $schemas[$resourceName]->getProducedAmount());
            } elseif ('army' === $schemas[$resourceName]->getProducedResource()->getResourceType()) {
                if ('resource' === $schemas[$resourceName]->getRequiredResource()->getResourceType()) {
                    $isConvertable &= ($city->getStorageCount($resourceName) >= $amount * $schemas[$resourceName]->getRequiredAmount());
                } elseif ('army' === $schemas[$resourceName]->getRequiredResource()->getResourceType()) {
                    $isConvertable &= ($city->getUnitCount($resourceName) >= $amount * $schemas[$resourceName]->getRequiredAmount());
                }
            }
        }

        return $isConvertable;
    }

    /**
     * Recruit a new general and apply its cost.
     */
    public function recruitGeneral(City $city, string $name): bool
    {
        $generalCount = $city->getLord()->getGeneralCount();
        $goldOK = $city->getStorageCount('gold') >= (50 * 2 ** $generalCount);
        $numberOfGeneralsOK = $generalCount < $city->getBuildingLevel('headquarters');

        if ($numberOfGeneralsOK && $goldOK) {
            $storage = $this->findOrCreateStorage('gold', $city);
            $storage->setAmount($storage->getAmount() - 50 * 2 ** count($city->getLord()->getGenerals()));
            $general = new General($name, $city->getNode());
            $city->getLord()->addGeneral($general);

            $this->addAndFlushCityLog($city, LogEventType::CITY_GENERAL_RECRUITMENT);

            return true;
        }

        return false;
    }

    /**
     * Switch on/off recruitment.
     */
    public function toggleRecruitment(City $city): void
    {
        $city->setRecruiting(!$city->isRecruiting());
    }

    /**
     * Try to get the specified unit entity or create if not found.
     */
    private function findOrCreateUnitArmy(string $unitName): UnitArmy
    {
        $cityArmy = $this->city->getDefense();
        foreach ($cityArmy->getUnits() as $army) {
            if ($unitName === $army->getResource()->getName()) {
                return $army;
            }
        }

        $army = $this->gameDataProvider->createArmyUnit($unitName, 0);
        $cityArmy->addUnit($army);

        return $army;
    }

    /**
     * Returns a translation key with the specified building and the specified action.
     */
    private function getConstructionActionTranslationKey(Constructions $construction, string $action): string
    {
        return sprintf('city.construction.%s.%s', $construction->getConstructionType(), $action);
    }

    /**
     * This function is just a wrapper to simplify the use of the LogManager function!
     *
     * @param string $logEvent ideally LogEventType
     */
    private function addCityLog(string $logEvent, array $params = []): void
    {
        $this->gameLogMgr->addCityLog($this->city, $logEvent, false, $params);
    }

    /**
     * This function is just a wrapper to simplify the use of the LogManager function!
     *
     * @param string $logEvent ideally LogEventType
     */
    private function addAndFlushCityLog(City $city, string $logEvent, array $params = []): void
    {
        $this->gameLogMgr->addCityLog($city, $logEvent, true, $params);
    }

    // City house/dungeon evolution

    /**
     * This function return a random number of houses to add when population increase.
     *
     * @param int $nextPopulationCount the new number of citizen in the city
     */
    private function getNbHouses(int $nextPopulationCount): int
    {
        $deltaTowerStep = CityManager::TOWER_STEPS[$this->city->getDungeonLevel()] - CityManager::TOWER_STEPS[$this->city->getDungeonLevel() - 1];
        $nbDicesPerCitizen = CityManager::TOTAL_NB_DICES / $deltaTowerStep;
        $outOfTowerCitizens = $nextPopulationCount - CityManager::TOWER_STEPS[$this->city->getDungeonLevel() - 1];
        $nbDices = floor($nbDicesPerCitizen * $outOfTowerCitizens) - floor($nbDicesPerCitizen * ($outOfTowerCitizens - 1));

        $nbHouses = 0;
        for ($i = 0; $i < $nbDices; ++$i) {
            $nbHouses += random_int(0, mt_getrandmax()) % 2;
        }

        return $nbHouses;
    }

    /**
     * Update dungeon level and number of houses when a new citizen is born.
     */
    private function updateCityOnNewCitizen(): void
    {
        if ($this->city->getPopulationCount() >= CityManager::TOWER_STEPS[$this->city->getDungeonLevel()]) {
            $this->city->increaseDungeonLevel();
        } else {
            $this->city->addHouses($this->getNbHouses($this->city->getPopulationCount()));
        }
    }

    /**
     * Update dungeon level and number of houses when a citizen is died.
     */
    private function updateCityOnDeadCitizen(): void
    {
        if ($this->city->getPopulationCount() < CityManager::TOWER_STEPS[$this->city->getDungeonLevel() - 1]) {
            $this->city->decreaseDungeonLevel();
            $deltaTowerStep = CityManager::TOWER_STEPS[$this->city->getDungeonLevel()] - CityManager::TOWER_STEPS[$this->city->getDungeonLevel() - 1];
            $nbHouses = 0;
            for ($i = 1; $i < $deltaTowerStep; ++$i) {
                $nbHouses += $this->getNbHouses(CityManager::TOWER_STEPS[$this->city->getDungeonLevel() - 1] + $i);
            }
            $this->city->addHouses($nbHouses);
        } else {
            $this->city->addHouses(-$this->getNbHouses($this->city->getPopulationCount()));
        }
    }

    /**
     * Log how much resource the city obtained when General harvested.
     */
    public function logNodeHarvest(City $city, General $general, ResourceSpot $resourceSpot, int $collectedAmount): void
    {
        $this->gameLogMgr->addCityLog(
            $city,
            LogEventType::CITY_GENERAL_HARVEST,
            true,
            [
                '%worldId%' => $resourceSpot->getNode()->getWorld()->getId(),
                '%nodeId%' => $resourceSpot->getNode()->getId(),
                '%nodeName%' => $resourceSpot->getNode()->getName(),
                '%generalName%' => $general->getName(),
                '%amount%' => $collectedAmount,
                '%icon%' => $resourceSpot->getResource()->getName(),
            ]
        );
    }
}
