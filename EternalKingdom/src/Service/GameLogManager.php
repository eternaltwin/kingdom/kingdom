<?php

namespace App\Service;

use App\Entity\Battle\Battle;
use App\Entity\City;
use App\Entity\LogSystem\BattleLog;
use App\Entity\LogSystem\CityLog;
use App\Entity\LogSystem\KingdomLog;
use App\Entity\LogSystem\Log;
use App\Entity\LogSystem\WorldLog;
use App\Entity\Lord;
use App\Entity\World\WorldMap;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * GameLogManager aims to collect logs and store them to DB.
 */
class GameLogManager
{
    private ArrayCollection $pendingLogs;

    public function __construct(private readonly GameDataProvider $gameDataProvider, private readonly EntityManagerInterface $entityManager)
    {
        $this->gameDataProvider->loadDicts();
    }

    /**
     * @param bool $instantFlush if set to true, it will persist and flush the log into DB right now
     *                           Else use the function beginLogTransaction & endLogTransaction
     */
    public function addCityLog(City $city, string $logEventType, bool $instantFlush, array $params = []): void
    {
        $logEvent = $this->gameDataProvider->getLogEventDict()[$logEventType];
        $log = new CityLog($city->getLord(), $logEvent, $params);
        $this->addLog($log, $instantFlush);
    }

    /**
     * @param bool $instantFlush if set to true, it will persist and flush the log into DB right now
     *                           Else use the function beginLogTransaction & endLogTransaction
     */
    public function addKingdomLog(Lord $lord, string $logEventType, bool $instantFlush, array $params = []): void
    {
        $logEvent = $this->gameDataProvider->getLogEventDict()[$logEventType];
        $log = new KingdomLog($lord, $logEvent, $params);
        $this->addLog($log, $instantFlush);
    }

    /**
     * @param bool $instantFlush if set to true, it will persist and flush the log into DB right now
     *                           Else use the function beginLogTransaction & endLogTransaction
     */
    public function addWorldLog(WorldMap $world, string $logEventType, bool $instantFlush, array $params = []): void
    {
        $logEvent = $this->gameDataProvider->getLogEventDict()[$logEventType];
        $log = new WorldLog($world, $logEvent, $params);
        $this->addLog($log, $instantFlush);
    }

    /**
     * @param bool $instantFlush if set to true, it will persist and flush the log into DB right now
     *                           Else use the function beginLogTransaction & endLogTransaction
     */
    public function addBattleLog(Battle $battle, string $logEventType, bool $instantFlush, array $params = []): void
    {
        $logEvent = $this->gameDataProvider->getLogEventDict()[$logEventType];
        $log = new BattleLog($battle, $logEvent, $params);
        $this->addLog($log, $instantFlush);
    }

    /**
     * Call that when you want to start collecting game logs.
     */
    public function beginLogTransaction(): void
    {
        $this->pendingLogs = new ArrayCollection();
    }

    private function addLog(Log $log, bool $instantFlush): void
    {
        if ($instantFlush) {
            $this->beginLogTransaction();
        }

        assert(isset($this->pendingLogs), 'Verify you added a BeginLogTransaction before your first call to a log without instant Flush!');
        $this->pendingLogs->add($log);

        if ($instantFlush) {
            $this->finishLogTransaction();
        }
    }

    /**
     * Call that when you want to flush all collected game logs.
     */
    public function finishLogTransaction(): void
    {
        if ($this->pendingLogs->count() > 0) {
            for ($i = $this->pendingLogs->count() - 1; $i >= 0; --$i) {
                $this->entityManager->persist($this->pendingLogs->get($i));
            }

            // TODO Should we move the flush outside to get more control when we flush
            $this->entityManager->flush();
        }
    }
}
