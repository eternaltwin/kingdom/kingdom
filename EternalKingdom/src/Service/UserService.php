<?php

namespace App\Service;

use App\Controller\DefaultController;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class UserService implements UserServiceInterface
{
    // Delay before considering a User as inactive (in days).
    public const int K_INACTIVITY_DELAY = 7;

    // Delay before showing the news again at reconnection (in seconds).
    public const int K_SHOW_NEWS_COOLDOWN = 3600 * 24 * 7;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly UserRepository $repository,
        private readonly FlashBagInterface $flashBag)
    {
    }

    #[\Override]
    public function save(User $user): User
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    #[\Override]
    public function findUserByUserId(string $userId): ?User
    {
        return $this->repository->findOneByUserId($userId);
    }

    #[\Override]
    public function createUser(string $userId, string $displayName, bool $isAdmin = false): User
    {
        $user = new User($userId, $displayName);
        if ($isAdmin) {
            $user->setRoles([User::ROLE_ADMIN]);
        }
        $this->save($user);

        return $user;
    }

    #[\Override]
    public function reconnect(User $user): void
    {
        $this->verifyInactivityStatus();

        // TODO Update : Last News date loaded from DB
        $lastNewsDate = new \DateTime('2024-06-01');
        $delta = max(0, $lastNewsDate->getTimestamp() - $user->getLastConnection()->getTimestamp());
        if ($delta > 0) {
            $this->flashBag->add(DefaultController::K_FLASH_REDIRECTION, DefaultController::K_FLASH_SHOWNEWS);
        }

        $user->setLastConnectionDate(new \DateTime());
        $this->save($user);
    }

    /**
     * Verify Inactivity for all players.
     */
    private function verifyInactivityStatus(): void
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->entityManager->getRepository(User::class);
        $userRepository->updatePlayersInactivityStatus(UserService::K_INACTIVITY_DELAY);
    }
}
