<?php

namespace App\Service\GameData;

use Doctrine\ORM\EntityRepository;

class EKDataHelpers
{
    public static function findEntity(EntityRepository $repository, array &$array, string $name)
    {
        if (array_key_exists($name, $array)) {
            $resource = $array[$name];
        } else {
            $resource = $repository->findOneBy(['name' => $name]);
            $array[$name] = $resource;
        }

        return $resource;
    }
}
