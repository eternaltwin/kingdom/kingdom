<?php

namespace App\Service\GameData;

use App\Entity\Resources;
use App\Enum\ResourceType;
use App\Repository\ResourcesRepository;
use Doctrine\Persistence\ObjectManager;

/**
 * Describe all the Resources that we can have in the game.
 */
class EKResourcesLoader extends GameDataLoader
{
    private array $resourcesData = [
        // Resources
        ['name' => 'wheat',             'type' => ResourceType::TYPE_RESOURCE],
        ['name' => 'gold',              'type' => ResourceType::TYPE_RESOURCE],
        ['name' => 'wood',              'type' => ResourceType::TYPE_RESOURCE],
        ['name' => 'lin',               'type' => ResourceType::TYPE_RESOURCE],
        ['name' => 'iron',              'type' => ResourceType::TYPE_RESOURCE],
        ['name' => 'horse',             'type' => ResourceType::TYPE_RESOURCE],
        ['name' => 'remainingHammers',  'type' => ResourceType::TYPE_RESOURCE],

        // Population
        ['name' => 'farmer',            'type' => ResourceType::TYPE_POPULATION],
        ['name' => 'lumberjack',        'type' => ResourceType::TYPE_POPULATION],
        ['name' => 'merchant',          'type' => ResourceType::TYPE_POPULATION],
        ['name' => 'worker',            'type' => ResourceType::TYPE_POPULATION],
        ['name' => 'recruiter',         'type' => ResourceType::TYPE_POPULATION],
        ['name' => 'citizen',           'type' => ResourceType::TYPE_POPULATION],

        // Army
        ['name' => 'soldier',           'type' => ResourceType::TYPE_ARMY],
        ['name' => 'pikeman',           'type' => ResourceType::TYPE_ARMY],
        ['name' => 'archer',            'type' => ResourceType::TYPE_ARMY],
        ['name' => 'horseman',          'type' => ResourceType::TYPE_ARMY],
        ['name' => 'knight',            'type' => ResourceType::TYPE_ARMY],
        ['name' => 'mountedArcher',     'type' => ResourceType::TYPE_ARMY],
        ['name' => 'paladin',           'type' => ResourceType::TYPE_ARMY],
        ['name' => 'catapult',          'type' => ResourceType::TYPE_ARMY],
        ['name' => 'ballista',          'type' => ResourceType::TYPE_ARMY],
    ];

    #[\Override]
    public function loadGameData(ObjectManager $manager): void
    {
        /** @var ResourcesRepository $repo */
        $repo = $manager->getRepository(Resources::class);
        foreach ($this->resourcesData as $resourceData) {
            $entity = $repo->findOneBy(['name' => $resourceData['name']]);
            if (null == $entity) {
                $entity = new Resources();
                $entity->setName($resourceData['name']);
            }

            $entity->setResourceType($resourceData['type']);
            $manager->persist($entity);
        }
        $manager->flush();
    }
}
