<?php

namespace App\Service\GameData;

use App\Entity\Constructions;
use App\Entity\ResourceConversionSchemas;
use App\Entity\Resources;
use Doctrine\Persistence\ObjectManager;

class EKResourceConversionSchemas extends GameDataLoader
{
    private array $conversionSchemas = [];
    private array $resources = [];
    private array $constructions = [];

    #[\Override]
    public function loadGameData(ObjectManager $manager): void
    {
        $this->fillData();

        $resourceConversionSchemasRepo = $manager->getRepository(ResourceConversionSchemas::class);
        $constructionRepo = $manager->getRepository(Constructions::class);
        $resourcesRepo = $manager->getRepository(Resources::class);

        foreach ($this->conversionSchemas as $schemaData) {
            $construction = EKDataHelpers::findEntity($constructionRepo, $this->constructions, $schemaData['requiredBuilding']);
            $requiredResource = EKDataHelpers::findEntity($resourcesRepo, $this->resources, $schemaData['requiredResource']);
            $producedResource = EKDataHelpers::findEntity($resourcesRepo, $this->resources, $schemaData['producedResource']);
            $entity = $resourceConversionSchemasRepo->findOneBy([
                'requiredBuilding' => $construction,
                'requiredLevel' => $schemaData['level'],
                'requiredResource' => $requiredResource,
                'producedResource' => $producedResource,
            ]);

            if (null == $entity) {
                $entity = new ResourceConversionSchemas();
                $entity->setRequiredBuilding($construction)
                    ->setRequiredLevel($schemaData['level'])
                    ->setProducedResource($producedResource);
            }

            $entity->setRequiredResource($requiredResource);
            $entity->setRequiredAmount($schemaData['requiredAmount']);
            $entity->setproducedAmount($schemaData['producedAmount']);

            $manager->persist($entity);
        }
        $manager->flush();
    }

    /**
     * Determines conversion schemas from level.
     */
    private function fillData(): void
    {
        $cauldronEfficiency = [100, 80, 70, 60, 50];
        $factoryButcherEfficiency = [5, 7, 10, 15, 20];
        for ($level = 1; $level <= 5; ++$level) {
            // Resource conversion
            $factorySchema = [
                'requiredBuilding' => 'factory',
                'level' => $level,
                'requiredResource' => 'lin',
                'requiredAmount' => 1,
                'producedResource' => 'gold',
                'producedAmount' => $factoryButcherEfficiency[$level - 1],
            ];

            $butcherSchema = [
                'requiredBuilding' => 'butcher',
                'level' => $level,
                'requiredResource' => 'horse',
                'requiredAmount' => 1,
                'producedResource' => 'wheat',
                'producedAmount' => $factoryButcherEfficiency[$level - 1],
            ];

            $cauldronSchema = [
                'requiredBuilding' => 'cauldron',
                'level' => $level,
                'requiredResource' => 'wood',
                'requiredAmount' => $cauldronEfficiency[$level - 1],
                'producedResource' => 'iron',
                'producedAmount' => 1,
            ];

            // Unit conversion
            $archerSchema1 = ['requiredBuilding' => 'archery', 'level' => $level, 'requiredResource' => 'soldier', 'requiredAmount' => 1,
                              'producedResource' => 'archer', 'producedAmount' => 1];
            $archerSchema2 = ['requiredBuilding' => 'archery', 'level' => $level, 'requiredResource' => 'lin', 'requiredAmount' => 7 - $level,
                              'producedResource' => 'archer', 'producedAmount' => 1];

            $pikemanSchema1 = ['requiredBuilding' => 'forge', 'level' => $level, 'requiredResource' => 'soldier', 'requiredAmount' => 1,
                               'producedResource' => 'pikeman', 'producedAmount' => 1];
            $pikemanSchema2 = ['requiredBuilding' => 'forge', 'level' => $level, 'requiredResource' => 'iron', 'requiredAmount' => 7 - $level,
                               'producedResource' => 'pikeman', 'producedAmount' => 1];

            $horsemanSchema1 = ['requiredBuilding' => 'stable', 'level' => $level, 'requiredResource' => 'soldier', 'requiredAmount' => 1,
                                'producedResource' => 'horseman', 'producedAmount' => 1];
            $horsemanSchema2 = ['requiredBuilding' => 'stable', 'level' => $level, 'requiredResource' => 'horse', 'requiredAmount' => 7 - $level,
                                'producedResource' => 'horseman', 'producedAmount' => 1];

            $knightSchema1 = ['requiredBuilding' => 'militaryAcademy', 'level' => $level, 'requiredResource' => 'soldier', 'requiredAmount' => 1,
                              'producedResource' => 'knight', 'producedAmount' => 1];
            $knightSchema2 = ['requiredBuilding' => 'militaryAcademy', 'level' => $level, 'requiredResource' => 'iron', 'requiredAmount' => 7 - $level,
                              'producedResource' => 'knight', 'producedAmount' => 1];
            $knightSchema3 = ['requiredBuilding' => 'militaryAcademy', 'level' => $level, 'requiredResource' => 'horse', 'requiredAmount' => 7 - $level,
                              'producedResource' => 'knight', 'producedAmount' => 1];

            $mArcherSchema1 = ['requiredBuilding' => 'militaryAcademy', 'level' => $level, 'requiredResource' => 'soldier', 'requiredAmount' => 1,
                               'producedResource' => 'mountedArcher', 'producedAmount' => 1];
            $mArcherSchema2 = ['requiredBuilding' => 'militaryAcademy', 'level' => $level, 'requiredResource' => 'lin', 'requiredAmount' => 7 - $level,
                               'producedResource' => 'mountedArcher', 'producedAmount' => 1];
            $mArcherSchema3 = ['requiredBuilding' => 'militaryAcademy', 'level' => $level, 'requiredResource' => 'horse', 'requiredAmount' => 7 - $level,
                               'producedResource' => 'mountedArcher', 'producedAmount' => 1];

            $paladinSchema1 = ['requiredBuilding' => 'guardTower', 'level' => $level, 'requiredResource' => 'soldier', 'requiredAmount' => 1,
                               'producedResource' => 'paladin', 'producedAmount' => 1];
            $paladinSchema2 = ['requiredBuilding' => 'guardTower', 'level' => $level, 'requiredResource' => 'iron', 'requiredAmount' => 2 * (7 - $level),
                               'producedResource' => 'paladin', 'producedAmount' => 1];
            $this->conversionSchemas[] = $factorySchema;
            $this->conversionSchemas[] = $butcherSchema;
            $this->conversionSchemas[] = $cauldronSchema;
            $this->conversionSchemas[] = $archerSchema1;
            $this->conversionSchemas[] = $archerSchema2;
            $this->conversionSchemas[] = $pikemanSchema1;
            $this->conversionSchemas[] = $pikemanSchema2;
            $this->conversionSchemas[] = $horsemanSchema1;
            $this->conversionSchemas[] = $horsemanSchema2;
            $this->conversionSchemas[] = $knightSchema1;
            $this->conversionSchemas[] = $knightSchema2;
            $this->conversionSchemas[] = $knightSchema3;
            $this->conversionSchemas[] = $mArcherSchema1;
            $this->conversionSchemas[] = $mArcherSchema2;
            $this->conversionSchemas[] = $mArcherSchema3;
            $this->conversionSchemas[] = $paladinSchema1;
            $this->conversionSchemas[] = $paladinSchema2;
        }
    }
}
