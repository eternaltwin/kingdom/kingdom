<?php

namespace App\Service\GameData;

use App\Entity\Constructions;
use App\Enum\ConstructionType;
use App\Repository\ConstructionsRepository;
use Doctrine\Persistence\ObjectManager;

class EKConstructionsLoader extends GameDataLoader
{
    private array $constructions = [
        // Buildings
        ['name' => 'palace', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'farm', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'attic', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'market', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'hut', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'constructionSite', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'barracks', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'wall', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'headquarters', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'workshop', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'guardTower', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'militaryAcademy', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'archery', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'factory', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'stable', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'butcher', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'forge', 'type' => ConstructionType::TYPE_BUILDING],
        ['name' => 'cauldron', 'type' => ConstructionType::TYPE_BUILDING],

        // Units
        ['name' => 'catapult', 'type' => ConstructionType::TYPE_UNIT],
        ['name' => 'ballista', 'type' => ConstructionType::TYPE_UNIT],
    ];

    #[\Override]
    public function loadGameData(ObjectManager $manager): void
    {
        /** @var ConstructionsRepository $repo */
        $repo = $manager->getRepository(Constructions::class);

        foreach ($this->constructions as $constructionData) {
            $entity = $repo->findOneBy(['name' => $constructionData['name']]);
            if (null == $entity) {
                $entity = new Constructions();
                $entity->setName($constructionData['name']);
            }

            $entity->setConstructionType($constructionData['type']);
            $manager->persist($entity);
        }
        $manager->flush();
    }
}
