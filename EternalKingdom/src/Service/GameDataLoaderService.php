<?php

namespace App\Service;

use App\Service\GameData\EKConstructionSchemasLoader;
use App\Service\GameData\EKConstructionsLoader;
use App\Service\GameData\EKLogsLoader;
use App\Service\GameData\EKMaxStoragesLoader;
use App\Service\GameData\EKResourceConversionSchemas;
use App\Service\GameData\EKResourcesLoader;
use App\Service\GameData\GameDataLoader;
use Doctrine\Persistence\ObjectManager;

class GameDataLoaderService
{
    public array $loaders = [];

    // TODO add EKWorldData + function loadWorldData?

    public function __construct()
    {
        // No dependencies
        $this->addLoader(new EKResourcesLoader());
        $this->addLoader(new EKLogsLoader());
        $this->addLoader(new EKConstructionsLoader());

        // Depends on previous GameData
        $this->addLoader(new EKMaxStoragesLoader());
        $this->addLoader(new EKConstructionSchemasLoader());
        $this->addLoader(new EKResourceConversionSchemas());
    }

    public function loadAllGameData(ObjectManager $manager): void
    {
        // TODO When tweaking values, find a way to tell to this what changed!
        // TODO Add a logic that tell if the game data needs to be patch!

        foreach ($this->loaders as $gameDataLoader) {
            $gameDataLoader->loadGameData($manager);
        }
    }

    private function addLoader(GameDataLoader $gameDataLoader): void
    {
        $this->loaders[] = $gameDataLoader;
    }
}
