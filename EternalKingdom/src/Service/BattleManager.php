<?php

namespace App\Service;

use App\Entity\Battle\Battle;
use App\Entity\Battle\BattleUnitStat;
use App\Entity\Battle\EngagedArmy;
use App\Entity\Battle\FightSurvivor;
use App\Entity\City;
use App\Entity\General;
use App\Entity\Lord;
use App\Entity\World\WorldMap;
use App\Entity\World\WorldMapNode;
use App\Enum\LogEventType;
use App\Kingdom\Battle\Service\RefillFights;
use App\Kingdom\Battle\Service\TickPendingFight;
use App\Kingdom\CheatSystem\CheatConfiguration;
use App\Kingdom\Lord\Service\UpdateGloryAndTitle;
use App\Kingdom\Ruleset\CoreRuleset\CoreRuleset;
use App\Kingdom\Ruleset\Service\GetRuleset;
use App\Repository\Battle\BattleRepository;
use App\Repository\Battle\FightSurvivorRepository;
use App\Repository\CityRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class BattleManager
{
    public const bool SETTING_CAN_CAPTURE_CAPITAL_CITY_OWNED_BY_PLAYER = false;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly GameLogManager $gameLogManager,
        private readonly GetRuleset $getRuleset,
        private readonly UpdateGloryAndTitle $updateGloryAndTitle,
        private readonly RefillFights $refillFightsService,
        private readonly TickPendingFight $tickPendingFight,
        private readonly TranslatorInterface $translator
    ) {
    }

    public function canClaimTerritory(General $attackingGeneral, string &$failReason): bool
    {
        $location = $attackingGeneral->getLocation();
        if (false === $this::SETTING_CAN_CAPTURE_CAPITAL_CITY_OWNED_BY_PLAYER && $location->getData()->isSpawnPoint() && null !== $location->getOwner()) {
            $failReason = $this->translator->trans('map.actions.attack.capitalCityClaimDisabled', [], 'Notification');

            return false;
        }

        // TODO Can we attack our own sovereign? -> $owningLord->getSpawnNode()->getSovereign()

        return true;
    }

    /**
     * Claims the current location where the specified General is!
     */
    public function claimTerritory(General $general, bool $wasDefendedWhenConquered): void
    {
        $lord = $general->getOwner();
        if (null === $lord) {
            throw new \LogicException('General must have an owner');
        }
        $location = $general->getLocation();
        $instigator = $general->getOwner();

        $previousSovereign = $location->getSovereign();
        $wasPreviouslyOwned = null !== $previousSovereign;

        $this->gameLogManager->beginLogTransaction();

        if ($wasPreviouslyOwned) {
            $previousSovereign->removeTerritory($location);
            $this->updateGloryAndTitle->exec($previousSovereign);
            $this->entityManager->persist($previousSovereign);

            // Previous Owner Log
            $this->gameLogManager->addKingdomLog(
                $previousSovereign,
                LogEventType::MANAGEMENT_TERRITORY_GOT_STOLEN,
                true,
                [
                    '%worldId%' => $lord->getWorldMap()->getId(),
                    '%nodeId%' => $location->getId(),
                    '%nodeName%' => $location->getName(),
                    '%userId%' => $instigator->getUser()->getId(),
                    '%lord%' => $instigator->getName(),
                    '%title%' => $instigator->getTitle(),
                ]
            );
        }

        // Instigator Log
        if ($wasPreviouslyOwned) {
            $this->gameLogManager->addKingdomLog(
                $lord,
                LogEventType::MANAGEMENT_CONQUEST_WITH_BATTLE,
                false,
                [
                    '%worldId%' => $lord->getWorldMap()->getId(),
                    '%nodeId%' => $location->getId(),
                    '%nodeName%' => $location->getName(),
                    '%userId%' => $previousSovereign->getUser()->getId(),
                    '%lord%' => $previousSovereign->getName(),
                    '%title%' => $previousSovereign->getTitle(),
                ]
            );
        } else {
            $this->gameLogManager->addKingdomLog(
                $lord,
                LogEventType::MANAGEMENT_CONQUEST_NO_RESISTANCE,
                false,
                [
                    '%worldId%' => $lord->getWorldMap()->getId(),
                    '%nodeId%' => $location->getId(),
                    '%nodeName%' => $location->getName(),
                ]
            );
        }

        // WorldMap Log
        if ($location->getData()->isSpawnPoint()) {
            $this->gameLogManager->addWorldLog(
                $lord->getWorldMap(),
                $wasDefendedWhenConquered ? LogEventType::WORLD_CONQUEST_WITH_BATTLE : LogEventType::WORLD_CONQUEST_NO_RESISTANCE,
                false,
                [
                    '%userId%' => $lord->getUser()->getId(),
                    '%lord%' => $lord->getName(),
                    '%generalName%' => $general->getName(),
                    '%worldId%' => $lord->getWorldMap()->getId(),
                    '%nodeId%' => $location->getId(),
                    '%nodeName%' => $location->getName(),
                    '%title%' => $lord->getTitle(),
                ]
            );
        }

        $this->gameLogManager->finishLogTransaction();

        // Clear all Generals that were still fortifying
        $generalRepo = $this->entityManager->getRepository(General::class);
        $nearbyGenerals = $generalRepo->findBy(['location' => $location]);
        foreach ($nearbyGenerals as $general) {
            if ($general->isFortifying()) {
                $general->setFortifying(false);
                $this->entityManager->persist($general);
            }
        }

        $lord->addTerritory($location);
        $this->updateGloryAndTitle->exec($lord);

        $this->entityManager->flush();
    }

    public function isLocationInWar(WorldMapNode $location): bool
    {
        $hasFoundBattleHere = false;
        foreach ($location->getGenerals() as $general) {
            if (null !== $general->getBattle()) {
                $hasFoundBattleHere = true;
                break;
            }
        }

        return $hasFoundBattleHere;
    }

    /**
     * Returns true if a Battle should be triggered from the specified location if a General attacks it.
     */
    public function shouldTriggerBattle(WorldMapNode $location): bool
    {
        $hasAnyDefense = false;
        if ($location->getSovereign() instanceof Lord) {
            if ($location->getData()->isSpawnPoint()) {
                /** @var CityRepository $cityRepo */
                $cityRepo = $this->entityManager->getRepository(City::class);
                if ($city = $cityRepo->findOneBy(['node' => $location])) {
                    $hasAnyDefense = $city->getDefense()->hasAnyUnits();
                }
            }

            if (!$hasAnyDefense) {
                $hasAnyDefense = $location->getArmyContainer()->hasAnyUnits();
            }

            if (!$hasAnyDefense) {
                $hasDefendingGeneral = false;
                foreach ($location->getGenerals() as $general) {
                    if ($general->getOwner() === $location->getSovereign() && $general->isFortifying() && $general->hasAnArmy()) {
                        $hasDefendingGeneral = true;
                        break;
                    }
                }
                $hasAnyDefense = $hasDefendingGeneral;
            }
        }

        return $hasAnyDefense;
    }

    /**
     * Start a battle on the current location of the specified General!
     */
    public function startBattle(General $instigatingGeneral, string &$failReason): void
    {
        assert($instigatingGeneral->getArmyContainer()->hasAnyUnits());

        $battle = null;
        $location = $instigatingGeneral->getLocation();

        /** @var BattleRepository $battleRepo */
        $battleRepo = $this->entityManager->getRepository(Battle::class);
        $existingBattle = $battleRepo->findBy(['location' => $location, 'isFinished' => false]);

        if (null == $existingBattle) {
            $battle = new Battle($location);

            // TODO Put log logic inside each function! Once GameLogManager has been refactored, we just have to flush all logs in the desired order.
            $this->addAttackingGeneralInBattle($battle, $instigatingGeneral);
            $this->addDefendingArmiesInBattle($battle, $location);
            $this->addFortifyingGeneralsInBattle($battle);

            assert([] !== $battle->getDefensingArmies());

            // TODO Do the same for all joining armies.
            $allEngagedArmies = array_merge($battle->getAttackingArmies(), $battle->getDefensingArmies());
            foreach ($allEngagedArmies as $engagedArmy) {
                $this->initBattleUnitStats($engagedArmy);
            }

            $this->refillFightsService->exec($battle);
            $this->entityManager->flush();

            $this->logBattleStart($instigatingGeneral, $battle);
        } else {
            $failReason = $this->translator->trans('map.actions.attack.battleAlreadyExist', [], 'Notification');
        }
    }

    /**
     * Trigger Management Battle Logs (Lord attacked something and Lord got attacked!)
     * And also Battle Engagement Logs!
     */
    public function logBattleStart(General $instigatingGeneral, Battle $battle): void
    {
        $location = $battle->getLocation();
        $locationSovereign = $location->getSovereign();
        $isBarbarianCity = $location->getData()->isSpawnPoint() && !$location->getSovereign() instanceof Lord;
        $instigatingLord = $instigatingGeneral->getOwner();

        $this->gameLogManager->beginLogTransaction();

        if ($isBarbarianCity) {
            $this->gameLogManager->addKingdomLog(
                $instigatingLord,
                LogEventType::MANAGEMENT_BATTLE_INSTIGATED_AGAINST_BARBARIAN,
                false,
                [
                    '%generalName%' => $instigatingGeneral->getName(),
                    '%worldId%' => $battle->getLocation()->getOwningWorld()->getId(),
                    '%nodeName%' => $battle->getLocation()->getName(),
                    '%nodeId%' => $battle->getLocation()->getId(),
                ]
            );

            $this->gameLogManager->addBattleLog(
                $battle,
                LogEventType::BATTLE_BARBARIAN_JOINED,
                false,
                [
                    '%amount%' => $location->getArmyContainer()->getTotalUnitCount(),
                ]
            );
        } else {
            // Notify instigator!
            $this->gameLogManager->addKingdomLog(
                $instigatingLord,
                LogEventType::MANAGEMENT_BATTLE_INSTIGATED_AGAINST_PLAYER,
                false,
                [
                    '%generalName%' => $instigatingGeneral->getName(),
                    '%userId%' => $locationSovereign->getUser()->getId(),
                    '%title%' => $locationSovereign->getTitle(),
                    '%lord%' => $locationSovereign->getName(),
                    '%worldId%' => $battle->getLocation()->getOwningWorld()->getId(),
                    '%nodeName%' => $battle->getLocation()->getName(),
                    '%nodeId%' => $battle->getLocation()->getId(),
                ]
            );

            // Notify defending Player!
            $isCapitalCityAttacked = $locationSovereign->getCity()->getNode() === $location;
            $logType = $isCapitalCityAttacked ? LogEventType::MANAGEMENT_BATTLE_DEFENSE_ON_CAPITAL_CITY :
                LogEventType::MANAGEMENT_BATTLE_DEFENSE_ON_TERRITORY;

            $this->gameLogManager->addKingdomLog(
                $locationSovereign,
                $logType,
                false,
                [
                    '%generalName%' => $instigatingGeneral->getName(),
                    '%userId%' => $instigatingLord->getUser()->getId(),
                    '%title%' => $instigatingLord->getTitle(),
                    '%lord%' => $instigatingLord->getName(),
                    '%worldId%' => $battle->getLocation()->getOwningWorld()->getId(),
                    '%nodeName%' => $battle->getLocation()->getName(),
                    '%nodeId%' => $battle->getLocation()->getId(),
                ]
            );

            $unitsCountInGarrison = $location->getArmyContainer()->getTotalUnitCount();
            if ($unitsCountInGarrison > 0) {
                $this->gameLogManager->addBattleLog(
                    $battle,
                    LogEventType::BATTLE_GARRISON_JOINED,
                    false,
                    [
                        '%userId%' => $locationSovereign->getUser()->getId(),
                        '%title%' => $locationSovereign->getTitle(),
                        '%lord%' => $locationSovereign->getName(),
                        '%amount%' => $location->getArmyContainer()->getTotalUnitCount(),
                    ]
                );
            }
        }

        $defendingGenerals = $battle->getDefendingGenerals();
        foreach ($defendingGenerals as $general) {
            $generalOwner = $general->getOwner();
            $this->gameLogManager->addBattleLog(
                $battle,
                LogEventType::BATTLE_GENERAL_JOINED_DEFENSE,
                false,
                [
                    '%userId%' => $generalOwner->getUser()->getId(),
                    '%title%' => $generalOwner->getTitle(),
                    '%lord%' => $generalOwner->getName(),
                    '%generalName%' => $general->getName(),
                    '%amount%' => $general->getArmyContainer()->getTotalUnitCount(),
                ]
            );
        }

        $this->gameLogManager->addBattleLog(
            $battle,
            LogEventType::BATTLE_GENERAL_JOINED_ATTACK,
            false,
            [
                '%userId%' => $instigatingLord->getUser()->getId(),
                '%title%' => $instigatingLord->getTitle(),
                '%lord%' => $instigatingLord->getName(),
                '%generalName%' => $instigatingGeneral->getName(),
                '%amount%' => $instigatingGeneral->getArmyContainer()->getTotalUnitCount(),
            ]
        );

        $this->gameLogManager->finishLogTransaction();
    }

    /**
     * Tick all Pending Battles (try to catchup if we didn't update Fights since a long time).
     */
    public function refreshAllBattles(WorldMap $worldMap, float $tickCount): void
    {
        $battleRepo = $this->entityManager->getRepository(Battle::class);
        $unfinishedBattles = $battleRepo->findBy(['worldMap' => $worldMap, 'isFinished' => false]);

        if (count($unfinishedBattles) > 0) {
            $this->entityManager->beginTransaction();
            try {
                for ($i = 0; $i < $tickCount; ++$i) {
                    $stillHaveFightsToTick = $this->tickPendingFight->exec($worldMap);
                    if (false == $stillHaveFightsToTick) {
                        break;
                    }
                }

                foreach ($unfinishedBattles as $battle) {
                    if ($this->shouldEndBattle($battle)) {
                        $this->endBattle($battle);
                    }
                }

                $this->entityManager->commit();
            } catch (\Exception $exception) {
                $this->entityManager->rollback();
                throw $exception;
            }
        }
    }

    /**
     * Returns true if a Battle should be ended right now.
     */
    public function shouldEndBattle(Battle $battle): bool
    {
        // Territory is not owned anymore
        if (null == $battle->getLocation()->getSovereign()) {
            return true;
        }

        // If Armies Owner die, armies become obsolete
        $isAllAttackingArmiesObsolete = true;
        foreach ($battle->getAttackingArmies() as $engagedArmy) {
            $isAllAttackingArmiesObsolete &= (null !== $engagedArmy->getLinkedArmy());
        }
        if (!$isAllAttackingArmiesObsolete) {
            return true;
        }

        if (CheatConfiguration::CHEAT_INSTANT_BATTLE_END) {
            return true;
        }

        return 0 == $battle->getFights()->count();
    }

    /**
     * Battle should be finished. End it and update everything!
     * TODO Update Winning General reputation
     * TODO Kill Losing General...
     * TODO Update MovementOrder?
     * TODO From here Vassalization too?
     *
     * @throws \Exception
     */
    public function endBattle(Battle $battle): void
    {
        assert(false === $battle->isFinished());

        // TODO If Attacking Lord is dead -> Cancel the battle
        // TODO If Defending Lord is dead -> Give the territory to the Attacking Lord
        $attackScore = $this->countUnitWithinArmies($battle->getAttackingArmies());
        $defenseScore = $this->countUnitWithinArmies($battle->getDefensingArmies());

        // dump(sprintf("A: %d - D: %d", $attackScore, $defenseScore));

        $this->entityManager->beginTransaction();
        try {
            $draw = $attackScore === $defenseScore;
            if (false === $draw) {
                $attackSideWon = $attackScore > $defenseScore;

                // TODO Add Notification!
                // dump(sprintf('Attack won ? %d [Atk: %d / Def: %d]', $attackSideWon, $attackScore, $defenseScore));
                $winningEngagedArmies = $attackSideWon ? $battle->getAttackingArmies() : $battle->getDefensingArmies();
                $losingEngagedArmies = $attackSideWon ? $battle->getDefensingArmies() : $battle->getAttackingArmies();

                // Evaluate winning Generals
                $winningGenerals = [];
                foreach ($winningEngagedArmies as $engagedArmy) {
                    if (null !== $engagedArmy->getLinkedArmy()) {
                        /* @var General $general */
                        $general = $battle->getEngagedGenerals()->findFirst(
                            fn (int $i, General $g) => $g->getArmyContainer()->getId() == $engagedArmy->getLinkedArmy()->getId());

                        if (null !== $general) {
                            $winningGenerals[] = $general;
                        }
                    }
                }

                $this->gameLogManager->beginLogTransaction();
                foreach ($losingEngagedArmies as $engagedArmy) {
                    $this->eliminateArmyAndGeneral($battle, $engagedArmy);
                }
                $this->gameLogManager->finishLogTransaction();

                $this->gameLogManager->addBattleLog(
                    $battle,
                    $attackSideWon ? LogEventType::BATTLE_ATTACK_WON : LogEventType::BATTLE_DEFENSE_WON,
                    true
                );

                // TODO This could choose the wrong Lord if there is more than one general!
                // Find a way to find the instigating General, maybe store it or add the engagement date?
                $winningGeneral = null;
                if (count($winningGenerals) > 0) {
                    $winningGeneral = $winningGenerals[0];
                }

                if (null !== $winningGeneral) {
                    $location = $battle->getLocation();
                    assert(null !== $winningGeneral->getOwner());
                    $oldSovereign = $location->getSovereign();
                    if ($oldSovereign !== $winningGeneral->getOwner()) {
                        $this->gameLogManager->beginLogTransaction();
                        // Win / Lose Logs
                        $dynParams = [
                            '%nodeName%' => $battle->getLocation()->getName(),
                            '%battleId%' => $battle->getId(),
                        ];
                        $this->gameLogManager->addKingdomLog(
                            $winningGeneral->getOwner(),
                            LogEventType::MANAGEMENT_BATTLE_WON,
                            false,
                            $dynParams
                        );

                        if (null !== $oldSovereign) {
                            $this->gameLogManager->addKingdomLog(
                                $oldSovereign,
                                LogEventType::MANAGEMENT_BATTLE_LOST,
                                false,
                                $dynParams
                            );
                        }

                        $this->gameLogManager->finishLogTransaction();

                        $this->claimTerritory($winningGeneral, wasDefendedWhenConquered: true);
                    }
                }
            } else {
                $this->gameLogManager->beginLogTransaction();
                foreach ($battle->getAttackingArmies() as $engagedArmy) {
                    $this->eliminateArmyAndGeneral($battle, $engagedArmy);
                }
                $this->gameLogManager->finishLogTransaction();
            }

            // Post Battle Cleanup
            $battle->markFinished();
            $battle->getFights()->clear();
            $this->entityManager->persist($battle);

            $this->resolveArmiesAfterBattle($battle);

            $this->entityManager->flush();

            /** @var FightSurvivorRepository $fightSurvivorRepository */
            $fightSurvivorRepository = $this->entityManager->getRepository(FightSurvivor::class);
            $fightSurvivorRepository->deleteSurvivors($battle);

            foreach ($battle->getEngagedGenerals() as $general) {
                $general->setBattle(null);
                $this->entityManager->persist($general);
            }

            $this->entityManager->flush();

            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->entityManager->rollback();
            throw $e;
        }
    }

    private function initBattleUnitStats(EngagedArmy $engagedArmy): void
    {
        foreach ($engagedArmy->getArmyContainer()->getUnits() as $unitArmy) {
            if ($unitArmy->getAmount() > 0) {
                $battleUnitStat = new BattleUnitStat($unitArmy);
                $engagedArmy->addBattleStat($battleUnitStat);
            }
        }
    }

    /***
     * If relevant, eliminate the General of the specified Army
     *
     * @param Battle $battle
     * @param EngagedArmy $army
     *
     * @return void
     */
    private function eliminateArmyAndGeneral(Battle $battle, EngagedArmy $army): void
    {
        if (null !== $army->getLinkedArmy()) {
            /** @var General $general */
            $general = $battle->getEngagedGenerals()->findFirst(
                fn (int $i, General $g) => $g->getArmyContainer()->getId() == $army->getLinkedArmy()->getId());

            if (null !== $general) {
                $owner = $general->getOwner();
                $this->gameLogManager->addBattleLog(
                    $battle,
                    LogEventType::BATTLE_GENERAL_DEFEATED,
                    false,
                    [
                        '%generalName%' => $general->getName(),
                        '%userId%' => $owner->getUser()->getId(),
                        '%title%' => $owner->getTitle()->value,
                        '%lord%' => $owner->getName(),
                    ]
                );

                $this->entityManager->persist($army);
                $this->entityManager->remove($general);
            }

            $army->getLinkedArmy()->clearAllUnits();
            $army->clearLinkedArmy();
        }
    }

    private function resolveArmiesAfterBattle(Battle $battle): void
    {
        foreach ($battle->getAllEngagedArmies() as $engagedArmy) {
            $linkedArmy = $engagedArmy->getLinkedArmy();
            if (null !== $linkedArmy) {
                $linkedArmy->overrideUnits($engagedArmy->getArmyContainer()->getUnits());

                $survivorCountPerUnitName = [];
                foreach ($engagedArmy->getFightSurvivors() as $fightSurvivor) {
                    if (array_key_exists($fightSurvivor->getUnitType()->getName(), $survivorCountPerUnitName)) {
                        ++$survivorCountPerUnitName[$fightSurvivor->getUnitType()->getName()];
                    } else {
                        $survivorCountPerUnitName[$fightSurvivor->getUnitType()->getName()] = 1;
                    }
                }
                foreach ($survivorCountPerUnitName as $unit => $count) {
                    $linkedArmyUnit = $linkedArmy->getUnit($unit);
                    $linkedArmyUnit->setAmount($linkedArmyUnit->getAmount() + $count);
                }

                $this->entityManager->persist($linkedArmy);

                $engagedArmy->clearLinkedArmy();
                $this->entityManager->persist($engagedArmy);
            }
        }
    }

    // Battle helpers

    private function addAttackingGeneralInBattle(Battle $battle, General $instigatingGeneral): void
    {
        $battle->engageGeneral($instigatingGeneral, isAttacking: true);
    }

    private function addFortifyingGeneralsInBattle(Battle $battle): array
    {
        $defendingGenerals = [];
        foreach ($battle->getLocation()->getGenerals() as $nearbyGeneral) {
            if ($nearbyGeneral->getOwner() === $battle->getLocation()->getSovereign() && $nearbyGeneral->isFortifying() && $nearbyGeneral->hasAnArmy()) {
                $battle->engageGeneral($nearbyGeneral, isAttacking: false);
                $nearbyGeneral->setFortifying(false);

                $defendingGenerals[] = $nearbyGeneral;
            }
        }

        return $defendingGenerals;
    }

    private function addDefendingArmiesInBattle(Battle $battle, WorldMapNode $territory): void
    {
        if ($territory->getData()->isSpawnPoint()) {
            /** @var CityRepository $cityRepo */
            $cityRepo = $this->entityManager->getRepository(City::class);
            if (($city = $cityRepo->findOneBy(['node' => $territory])) && $city->getDefense()->hasAnyUnits()) {
                $battle->engageArmy($city->getDefense(), isAttacking: false);
            }
        }

        if ($territory->getArmyContainer()->hasAnyUnits()) {
            $battle->engageArmy($territory->getArmyContainer(), isAttacking: false);
        }
    }

    /**
     * @param array<EngagedArmy> $engagedArmies
     */
    private function countUnitWithinArmies(array $engagedArmies): int
    {
        $unitsCount = 0;
        foreach ($engagedArmies as $engagedArmy) {
            if (null !== $engagedArmy->getLinkedArmy()) {
                $unitsCount += $engagedArmy->getArmyContainer()->getTotalUnitCount();
                $unitsCount += $engagedArmy->getFightSurvivors()->count();
                $unitsCount += $engagedArmy->getFightingUnits()->count();
            }
        }

        return $unitsCount;
    }

    // Refer to this calculation for the estimate : https://github.com/motion-twin/WebGamesArchives/blob/8b8e2f202e1fa8cdf1d96e4140dad0cba458054e/Muxxu/Kingdom/src/Rules.hx#L1245
    public function getEstimatedBattleEndDate(Battle $battle): \DateTime
    {
        $estimatedDate = new DateTime();

        if($battle->isFinished()) {
            return $estimatedDate;
        }

        $startDate = $battle->getStartTime();
        $attUnitsHealth = 0;
        $defUnitsHealth = 0;

        $ruleSet = $this->getRuleset->exec();

        foreach ($battle->getAllEngagedArmies() as $engagedArmy) {
            foreach ($engagedArmy->getFightingUnits() as $fighter) {
                    if($engagedArmy->isAttacking()) {
                        $attUnitsHealth += $fighter->getRemainingHealth();
                    } else {
                        $defUnitsHealth += $fighter->getRemainingHealth();
                    }
            }
            foreach($engagedArmy->getFightSurvivors() as $survivor) {
                if($engagedArmy->isAttacking()) {
                    $attUnitsHealth += $survivor->getRemainingHealth();
                } else {
                    $defUnitsHealth += $survivor->getRemainingHealth();
                }
            }
            foreach($engagedArmy->getArmyContainer()->getUnits() as $unitArmy) {
                $unitInfo = $ruleSet->getUnitsInfo($unitArmy->getResource()->getName());
                if($engagedArmy->isAttacking()) {
                    $attUnitsHealth += $unitArmy->getAmount() * $unitInfo[CoreRuleset::healthKey];
                } else {
                    $defUnitsHealth += $unitArmy->getAmount() * $unitInfo[CoreRuleset::healthKey];
                }
            }
        }

        $maxHealth = $attUnitsHealth > $defUnitsHealth ? $attUnitsHealth : $defUnitsHealth;
        $time = round($maxHealth / (0.75 * count($battle->getFights())));
        $estimatedDate = $startDate->modify("+$time minutes");

        return $estimatedDate;
    }
}
