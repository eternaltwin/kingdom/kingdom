SET folder=%~dp0\..\EternalKingdom\bin\console

php %folder% doctrine:database:drop --force
php %folder% doctrine:database:create
php %folder% doctrine:migrations:migrate --no-interaction