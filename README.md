EternalKingdom Setup
-------------------------------

EternalKingdom est un projet OpenSource de reconstruction du jeu Kingdom développé par Motion à l'époque déployé sur MUXXU.

**Technologies:**
- Php 8.2
- Symfony 5.4
- Postgres 17
- Yarn 1.22.22

**Equipe de dev Open Source:**
- Lead & Documentation : Drange, Glaurung
- Developpeurs actifs: Talsi, Bibni, Thonk.
- Developpeurs inactifs : Keats, Devwwn, Simpkin, BSanchez
- Aide ponctuelles des membres d'EternalTwin : Biosha, Evian, Demurgos, Patate.

---

# Lancer le serveur WEB en local: Quickstart

Afin de pouvoir lancer et tester le site, il faut d'abord procéder à quelques installations.

Rendez vous d'abord sur cette page pour avoir accès à la documentation complète d'un setup Symfony (https://symfony.com/doc/5.x/setup.html)

1. **Technologies à installer** (Technical Requirements)

Veillez à bien installer Php, Composer, Symfony & yarn. Et enfin Postgres! (Vous pouvez utiliser docker pour postgres si vous connaissez)  
Si la commande `symfony check:requirements` indique Succès, vous pouvez continuer.  
Une fois php installé, veuillez modifier le fichier php.ini et décommenter la ligne `extension=pdo_pgsql` (en retirant le ';')

2. **Modifier vos variables d'environnement**

Rendez-vous dans le dossier EternalKingdom.  
Copiez le fichier **.env.example** et renommez le **.env.local** puis décommentez et modifiez la ligne 21 (DATABASE_URL pour postgres) à votre convenance (à savoir : modifiez `db_user` et `db_password` selon la config de votre bdd).  
Remplacez aussi `db_name` par `EternalKingdom`

3. **Charger les plugins tierces non stockés sur le git**

Rendez vous dans le dossier EternalKingdom de ce projet git et lancez la commande : `composer install`  
Ceci à pour but de télécharger et installer les plugins non stockés sur le git du fait de leurs poids.

4. **Lancement de Eternal Twin** (nécessaire pour l'OAuth2).

Rendez-vous dans le dossier Eternaltwin.  
Pour installer Eternal Twin et ses dépendances : `yarn install`  
Si le fichier **eternaltwin.toml** n'existe pas, créez-le en copiant **eternaltwin.toml.example**.  
Dans le fichier **eternaltwin.toml**, commentez la ligne 10 (api = "postgres") et décommentez la ligne 11 (api = "in-memory").  
Pour lancer l'application : `yarn etwin start`

Vous pouvez maintenant accéder Eternal Twin (lite) depuis http://localhost:50320

5. **Créer/Mettre à jour la base de données**

Rendez-vous dans le dossier EternalKingdom et lancez les commandes suivantes en entrant yes quand demandé :  
`php bin/console doctrine:database:create`  
`php bin/console doctrine:migrations:migrate`  
`php bin/console doctrine:fixtures:load`

Pour plus d'informations :  
- Create the DB, Load migrations & Load fixtures!  
https://gitlab.com/eternaltwin/kingdom/kingdom/-/wikis/Symfony/Commands-sheet

6. **Lancement du serveur**

Rendez-vous dans le dossier EternalKingdom et lancez le projet Symfony à l'aide de la commande :
`symfony server:start` 

Rendez vous maintenant sur http://localhost:8000 (ou bien http://localhost:8080) et profitez du site 💖

Vous allez devoir vous créer un compte etwin (pas de panique, ceci n'est qu'en local, vous pouvez mettre n'importe quoi pour les creds).  
Si vous rencontrez des problèmes lors de la connection, n'hésitez pas à déco reco et/ou à relancer le serveur de dév.

# Configurer Eternaltwin:

Etwin documentation: https://eternal-twin.net/docs/app/etwin-oauth  
dans EternalKingdom/.env: (ou .env.local)
```
    IDENTITY_SERVER_URI="http://localhost:50320"
    OAUTH_CALLBACK="'http://localhost:8000/oauth/callback'" 
    OAUTH_AUTHORIZATION_URI="http://localhost:50320/oauth/authorize"
    OAUTH_TOKEN_URI="http://localhost:50320/oauth/token"
    OAUTH_CLIENT_ID="kingdom@clients"
    OAUTH_SECRET_ID="dev_secret"
```
Ces variables d'environnements doivent correspondre à celles définis dans EternalTwin/etwin.toml
```
[clients.kingdom]
display_name = "kingdom"
app_uri = "http://localhost:8000"
callback_uri = "http://localhost:8000/oauth/callback"
secret = "dev_secret"
```

## Utiliser Eternal Twin avec une database (api = postgres uniquement)

Etwin documentation: https://eternal-twin.net/docs/app/etwin-integration

Dans un premier temps, copiez le fichier EternalTwin/etwin.example.toml et renommez le EternalTwin/etwin.toml
Dans ce fichier changez les paramètres suivants pour qu'ils correspondent a votre base de données.
```
api = "postgres"
[db]
# Database service host
host = "localhost"
# Database service port
port = 5432
# Database name
name = "eternal_twin"
# Database user (role). In development, it is recommended to be a superuser to manage extensions.
user = "user"
# Password for the database user.
password = "password"
```

Dans le dossier `Eternaltwin` vous pouvez maintenant initialiser la base de données :
```
yarn etwin db reset
```

Si vous avez une base de données existante, vous pouvez la mettre à jour avec :

```
yarn etwin db sync
```

# Installer le projet avec Docker

Si vous le souhaitez, vous pouvez installer le projet avec [Docker](https://docs.docker.com/get-docker/) pour minimiser le nombre de dépendances à installer et de manipulations à effectuer sur votre machine.

## Utilisateurs Windows

Pour les utilisateurs de Windows, il est fortement recommandé d'utiliser [WSL2](https://docs.microsoft.com/en-us/windows/wsl/install-win10) pour exécuter Docker.

WSL2 devrait être installé par défaut sur les versions récentes de Windows 10+. Essayez d'exécuter `wsl --help` dans un terminal Powershell. Si cela ne fonctionne pas, suivez les instructions de [cette page](https://learn.microsoft.com/en-us/windows/wsl/install-manual) pour l'installer.

Installez ensuite [Ubuntu](https://apps.microsoft.com/store/detail/ubuntu/9PDXGNCFSCZV?hl=fr-fr&gl=fr&rtc=1) avec WSL2 : `wsl --install -d Ubuntu`

Lancez ensuite Ubuntu : `wsl -d Ubuntu`

Après avoir configuré votre compte Ubuntu, vous pouvez installer le projet en suivant les instructions ci-dessous.

## Installer les outils de build

### Linux (Natif ou WSL2)

```bash
sudo -s
apt-get update -y
apt-get install build-essential -y
```

### MacOS

Rien à faire.

## Installer Docker et Docker compose

### Linux (Natif ou WSL2)

```bash
apt-get install lsb-release -y
mkdir -m 0755 -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update -y
apt-get install docker docker-compose docker-compose-plugin -y
exit
```

Utilisateurs WSL2 : Bien qu'il soit possible d'exécuter une application extraite sous Windows et montée sur WSL2, cela sera très lent, je recommande donc d'extraire le dépôt dans WSL et de travailler ensuite avec les sources soit par l'intermédiaire de VSCode's WSL remote https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-wsl, soit en accédant aux fichiers via SMB (par exemple `\\wsl$\Debian\root\kingdom`).

### MacOS

Installer [Docker Desktop](https://docs.docker.com/docker-for-mac/install/).

## Installer le projet

 - Si ce n'est pas encore fait, générez une clé SSH et ajoutez-la à votre profil GitLab :
    - Générer la clé : `ssh-keygen -t rsa -b 2048 -C "Clé SSH pour le dépôt Kingdom (https://gitlab.com/eternaltwin/kingdom/kingdom)"`
    - Afficher la clé : `cat ~/.ssh/id_rsa.pub`
    - Copiez la clé et ajoutez-la à votre profil GitLab : https://gitlab.com/-/profile/keys

 - Clonez le dépôt et déplacez vous dans le dossier créé : `git clone git@gitlab.com:eternaltwin/kingdom/kingdom.git && cd kingdom`

 - Construisez les conteneurs : `make`

Et c'est tout !

 Si tout va bien, vous pouvez accéder à:
  -  Eternal Kingdom sur http://localhost:8000
  -  Eternaltwin sur http://localhost:50320

Vous pouvez vous créér un compte Eternaltwin sur le site correspondant, et vous connecter à Eternal Kingdom avec.
N'oubliez pas de changer la variable d'environnement ADMIN_ID dans le fichier .env.local avec l'UUID du compte ainsi créé pour avoir les droits d'administrateur.

# Quelques commandes utiles

(testées avec l'installation Docker, mais devrait marcher avec l'installation native Windows si exécutées dans le dossier `EternalKingdom`)

- `composer db:reset-and-load-fixtures` : Réinitialise la base de données et charge les fixtures. A utiliser pour un fresh start.
- `composer db:make-migrations` : Génère des migrations Doctrne en comparant le schéma de la base de données avec les entités de votre code
- `composer run-tests` : Lance l'ensemble des tests d'EternalKingdom, à exécuter régulièrement pour s'assurer que vous ne cassez rien.

Voir plus de commandes dans le [`composer.json`](EternalKingdom/composer.json)

# License

[AGPL-3.0-or-later](./license.md)
