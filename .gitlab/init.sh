#!/bin/env bash
set -euo pipefail

pacman -Syu --quiet --noconfirm --noprogressbar
pacman -S --quiet --noconfirm --noprogressbar \
    composer \
    php \
    php-pgsql
pacman -Sc --quiet --noconfirm

echo "extension=iconv" > "/etc/php/conf.d/ext_iconv.ini"
echo "extension=pdo_pgsql" > "/etc/php/conf.d/ext_pdo_pgsql.ini"

set -x
php --version
composer --version
set +x
