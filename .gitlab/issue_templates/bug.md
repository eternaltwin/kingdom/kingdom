## Summary

(Summarize the bug encountered concisely)

Put the same in the title with the page were the bug happened (put Website if that's a general issue).

## Steps to reproduce

(How one can reproduce the issue - this is very important)

Example:
1. Recruit a general
2. Select your general and click on the move action
3. Choose a destination very far away
3. **Notice that general disappeared**


/label ~Bugs
